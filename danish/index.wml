#use wml::debian::links.tags
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/index.def"
#use wml::debian::mainpage title="<motto>"
#use wml::debian::translation-check translation="fe2d0a6290a91814fb8d336efc57e158b56b319e"

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<div id="splash">
  <h1>Debian</h1>
</div>

<!-- The first row of columns on the site. -->
<div class="row">
  <div class="column column-left">
    <div style="text-align: center">
      <h1>Fællesskabet</h1>
      <h2>Debian er et fællesskab!</h2>

#include "$(ENGLISHDIR)/index.inc"

    <div class="row">
      <div class="community column">
        <a href="intro/people" aria-hidden="true">
          <img src="Pics/users.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/people">Personer</a></h2>
        <p>Hvem vi er og hvad vi gør</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/philosophy" aria-hidden="true">
          <img src="Pics/heartbeat.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/philosophy">Vores filosofi</a></h2>
        <p>Hvorfor gør vi det, og hvordan gør vi det</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="devel/join/">
          <img src="Pics/user-plus.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="devel/join/">Bliv involveret, bidrag</a></h2>
        <p>Hvordan du kan deltage!</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#community" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#community">Mere ...</a></h2>
        <p>Yderligere oplysninger om Debians fællesskab</p>
      </div>
    </div>
  </div>
  <div class="column column-right">
    <div style="text-align: center">
      <h1>Styresystemet</h1>
      <h2>Debian er et helt frit styresystem!</h2>
      <div class="os-img-container">
        <img src="Pics/debian-logo-1024x576.png" alt="Debian">
	<a href="$(HOME)/download" class="os-dl-btn"><download></a>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/why_debian" aria-hidden="true">
          <img src="Pics/trophy.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/why_debian">Hvorfor Debian</a></h2>
        <p>Hvad der gør Debian til noget særligt</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="support" aria-hidden="true">
          <img src="Pics/life-ring.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="support">Brugersupport</a></h2>
        <p>Få hjælp og dokumentation</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="security/" aria-hidden="true">
          <img src="Pics/security.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="security/">Sikkerhedsopdateringer</a></h2>
        <p>Debians sikkerhedsbulletiner (DSA)</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#software" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
    <h2><a href="intro/index#software">Mere ...</a></h2>
        <p>Yderligere links til downloads og software</p>
      </div>
    </div>
  </div>
</div>

<hr>

# <!-- An optional row highlighting events happening now, such as releases, point releases, debconf and minidebconfs, and elections (dpl, GRs...). -->
# <div class="row">
#  <div class="column styled-href-blue column-left">
#    <div style="text-align: center">
#      <h2><a href="https://debconf21.debconf.org/">DebConf21</a> er i gang!</h2>
#      <p>Debian Conference finder sted online fra den 24. til 28. august 2021.</p>
#    </div>
#  </div>
# </div>


<!-- The next row of columns on the site. -->
<!-- The News will be selected by the press team. -->


<div class="row">
  <div class="column styled-href-blue column-left">
    <div style="text-align: center">
      <h1><projectnews></h1>
      <h2>Nyheder og annonceringer om Debian</h2>
    </div>

    <:= get_top_news() :>

    <!-- No more News entries behind this line! -->
    <div class="project-news">
      <div class="end-of-list-arrow"></div>
      <div class="project-news-content project-news-content-end">
        <a href="News">Alle nyhederne</a> &emsp;&emsp;
    <a class="rss_logo" style="float: none" href="News/news">RSS</a>
      </div>
    </div>
  </div>
</div>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian News" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Debian Project News" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Debians sikkerhedsbulletiner (kun overskrifter)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debians sikkerhedsbulletiner (opsummeringer)" href="security/dsa-long">
:#rss#}
