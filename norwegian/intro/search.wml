#use wml::debian::template title="Informasjon om å bruke Debians søkemotor"
#use wml::debian::translation-check translation="821d2af3a565be7b911813a3fb1a5543be4391e6" maintainer="Hans F. Nordhaug"

  <p>
    Debians søkemotor på 
    <a href="https://search.debian.org/">https://search.debian.org/</a>
    tillater forskjellige søkemetoder, alt etter hva du ønsker å
    finne.</p>

  <h3>Enkelt søk</h3>

  <p>
    Den mest lettvinte måten er å fylle inn et enkelt ord inn i
    søkefeltet, og trykke ENTER (eller klikk på <em>Søk</em>-knappen).
    Søkemotoren leverer da alle sidene som inneholder dette ordet.
    Denne måten gir som regel gode resultater.</p>

  <p>
    Det neste trinnet opp er å søke på mer enn ett ord som 
    leverer sider som inneholder alle søkeordene du skrev inn.</p>

  <h3>Boolsk søk</h3>

  <p>
    Om enkelt søk ikke er nok, kan 
    <a href="https://foldoc.org/boolean">boolsk</a>
    søk virke for deg.  Du kan bruke <em>AND</em>, <em>OR</em>,
    <em>NOT</em>, og kombinasjoner av disse tre. Merk at disse 
    operatorene må skrives med store bokstaver for å bli gjenkjent.
  </p>

  <p>
    <b>AND</b> leverer resultater hvor begge ordene
      fins på siden.  For eksempel vil «gcc AND patch» finne sider
      som inneholder både «gcc» og «patch». Dette søket gir samme resultat
      som «gcc patch», men eksplisitt bruk av AND kan være nyttig sammen
      med andre operatorer.</p>

  <p>
    <b>OR</b> leverer resultater hvor minst ett av ordene fins på siden.
    For eksempel vil «gcc OR patch» finne sider som inneholder enten «gcc»
    eller «patch».</p>

  <p>
    <b>NOT</b> velger bort et ord fra resultatene. For eksempel vil 
    «gcc NOT patch» finne alle sidene som inneholder «gcc», bortsett fra
    de sidene som også inneholder «patch». Du kan også skrive 
    «gcc AND NOT patch» for det samme resultatet, men søk etter bare
    «NOT patch» er ikke støttet.</p>

  <p>
    <b><b>(</b>...<b>)</B></b> gir enda mer nyansert kontroll, siden du
    nå kan sette sammen logiske enheter. For eksempel vil 
    «(gcc OR make) NOT patch» finne alle sider som inneholder enten «gcc»
    eller «make», men ikke «patch».</p>

# Local variables:
# mode: sgml
# sgml-indent-data:t
# sgml-doctype:"../.doctype"
# End:
