#use wml::debian::template title="Hoe www.debian.org gemaakt wordt" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="7f876037d8c7ab141d9c245fcd09a3b77e25b3a7"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#look">Uitzicht &amp; aanvoelen</a></li>
<li><a href="#sources">Broncode</a></li>
<li><a href="#scripts">Scripts</a></li>
<li><a href="#generate">Het genereren van de website</a></li>
<li><a href="#help">Hoe u kunt helpen</a></li>
<li><a href="#faq">Hoe u niet zou mogen helpen... (FAQ)</a></li>
</ul>


<h2><a id="look">Uitzicht &amp; aanvoelen</a></h2>

<p>De website van Debian is een verzameling mappen en bestanden
die te vinden zijn in <code>/org/www.debian.org/www</code>
op <em>www-master.debian.org</em>. De meeste pagina's zijn
statische HTML-bestanden. Deze bevatten geen dynamische elementen
zoals CGI en PHP-scripts, omdat de website gespiegeld wordt.
</p>

<p>
De website van Debian gebruikt een metataal voor websites
(<a href="https://packages.debian.org/unstable/web/wml">WML</a>)
om de HTML-pagina's te genereren, met inbegrip van
kop- en voetteksten, titels, inhoudstafels, enz.
Hoewel een <code>.wml</code>-bestand er op het eerste gezicht misschien
uitziet als HTML, is HTML slechts één
van de types extra informatie die in WML gebruikt kunnen worden. U
kunt ook Perl-code toevoegen aan een pagina, zodat u ermee bijna alles
kunt gedaan krijgen.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Onze webpagina's voldoen momenteel aan de standaard <a href="http://www.w3.org/TR/html4/">HTML 4.01 Strict</a>.</p>
</aside>

<p>
Nadat WML klaar is met het toepassen van zijn diverse filters op een bestand,
is het eindproduct echte HTML. Merk evenwel op dat hoewel WML op een basaal
niveau de geldigheid van uw HTML-code controleert (en deze soms automatisch
corrigeert), u een hulpmiddel zoals
<a href="https://packages.debian.org/unstable/web/weblint">weblint</a>
en/of
<a href="https://packages.debian.org/unstable/web/tidy">tidy</a>
zou moeten installeren voor syntaxis- en minimale stijlcontrole.
</p>

<p> Iedereen die op regelmatige basis bijdragen levert aan de website van
Debian, zou WML moeten installeren om de code te testen en er zeker van te zijn
dat de resulterende HTML-pagina's er goed uitzien. Indien u op een computer
met Debian werkt, moet u gewoon het pakket <code>wml</code> installeren. Lees
voor meer informatie de pagina over <a href="using_wml">het gebruik van WML</a>.


<toc-add-entry name="sources">Broncode</toc-add-entry>

<p>De broncode voor de webpagina's van Debian wordt in Git bewaard. Met het
versiebeheersysteem kunnen we alle wijzigingen bijhouden en zien wie wat
wanneer en zelfs waarom heeft vastgelegd. Git biedt een veilige manier om de
gelijktijdige bewerking van bronbestanden door meerdere auteurs te beheren -
voor het webteam van Debian een cruciale taak, omdat dit team veel leden telt.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="using_git">Meer lezen over Git</a></button></p>

<p>
Hier volgt wat achtergrondinformatie over de wijze waarop de broncode
gestructureerd is:
</p>

<ul>
  <li>De basismap in de git-opslagplaats (<code>webwml</code>) bevat mappen die
  vernoemd zijn naar de taal van de respectieve pagina's, twee
  Makefile-bestanden en verschillende scripts. De namen van de mappen voor de
  vertaalde pagina's moeten in het Engels zijn en uit kleine letters bestaan,
  bijvoorbeeld <code>dutch</code> en niet <code>Nederlands</code>.</li>

  <li>Vooral het bestand <code>Makefile.common</code> is bijzonder belangrijk,
  omdat het een aantal gemeenschappelijke regels bevat welke toegepast worden
  door dit bestand op te nemen in de andere Makefile-bestanden.</li>

  <li>Al de onderliggende mappen voor de verschillende talen bevatten ook
  Makefile-bestanden, verschillende <code>.wml</code>-broncodebestanden en
  extra onderliggende mappen. Alle namen voor bestanden en mappen volgen een
  bepaald patroon, zodat elke link voor alle vertaalde pagina's werkt. Sommige
  mappen bevatten ook een <code>.wmlrc</code>-configuratiebestand met extra
  commando's en voorkeursinstellingen voor WML.</li>

  <li>De map <code>webwml/english/template</code> bevat speciale WML-bestanden
  die als sjablonen werken. Vanuit alle andere bestanden kan er naar worden
  verwezen met het commando <code>#use</code>.</li>
</ul>

<p>
Opmerking: om er zeker van te zijn dat wijzigingen in de sjablonen doorstromen
naar de bestanden die ze gebruiken, hebben de bestanden deze sjablonen als
Makefile-vereiste. Een grote meerderheid van de bestanden gebruiken het
sjabloon <code>template</code> als generieke vereiste, en daarom bevatten ze
bovenaan de volgende regel:
</p>

<p>
<code>#use wml::debian::template</code>
</p>

<p>
Natuurlijk bestaan er uitzonderingen op deze regel.
</p>

<h2><a id="scripts">Scripts</a></h2>

<p>
De scripts zijn hoofdzakelijk geschreven in shell of Perl. Sommige ervan
werken apart en andere zijn geïntegreerd in de WML-bronbestanden.
</p>

<ul>
  <li><a href="https://salsa.debian.org/webmaster-team/cron.git">webmaster-team/cron</a>:
  Deze Git-opslagplaats bevat al de scripts die gebruikt worden voor het
  updaten van de website van Debian, d.w.z. de broncode van de scripts voor het
  opnieuw bouwen van <code>www-master</code>.</li>
  <li><a href="https://salsa.debian.org/webmaster-team/packages">webmaster-team/packages</a>:
  Deze Git-opslagplaats bevat de broncode van de scripts voor het opnieuw
  bouwen van <code>packages.debian.org</code>.</li>
</ul>

<h2><a id="generate">Het genereren van de website</a></h2>

<p>
WML, sjablonen en shell- en Perl-scripts zijn alle ingrediënten die u nodig
heeft om de website van Debian te genereren:
</p>

<ul>
  <li>Het grootste gedeelte wordt gegenereerd met behulp van WML (uit de <a href="$(DEVEL)/website/using_git">Git-opslagplaats</a>).</li>
  <li>De documentatie wordt ofwel gegenereerd met DocBook XML (<a href="$(DOC)/vcs"><q>ddp</q>-Git-opslagplaats</a>)
  of met <a href="#scripts">cron-scripts</a> uit de overeenkomstige Debian-pakketten.</li>
  <li>Sommige delen van de website worden gegenereerd met scripts die van
  andere bronnen gebruik maken, bijvoorbeeld de pagina's om op mailinglijsten
  in te tekenen of zich uit te schrijven.</li>
</ul>

<p>
Een automatische update (vanuit de git-opslagplaats en andere bronnen van de
webboom) wordt zesmaal per dag uitgevoerd. Daarnaast voeren we regelmatig de
volgende controles uit op de volledige website:
</p>

<ul>
  <li><a href="https://www-master.debian.org/build-logs/urlcheck/">URL-controle</a>
  <li><a href="https://www-master.debian.org/build-logs/validate/">wdg-html-validator</a>
  <li><a href="https://www-master.debian.org/build-logs/tidy/">tidy</a>
</ul>

<p>
De huidige logboeken over het bouwen van de website
zijn te vinden op <url "https://www-master.debian.org/build-logs/">.
</p>

<p>Indien u wenst bij te dragen tot de site, bewerk dan <strong>niet</strong>
gewoon bestanden uit de map <code>www/</code>, of voeg er ook geen nieuwe items
aan toe. Neem in plaats daarvan eerst contact op met de <a href="mailto:webmaster@debian.org">webmasters</a>.
</p>

<aside>
<p><span class="fas fa-cogs fa-3x"></span> Een meer technisch noot:
alle bestanden en mappen zijn eigendom van de groep <code>debwww</code> die schrijfrechten bezit.
Op die manier kan het webteam inhoud uit de web-map wijzigen.
De modus <code>2775</code> van de mappen betekent dat alle bestanden die hier aangemaakt worden de groep (<code>debwww</code>) zullen overerven.
Van leden van de groep wordt verwacht dat ze <code>umask 002</code> instellen, zodat bestanden aangemaakt worden met schrijfrechten voor de groep.</p>
</aside>


<h2><a id="help">Hoe u kunt helpen</a></h2>

<p>
We moedigen iedereen aan om te helpen met de website van Debian. Indien u over
waardevolle informatie in verband met Debian beschikt, die volgens u ontbreekt,
<a href="mailto:debian-www@lists.debian.org">neem dan contact op</a> — wij
zullen ervoor zorgen dat ze opgenomen wordt.
Kijk ook eens naar de bovengenoemde
<a href="https://www-master.debian.org/build-logs/">bouwlogboeken</a> en kijk
of u suggesties heeft om een probleem op te lossen.
</p>

<p>
We zijn ook op zoek naar mensen die kunnen helpen met het ontwerp (grafische
vormgeving, lay-out, enz.). Indien u vloeiend Engels spreekt, overweeg dan om
onze pagina's na te lezen en fouten te
<a href="mailto:debian-www@lists.debian.org">rapporteren</a>.
Als u een andere taal spreekt, dan wil u misschien helpen met het vertalen van
bestaande pagina's of met het verhelpen van bugs in reeds vertaalde pagina's.
Bekijk in beide gevallen de lijst met
<a href="translation_coordinators">vertaalcoördinatoren</a> en neem contact op
met de verantwoordelijke persoon. Bekijk voor meer informatie onze
<a href="translating">pagina voor vertalers</a>.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="todo">Lees onze TODO-lijst</a></button></p>


<aside class="light">
  <span class="fa fa-question fa-5x"></span>
</aside>

<h2><a id="faq">Hoe u niet zou mogen helpen... (FAQ)</a></h2>

<p>
<strong>[V] Ik wil <em>deze mooie functie</em> opnemen in de website van Debian, mag ik?</strong>
</p>

<p>
[A] Neen. We willen dat www.debian.org zo toegankelijk mogelijk is, dus
</p>

<ul>
    <li>geen browserspecifieke "extensies".
    <li>niet louter gebruik maken van afbeeldingen. Afbeeldingen kunnen gebruikt
        worden ter verduidelijking, maar de informatie op www.debian.org moet
        toegankelijk blijven voor een zuivere tekstuele webbrowser, zoals lynx.
</ul>

<p>
<strong>[V] Ik wil dit leuke idee implementeren. Kunt u alstublieft <em>foo</em> en <em>bar</em> activeren in de HTTP server van www.debian.org?</strong>
</p>

<p>
[A] Neen. We willen het voor beheerders makkelijk maken om een
spiegelserver voor www.debian.org op te zetten, dus alstublieft geen speciale
HTTPD-functionaliteit. Neen, zelfs geen SSI (Server Side Includes). Er werd een
uitzondering gemaakt voor "content negotiation", het onderhandelen over inhoud,
omdat dit de enige robuuste manier is om de pagina's in verschillende talen aan
te bieden.
</p>
