#use wml::debian::template title="Toestand van de lokalisatie van Debconf-sjablonen met PO-bestanden voor de taalcode: @tmpl_lang@"
#include '$(ENGLISHDIR)/international/l10n/dtc.def'
#use wml::debian::translation-check translation="8536cf419447d00f034a8e3ad9efa6a243462fe7"

#include "$(ENGLISHDIR)/international/l10n/po-debconf/menu.inc"

<p><podebconf-langs-short>

<h2>Toestand</h2>
<p>
 @tmpl_lang_stats@ tekstfragmenten werden naar deze taal vertaald
 (from <podebconf-total-strings>). De pakketten worden onderverdeeld in drie groepen:
 <a href="#todo">de vertaling is aan de gang</a>,
 <a href="#done">de vertaling is up-to-date</a> en
 <a href="#i18n">de vertaling moet nog gebeuren</a>.
</p>

<p>
 Pakketten worden voorafgegaan door een uitroepteken als ze fouten bevatten die
 verband houden met po-debconf. In dergelijke gevallen zouden vertalers eerst
 moeten nagaan of de <a href="errors-by-pkg">fouten</a> niet specifiek zijn
 voor één taal. Anders moeten ze de fout rapporteren en aan andere pakketten
 werken totdat het defecte pakket is hersteld.
</p>

<p>
 In elke tabel worden de pakketten geordend volgens hun
 <a href="https://popcon.debian.org/source/by_inst">popcon-score</a>,
 zodat vertalers zich kunnen concentreren op de populairste pakketten.
</p>

<p>
 Voordat u debconf-sjablonen van een pakket vertaalt of bijwerkt, moet u
 controleren of ze momenteel niet worden
 <a href="https://l10n.debian.org/coordination/english/en.by_status.html">\
 gereviseerd</a>. Dit revisieproces zal waarschijnlijk tot veranderingen aan de
 oorspronkelijke tekstfragmenten leiden. Dus wacht u best tot dit proces is
 voltooid, en begint u dan met uw vertaling. Als u deze sjablonen al heeft
 vertaald, wordt u aan het einde van het revisieproces op de hoogte gesteld om
 uw vertaling bij te werken. Als de revisie is gemarkeerd met de status
 <tt>bts</tt>, moet u uw vertaling alleen baseren op de sjablonen die in dit
 bugrapport staan vermeld.
</p>

<h2><a name="todo">Pakketten met ondersteuning voor po-debconf en waarvan de
 vertaling aan de gang is</a></h2>
<p>
  U kunt deze PO-bestanden bijwerken en deze als bugrapport
  bij de pakketbeheerders indienen. Om aan een nieuwe vertaling te beginnen moet
  u kijken in deze <a href="pot">lijst van pakketten</a> die dergelijke
  PO-bestanden bevatten.
</p>

#include '$(ENGLISHDIR)/international/l10n/po-debconf/todo.inc'

<h2><a name="done">Pakketten met ondersteuning voor po-debconf en waarvan de
 vertaling afgewerkt is</a></h2>

#include '$(ENGLISHDIR)/international/l10n/po-debconf/ok.inc'

<h2><a name="i18n">Pakketten met ondersteuning voor po-debconf en die nog
 vertaald moeten worden</a></h2>
<p>
  De eventueel hieronder vermelde pakketten bevatten Debconf-sjablonen die via
  po-debconf gelokaliseerd worden, maar momenteel nog niet naar deze taal
  vertaald werden.
</p>

#include '$(ENGLISHDIR)/international/l10n/po-debconf/untranslated.inc'

<p><podebconf-langs-short>

#include "$(ENGLISHDIR)/international/l10n/date.gen"
