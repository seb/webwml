#use wml::debian::template title="Kwetsbaarheid van GRUB2 UEFI SecureBoot - 'Opstartlek BootHole'"
#use wml::debian::translation-check translation="9c47c948da90cea57112872aa23f03da3a967d7b"

<p>
Ontwikkelaars binnen Debian en elders in de Linux-gemeenschap zijn zich
onlangs bewust geworden van een ernstig probleem in het GRUB2-opstartprogramma
waardoor iemand met slechte bedoelingen UEFI Secure Boot (UEFI veilige
opstart) volledig kan omzeilen. Alle details van het probleem worden beschreven
in <a href="$(HOME)/security/2020/dsa-4735">Debian beveiligingsadvies
4735</a>. Het doel van dit document is uitleggen wat de gevolgen zijn van deze
veiligheidskwetsbaarheid en welke stappen gezet werden om deze aan te pakken.
</p>

<ul>
  <li><b><a href="#what_is_SB">Achtergrond: Wat is UEFI Secure Boot?</a></b></li>
  <li><b><a href="#grub_bugs">Meerdere bugs aangetroffen in GRUB2</a></b></li>
  <li><b><a href="#linux_bugs">Ook in Linux werden bugs aangetroffen</a></b></li>
  <li><b><a href="#revocations">Er moeten sleutels ingetrokken worden om de Secure-Boot-keten te repareren</a></b></li>
  <li><b><a href="#revocation_problem">Welke effecten heeft het intrekken van de sleutels?</a></b></li>
  <li><b><a href="#package_updates">Bijgewerkte pakketten</a></b>
  <ul>
    <li><b><a href="#grub_updates">1. GRUB2</a></b></li>
    <li><b><a href="#linux_updates">2. Linux</a></b></li>
    <li><b><a href="#shim_updates">3. Shim</a></b></li>
    <li><b><a href="#fwupdate_updates">4. Fwupdate</a></b></li>
    <li><b><a href="#fwupd_updates">5. Fwupd</a></b></li>
  </ul></li>
  <li><b><a href="#buster_point_release">Debian tussenrelease 10.5 (<q>buster</q>)
        met bijgewerkte installatie- en live-media</a></b></li>
  <li><b><a href="#more_info">Meer informatie</a></b></li>
</ul>

<h1><a name="what_is_SB">Achtergrond: Wat is UEFI Secure Boot?</a></h1>

<p>
UEFI Secure Boot (SB - UEFI veilige opstart) is een verificatiemechanisme
dat moet verzekeren dat de code die door de UEFI firmware van een computer
gelanceerd wordt, te vertrouwen is. Het is ontworpen om het systeem te
beschermen tegen het laden en uitvoeren van kwaadaardige code vroegtijdig in
het opstartproces, nog voordat het besturingssysteem geladen wordt.
</p>

<p>
SB werkt met cryptografische controlegetallen en handtekeningen. Elk programma
dat door de firmware geladen wordt heeft een handtekening en een controlegetal
en voordat de uitvoering van het programma toegestaan wordt, zal de firmware
nagaan of het betrouwbaar is door de geldigheid van het controlegetal en de
handtekening te controleren. Wanneer SB op het systeem geactiveerd is, zal een
poging om een onbetrouwbaar programma uit te voeren niet toegestaan worden. Dit
voorkomt het uitvoeren van onverwachte/ongeoorloofde code in een UEFI-omgeving.
</p>

<p>
De meeste x86-hardware wordt door de fabrikant geleverd met Microsoft-sleutels.
Dit betekent dat de firmware op deze systemen binaire code zal vertrouwen die
ondertekend werd door Microsoft. De meeste moderne systemen worden geleverd met
een geactiveerd SB-systeem - standaard zullen zij geen niet-ondertekende code
uitvoeren, maar het is mogelijk om de firmware-configuratie aan te passen om SB
uit te schakelen of om extra ondertekeningssleutels op te nemen.
</p>

<p>
Zoals veel op Linux gebaseerde besturingssystemen, gebruikt Debian het
programma shim om dat vertrouwen van de firmware uit te breiden naar de andere
programma's die beveiligd moeten worden tijdens de vroege stadia van het
opstartproces: de GRUB2-bootloader, de Linux-kernel en hulpmiddelen voor het
opwaarderen van firmware (fwupd en fwupdate).
</p>

<h1><a name="grub_bugs">Meerdere bugs aangetroffen in GRUB2</a></h1>

<p>
Jammer genoeg werd een ernstige bug aangetroffen in de code van de
GRUB2-bootloader die de configuratie (grub.cfg) leest en ontleedt. Deze bug
verbreekt de vertrouwensketen. Door het exploiteren van deze bug wordt het
mogelijk uit de beveiligde omgeving uit te breken en niet-ondertekende
programma's te laden tijdens de eerste fases van het opstartproces. Deze
kwetsbaarheid werd ontdekt door onderzoekers van Eclypsium en zij gaven het de
naam
<b><a href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">BootHole</a></b>.
</p>

<p>
In plaats van enkel deze ene bug te repareren, werden ontwikkelaars aangespoord
om een diepgaande audit van de broncode van GRUB2 uit te voeren. Het ware
onverantwoord geweest om enkel één belangrijk probleem op te lossen zonder ook
op zoek te gaan naar mogelijke andere problemen. Een team  van ingenieurs heeft
verschillende weken samengewerkt om een reeks andere problemen te identificeren
en op te lossen. We hebben een paar plaatsen gevonden waar interne
geheugentoewijzingen kunnen overlopen bij onverwachte invoer, ook een aantal
plaatsen waar gehele getallen in wiskundige berekeningen zouden kunnen
overlopen en problemen zouden kunnen veroorzaken en enkele plaatsen waar
geheugen kan worden gebruikt nadat het is vrijgegeven. Oplossingen voor al deze
problemen werden binnen de gemeenschap uitgewisseld en getest.
</p>

<p>
Nogmaals, raadpleeg <a href="$(HOME)/security/2020/dsa-4735">Debian
beveiligingsadvies 4735</a> voor een volledige lijst van aangetroffen problemen.
</p>


<h1><a name="linux_bugs">Ook in Linux werden bugs aangetroffen</a></h1>

<P>
Tijdens het bespreken van de GRUB2-gebreken, spraken ontwikkelaars ook over
twee sluipwegen die onlangs aangetroffen en gerepareerd werden door Jason A.
Donenfeld (zx2c4)
(<a href="https://git.zx2c4.com/american-unsigned-language/tree/american-unsigned-language.sh">1</a>, <a href="https://git.zx2c4.com/american-unsigned-language/tree/american-unsigned-language-2.sh">2</a>),
waarbij ook Linux mogelijk het omzeilen van Secure Boot zou toelaten. Deze twee
sluipwegen laten de systeembeheerder toe op een vergrendeld systeem
ACPI-tabellen te vervangen, waar dit niet zou mogen. Reparaties voor deze
problemen werden reeds uitgebracht.
</p>

<h1><a name="revocations">Er moeten sleutels ingetrokken worden om de Secure-Boot-keten te repareren</a></h1>

<p>
Debian en andere leveranciers van besturingssystemen zullen ongetwijfeld <a
href="#package_updates">gerepareerde versies uitbrengen</a> van GRUB2 en
Linux. Dit houdt echter geen volledige reparatie in van de hier gesignaleerde
problemen. Kwaadwillige personen zouden nog steeds gebruik kunnen maken van
oudere versies van deze programma's om Secure Boot te omzeilen.
</p>

<p>
Om dat te stoppen, is de volgende stap dat Microsoft die onveilige binaire
bestanden blokkeert om te voorkomen dat ze onder SB worden uitgevoerd. Dit
is mogelijk via de <b>DBX</b>-lijst, een functionaliteit van het UEFI Secure
Boot-ontwerp. Aan alle Linux-distributies die door Microsoft ondertekende
versies van shim bevatten, werd gevraagd details over te maken van de
betreffende programma's en sleutels om dit proces te faciliteren. Het <a
href="https://uefi.org/revocationlistfile">bestand met de lijst van
UEFI-intrekkingen</a> zal bijgewerkt worden om deze informatie op te nemen. Op
<b>een bepaald</b> moment in de toekomst zullen systemen deze bijgewerkte lijst
beginnen gebruiken en weigeren de kwetsbare programma's nog langer onder Secure
Boot uit te voeren.
</p>

<p>
De <i>exacte</i> tijdlijn voor het ontplooien van deze aanpassingen is nog niet
bekend. Ergens in de toekomst zullen BIOS/UEFI-leveranciers deze nieuwe lijst
met intrekkingen beginnen gebruiken in recentere firmware-versies voor nieuwe
hardware. <b>Mogelijk</b> zal ook Microsoft updates uitbrengen voor bestaande
systemen via Windows Update. Mogelijk zullen bepaalde Linux-distributies
updates uitbrengen via hun eigen proces van beveiligingsupdates.
<b>Momenteel</b> doet Debian dat nog niet, maar we bekijken het voor de
toekomst.
</p>

<h1><a name="revocation_problem">Welke effecten heeft het intrekken van de sleutels?</a></h1>

<p>
De meeste leveranciers wantrouwen het automatisch toepassen van updates die
sleutels intrekken die voor Secure Boot worden gebruikt. Voor SB geschikt
gemaakte bestaande software-installaties zouden mogelijk plots helemaal niet
meer kunnen opstarten, tenzij de gebruiker er zorg voor gedragen heeft om ook
alle nodige software-updates te installeren. Windows/Linux dual-bootsystemen
zouden plots niet meer in staat kunnen zijn om Linux op te starten. Ook oude
installaties en live-systemen zouden niet langer kunnen opstarten, wat het
potentieel moeilijker zou maken om systemen te herstellen.
</p>

<p>
Er zijn twee voor de hand liggende manieren om een dergelijk niet-opstartend
systeem te repareren:
</p>

<ul>
  <li>Herstarten in <q>rescue</q>-modus (reparatiemodus)
    met <a href="#buster_point_release">recentere installatiemedia</a> en
    de noodzakelijke updates op die manier toepassen; of</li>
  <li>Secure Boot tijdelijk uitzetten om opnieuw toegang te krijgen tot
    het systeem, de updates toepassen en dan Secure Boot opnieuw aanzetten.</li>
</ul>

<p>
Beide mogelijkheden kunnen eenvoudig lijken, maar allebei kunnen ze tijdrovend
zijn voor gebruikers die verschillende systemen moeten onderhouden. U dient ook
te weten dat voor het uit- en aanzetten van Secure Boot rechtstreekse toegang
tot de machine nodig is. Dit is zo bedoeld. Normaal is het <b>onmogelijk</b> om
deze configuratie aan te passen buiten het instellingenprogramma van de
firmware om. Net om deze reden dient men mogelijk extra zorg te besteden aan
servercomputers die op een andere locatie staan.
</p>

<p>
Om deze redenen wordt <b>alle</b> Debian-gebruikers ten zeerste aanbevolen
ervoor te zorgen dat alle <a href="#package_updates">aanbevolen updates</a> zo
snel mogelijk op hun systeem geïnstalleerd worden om de kans op problemen in de
toekomst te verkleinen.
</p>

<h1><a name="package_updates">Bijgewerkte pakketten</a></h1>

<p>
<b>Opmerking:</b> Systemen met Debian 9 (<q>stretch</q>) of ouder
zullen hier <b>niet</b> noodzakelijk updates krijgen, omdat Debian 10
(<q>buster</q>) de eerste release van Debian was met ingebouwde ondersteuning
voor UEFI Secure Boot.
</p>

<p>De ondertekende versies van alle pakketten hier werden bijgewerkt, ook
indien er geen andere aanpassingen nodig waren. Debian heeft een nieuw(e)
ondertekeningssleutel/certificaat moeten genereren voor zijn eigen Secure
Boot-pakketten. Het oude certificaat had het label <q>Debian Secure Boot
Signer</q> (vingerafdruk
<code>f156d24f5d4e775da0e6a9111f074cfce701939d688c64dba093f97753434f2c</code>);
het nieuwe certificaat is <q>Debian Secure Boot Signer 2020</q>
(<code>3a91a54f9f46a720fe5bbd2390538ba557da0c2ed5286f5351fe04fff254ec31</code>). </p>

<p>
Er zijn vijf broncodepakketten in Debian die zullen worden bijgewerkt vanwege
de hier beschreven UEFI Secure Boot-wijzigingen:
</p>

<h2><a name="grub_updates">1. GRUB2</a></h2>

<p>
Bijgewerkte versies van de GRUB2-pakketten van Debian zijn nu beschikbaar via
het archief debian-security van de stabiele release Debian 10
(<q>buster</q>). Zeer binnenkort zullen gerepareerde versies voor de
ontwikkelingsversies van Debian (unstable en testing) beschikbaar zijn
in het normale Debian archief.
</p>

<h2><a name="linux_updates">2. Linux</a></h2>

<p>
Bijgewerkte versies van de linux-pakketten van Debian zijn nu beschikbaar via
het archief buster-proposed-updates van de stabiele release Debian 10
(<q>buster</q>) en deze zullen opgenomen worden in de aanstaande tussenrelease
10.5. Ook in het Debian-archief voor de ontwikkelingsversies van Debian
(unstable en testing) zijn nieuwe pakketten aanwezig. We hopen tevens
binnenkort gerepareerde pakketten te kunnen uploaden naar buster-backports.
</p>

<h2><a name="shim_updates">3. Shim</a></h2>

<p>
Vanwege de manier waarop Debian's Secure Boot-sleutelbeheer werkt, hoeft Debian
zijn bestaande door Microsoft ondertekende shim-pakketten <b>niet</b> in te
trekken. De ondertekende versies van de shim-helper-pakketten moesten echter
opnieuw gebouwd worden om gebruik te maken van de nieuwe ondertekeningssleutel.
</p>

<p>
Er zijn nu via buster-proposed-updates bijgewerkte versies van de
shim-pakketten van Debian beschikbaar voor de stabiele release Debian 10
(<q>buster</q>) en deze zullen opgenomen worden in de aanstaande tussenrelease
10.5. Ook in het Debian-archief voor ontwikkelingsversies (unstable en testing)
zijn nieuwe pakketten beschikbaar.
</p>

<h2><a name="fwupdate_updates">4. Fwupdate</a></h2>

<p>
Er zijn nu via buster-proposed-updates bijgewerkte versies van de
fwupdate-pakketten van Debian beschikbaar voor de stabiele release Debian 10
(<q>buster</q>) en deze zullen opgenomen worden in de aanstaande tussenrelease
10.5. Reeds een tijd geleden werd fwupdate verwijderd uit unstable en testing
ten gunste van fwupd.
</p>

<h2><a name="fwupd_updates">5. Fwupd</a></h2>

<p>
Er zijn nu via buster-proposed-updates bijgewerkte versies van de
fwupd-pakketten van Debian beschikbaar voor de stabiele release Debian 10
(<q>buster</q>) en deze zullen opgenomen worden in de aanstaande tussenrelease
10.5. Ook in het Debian-archief voor ontwikkelingsversies (unstable en testing)
zijn nieuwe pakketten beschikbaar.
</p>

<h1><a name="buster_point_release">Tussenrelease Debian 10.5 (<q>buster</q>),
bijgewerkte installatie- en live-media</a></h1>

<p>
Het is de bedoeling dat alle hier beschreven oplossingen opgenomen worden in de
tussenrelease Debian 10.5 (<q>buster</q>), welke gepland is voor 1 augustus. Om
die reden is 10.5 een goede keuze voor gebruikers die op zoek zijn naar Debian
installatie- en live-media. Het is mogelijk dat eerdere versies niet langer
zullen werken met Secure Boot wanneer de intrekkingen van de sleutels
uitgevoerd worden.
</p>

<h1><a name="more_info">Meer informatie</a></h1>

<p>
Veel meer informatie over hoe UEFI Secure Boot opgezet is in Debian, is te
vinden in de wiki-pagina's van Debian - zie
<a href="https://wiki.debian.org/SecureBoot">https://wiki.debian.org/SecureBoot</a>.</p>

<p>
Andere informatiebronnen over dit onderwerp zijn onder meer:
</p>

<ul>
  <li><a href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">Het artikel <q>BootHole</q> van Eclypsium</a>, met een beschrijving van
de aangetroffen kwetsbaarheden.</li>
  <li><a href="https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/ADV200011">Microsoft
      richtlijnen voor de aanpak van omzeilingen van de beveiliging in GRUB</a></li>
  <li><a href="https://wiki.ubuntu.com/SecurityTeam/KnowledgeBase/GRUB2SecureBootBypass">Artikel uit de Ubuntu
      KnowledgeBase</a></li>
  <li><a href="https://access.redhat.com/security/vulnerabilities/grub2bootloader">Artikel van Red
      Hat over de kwetsbaarheid</a></li>
  <li><a href="https://www.suse.com/c/suse-addresses-grub2-secure-boot-issue/">Artikel van SUSE
       over de kwetsbaarheid</a></li>
</ul>
