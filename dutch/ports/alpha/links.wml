#use wml::debian::template title="Overzetting naar Alpha -- Links" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/alpha/menu.inc"
#use wml::debian::translation-check translation="b9cb4ebb36a3cc48be8b8b2ac3b06f113491d26f"

<h1>Alpha-links</h1>

<ul>

<li><a href="http://www.alphalinux.org/">AlphaLinux.org</a><br />
Deze pagina bevat vrijwel alles wat u wilt of moet weten over het gebruiken van
Linux op Alpha's. Bevat talrijke links en een uitstekende nieuwssectie. Deze
site is ook de thuisbasis van de officiële
<a href="http://www.alphalinux.org/docs/">FAQ's en HOWTO's</a> in verband met
Linux op Alpha's.</li>

<li><a href="http://www.alphalinux.org/docs/alpha-howto.html">\
Inleiding op Alpha-systemen</a><br />
Een tamelijk oud, maar nog steeds informatief document voor de nieuwe of ervaren
gebruiker. Het beschrijft de verschillen tussen Alpha's en andere architecturen
en beschrijft de verschillende systemen. Als u een oud systeem hebt en u moet
echt weten wat uw systeem is, ondanks wat er op de behuizing staat, raadpleeg
dan
<a href="http://www.alphalinux.org/docs/alpha-howto.html#The%20Systems">deze
subsectie</a>.</b></li>

<li><a href="http://www.alphalinux.org/faq/FAQ.html">Linux/Alpha FAQ</a><br />
Een goede bron van informatie. De Linux/Alpha FAQ werd oorspronkelijk
samengesteld door Red Hat-gebruikers, maar is uitgebreid met meer algemene
informatie en is nog steeds een geweldige referentie.</li>

<li><a href="http://www.alphalinux.org/faq/SRM-HOWTO/index.html">SRM Firmware HOWTO</a><br />
De officiële HOWTO voor systemen die gebruik maken van SRM firmware. Indien u
<a href="https://sourceforge.net/projects/aboot/">aboot</a>
moet gebruiken om uw systeem op te starten, is dit de pagina voor u.
#This HOWTO is included in
#the latest version of the
#<a href="https://tracker.debian.org/pkg/aboot">Debian packet</a>
#as well.
</li>

<li><a href="http://alphalinux.org/faq/MILO-HOWTO/t1.html">MILO HOWTO</a><br />
De officiële HOWTO voor MILO. Merk op dat het
<a href="$(HOME)/devel/debian-installer/">installatiesysteem van Debian</a> geen
ondersteuning biedt voor MILO.
SRM wordt ten zeerste aanbevolen, maar als u niet kunt overschakelen en u bent
geïnteresseerd in MILO-ondersteuning voor releases van Debian na Woody, lees dan
<a href="https://lists.debian.org/debian-alpha/2004/debian-alpha-200402/msg00003.html">dit bericht op debian-alpha</a> en schrijf u in op
<a href="https://lists.debian.org/debian-boot/">debian-boot</a>
om MILO-ondersteuning toe te voegen. De laatst bekende locaties zijn
<a href="http://www.suse.de/~stepan/">de pagina van Stepan Rainauer</a>,
<a href="ftp://genie.ucd.ie/pub/alpha/milo/">Nikita Schmidt</a> en het door
<a href="http://dev.gentoo.org/~taviso/milo/">Gentoo</a> geleverde werk.</li>

<li><a href="http://www.alphalinux.org/faq/alphabios-howto.html">AlphaBIOS Firmware HOWTO</a><br />
De officiële HOWTO voor systemen die gebruik maken van AlphaBIOS firmware.</li>

<li><a href="https://digital.com/about/dec/">Digital's oude documentatiebibliotheek</a> <!-- with a <a href="ftp://ftp.unix-ag.org/user/nils/Alpha-Docs/">Mirror by Nils Faerber</a> --> </li>

<!-- <li><a href="http://www.alphanews.net/">alphanews.net</a><br />
Some alpha related news are posted here, for several OS which run or used
to run on alphas.</li> -->

<li><a href="http://www.helgefjell.de/browser.php">Browsers op Linux Alpha</a><br />
Als u problemen hebt met uw browser in een 64-bits omgeving (zou tegenwoordig
niet meer het geval mogen zijn) of als u gewoon een nieuwe wilt uitproberen, dan
vind u hier een lijst van browsers waarvan geweten is dat ze werken (en niet
werken).</li>

<li><a href="http://alphacore.info/wiki/">AlphaCore Wiki-pagina</a><br />
Deze Wiki, die momenteel voornamelijk gericht is op AlphaCore (Fedora Core op
Alpha), is ook bedoeld om algemeen bruikbare alpha-gerelateerde informatie te
verzamelen.</li>
</ul>

<p>
Met dank aan Nils Faerber voor de toestemming om hier delen van zijn verzameling
links op te nemen.
</p>

<h1><a name="lists">Mailinglijsten</a></h1>

<ul>

<li>De mailinglijst debian-alpha<br />
Stuur een e-mail met als onderwerp 'subscribe' naar
<email "debian-alpha-request@lists.debian.org" /> om in te schrijven.
<a href="https://lists.debian.org/debian-alpha/">Er zijn archieven beschikbaar</a>.</li>

<li>De Linux/Alpha-lijst van Red Hat

<p>
Deze lijst is gericht op Red Hat Linux/Alpha-gebruikers, maar bevat ook
waardevolle tips voor algemene Linux-Alpha-problemen. Ga naar de
<a href="https://www.redhat.com/mailman/listinfo/axp-list">Info-pagina van de
lijst</a> om u te abonneren. Van deze lijst zijn er ook
<a href="https://www.redhat.com/archives/axp-list/">archieven</a>
beschikbaar.
Een ander archief (dat doorzocht kan worden) is te vinden op
<url "http://www.lib.uaa.alaska.edu/axp-list/" />.
</p></li>

</ul>
