#use wml::debian::template title="Debian voor IA-64" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/ia64/menu.inc"
#use wml::debian::translation-check translation="09225985e5d001870fb942c6fb74704fe3fc552a"

<h1>Debian voor IA-64</h1>

<h2>Toestand</h2>

<p>
IA-64 was een door Debian ondersteunde architectuur van Debian 3.0 (woody) tot Debian 7 (wheezy)

<p>
Indien u zou willen helpen, begin dan met intekenen op de
<a href="#mailinglist">mailinglijst debian-ia64</a>.

<p>
De normale kanalen in Debian voor het verkrijgen van installatiemedia en
documentatie hebben ook ia64 cd-images.

<h2>BIOS-versies</h2>

<p>
Alle moderne ia64-systemen zouden goed moeten werken.
<p>
Het is mogelijk dat u een zeer vroege ia64-machine aantreft die een
BIOS-upgrade nodig heeft om goed te werken met Linux.
Een specifieke combinatie waarover we dit hoorden is wanneer u nieuwe
kernels tracht te gebruiken op "Lion" systemen met echt oude BIOS-versies.
Om het u makkelijker te maken volgt hier wat we weten over BIOS-versies
die op oudere ia64-systemen goed werken voor Debian:

<ul>
<li> Lion, ook bekend als HP rx4610: versie 99b werkt goed.
<li> BigSur, ook bekend als HP i2000: versie 130 werkt good.
</ul>

<p>
Firmware-downloads voor
<a href="http://www.hp.com">HP</a>-systemen
zijn beschikbaar vanaf
<a href="http://welcome.hp.com/country/us/eng/software_drivers.htm"> HIER </a>.
</p>

<p>
Indien iemand informatie heeft over geschikte BIOS-versies voor andere
IA-64-systemen waarop Debian succesvol werkt, kan die ons dit laten weten
op de mailinglijst debian-ia64.</p>

<h2>Contactpersonen</h2>

<p>
De initiatiefnemers voor het geschikt maken van Debian voor IA-64 port waren
Bdale Garbee en Randolph Chung.
Nu is de beste manier voor het stellen van vragen via de mailinglijst.

<h2><a name="mailinglist">Mailinglijst</a></h2>

<p>
Om in te tekenen op de mailinglijst voor deze uitgave, kunt u een bericht
sturen met het woord "subscribe" als onderwerp naar
<email "debian-ia64-request@lists.debian.org">, of u kunt de
<a href="https://lists.debian.org/debian-ia64/">webpagina voor mailinglijsten</a> gebruiken.
U kunt ook het
<a href="https://lists.debian.org/debian-ia64/">archief van de mailinglijst</a>
doorbladeren en doorzoeken.
</p>
