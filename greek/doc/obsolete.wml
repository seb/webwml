#use wml::debian::template title="Obsolete Documentation"
#use wml::debian::translation-check translation="4792632f7a20682a368627c66663b57ff1b3fa8b" maintainer="galaxico"

#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/obsolete.defs"

<h1 id="historical">Historical documents</h1>

<p>The documents listed below were either written a long time ago and
are not up to date or have been written for previous versions of Debian
and have not been updated to current versions. Their information is
outdated, but may still be of interest to some.</p>

<p>The documents that have lost their relevance and serve no purpose any
more, have had their references removed, but the source code for many of
these obsolete manuals can be found on the
<a href="https://salsa.debian.org/ddp-team/attic">DDP's attic</a>.</p>


<h2 id="user">User oriented documentation</h2>

<document "dselect Documentation for Beginners" "dselect">

<div class="centerblock">
<p>
  This file documents dselect for first-time users, and is intended to help
  in getting Debian installed successfully. It makes no attempt to explain
  everything, so when you first meet dselect, work through the help screens.
</p>
<doctable>
  <authors "Stéphane Bortzmeyer">
  <maintainer "(?)">
  <status>
  stalled: <a href="https://packages.debian.org/aptitude">aptitude</a> has
  replaced dselect as the standard Debian package management interface
  </status>
  <availability>
  <inddpvcs name="dselect-beginner" formats="html txt pdf ps"
            langs="ca cs da de en es fr hr it ja pl pt ru sk" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "User's Guide" "users-guide">

<div class="centerblock">
<p>
This <q>User's Guide</q> is nothing but a reformatted <q>Progeny User's Guide</q>.
Contents are adjusted for the standard Debian system.</p>

<p>Over 300 pages with a good tutorial to start using the Debian system
from <acronym lang="en" title="Graphical User Interface">GUI</acronym> desktop 
and shell command line.
</p>
<doctable>
  <authors "Progeny Linux Systems, Inc.">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  Useful itself as a tutorial.  Written for the woody release,
  getting obsolete.
  </status>
  <availability>
# langs="en" isn't redundant, it adds the needed language suffix to the link
  <inddpvcs name="users-guide" index="users-guide" langs="en" formats="html txt pdf" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Tutorial" "tutorial">

<div class="centerblock">
<p>
This manual is for a new Linux user, to help such a user get acquainted
with Linux once they have installed it, or for a new Linux user on a system
which someone else is administering.
</p>
<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II">
  <editors "Havoc Pennington">
  <maintainer "(?)">
  <status>
  stalled; incomplete;
  probably obsoleted by <a href="user-manuals#quick-reference">Debian Reference</a>
  </status>
  <availability>
  not yet complete
  <inddpvcs name="debian-tutorial" vcsname="tutorial" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian GNU/Linux: Guide to Installation and Usage" "guide">

<div class="centerblock">
<p>
  A manual, oriented towards the end-user.
</p>
<doctable>
  <authors "John Goerzen, Ossama Othman">
  <editors "John Goerzen">
  <status>
  ready (but it is for potato)
  </status>
  <availability>
  <inoldpackage "debian-guide">
  </availability>
</doctable>
</div>

<hr />

<document "Debian User Reference Manual" "userref">

<div class="centerblock">
<p>
  This manual provides at least an overview of everything a user should know
  about their Debian GNU/Linux system (i.e. setting up X, how to configure
  network, accessing floppy disks, etc.). It is intended to bridge the gap
  between the Debian Tutorial and the detailed manual and info pages
  supplied with each package.</p>

  <p>It is also intended to give some idea of how to combine commands, on
  the general Unix principle, that <em>there is always more than one way to
  do it</em>.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer">
  <editors "Thalia L. Hooker, Oliver Elphick">
  <maintainer "(?)">
  <status>
  stalled and quite incomplete;
  probably obsoleted by <a href="user-manuals#quick-reference">Debian Reference</a>
  </status>
  <availability>
  <inddpvcs name="user" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />


<document "Debian System Administrator's Manual" "system">

<div class="centerblock">
<p>
  This document is mentioned in the introduction of the Policy manual.
  It covers all system administration aspects of a Debian system.
</p>
<doctable>
  <authors "Tapio Lehtonen">
  <maintainer "(?)">
  <status>
  stalled; incomplete;
  probably obsoleted by <a href="user-manuals#quick-reference">Debian Reference</a>
  </status>
  <availability>
  not yet available
  <inddpvcs name="system-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Network Administrator's Manual" "network">

<div class="centerblock">
<p>
  This manual covers all network administration aspects of a Debian system.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson, Ivan E. Moore II">
  <maintainer "(?)">
  <status>
  stalled; incomplete;
  probably obsoleted by <a href="user-manuals#quick-reference">Debian Reference</a>
  </status>
  <availability>
  not yet available
  <inddpvcs name="network-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

# Add this to books, there's a revised edition (2nd) @ Amazon
<document "The Linux Cookbook" "linuxcookbook">

<div class="centerblock">
<p>
  A hands-on reference guide to the Debian GNU/Linux system that shows,
  in over 1,500 <q>recipes</q>, how to use it for everyday activities &mdash; from
  working with text, images, and sound to productivity and networking
  issues. Like the software the book describes, the book is copylefted
  and its source data is available.
</p>
<doctable>
  <authors "Michael Stutz">
  <status>
  published; written for woody, getting obsolete
  </status>
  <availability>
  <inoldpackage "linuxcookbook">
  <p><a href="http://dsl.org/cookbook/">from the author</a>
  </availability>
</doctable>
</div>

<hr />

<document "APT HOWTO" "apt-howto">

<div class="centerblock">
<p>
  This manual tries to be a quick but complete source of information 
  about the APT system and its features. It contains much information 
  about the main uses of APT and many examples.
</p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>
   obsolete since 2009
  </status>
  <availability>
  <inoldpackage "apt-howto">
  <inddpvcs name="apt-howto" langs="en ca cs de es el fr it ja pl pt-br ru uk tr zh-tw zh-cn"
	    formats="html txt pdf ps" naming="locale" vcstype="attic"/>
  </availability>
</doctable>
</div>


<h2 id="devel">Developer documentation</h2>

<document "Introduction: Making a Debian Package" "makeadeb">

<div class="centerblock">
<p>
  Introduction on how to create a <code>.deb</code>, using
  <strong>debmake</strong>.
</p>
<doctable>
  <authors "Jaldhar H. Vyas">
  <status>
  stalled, obsoleted by <a href="devel-manuals#maint-guide">New Maintainers' Guide</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">HTML online</a>
  </availability>
</doctable>
</div>

<hr />

<document "Debian Programmers' Manual" "programmers">

<div class="centerblock">
<p>
  Helps new developers to create a package for the Debian GNU/Linux system.
</p>
<doctable>
  <authors "Igor Grobman">
  <status>
  obsoleted by <a href="devel-manuals#maint-guide">New Maintainers' Guide</a>
  </status>
  <availability>
  <inddpvcs name="programmer" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Packaging Manual" "packman">

<div class="centerblock">
<p>
  This manual describes the technical aspects of creating Debian binary
  and source packages. It also documents the interface between dselect
  and its access method scripts. It does not deal with the Debian
  Project policy requirements, and it assumes familiarity with dpkg's
  functions from the system administrator's perspective.

<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  Parts that were de facto policy were merged into
  <a href="devel-manuals#policy">debian-policy</a>.
  </status>
  <availability>
  <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>

<hr />

<document "How Software Producers can distribute their products directly in .deb format" "swprod">

<div class="centerblock">
<p>
  This document is intended as a starting point to explain how software
  producers can integrate their products with Debian, what different
  situations can arise depending on the license of the products and the
  choices of the producers, and what possibilities there are. It does not
  explain how to create packages, but it links to documents which do exactly
  that.

  <p>You should read this if you are not familiar with the big picture of
  creating and distributing Debian packages, and optionally with adding them
  to the Debian distribution.

<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  obsolete
  </status>
  <availability>
  <inddpvcs-distribute-deb>
  </availability>
</doctable>
</div>

<hr />

<document "Introduction to i18n" "i18n">

<div class="centerblock">
<p>
  This document describes basic idea and howto of l10n (localization),
  i18n (internationalization), and m17n (multilingualization) for
  programmers and package maintainers.

  <p>The aim of this document is to make more packages support i18n
  and to make Debian a more internationalized distribution.
  Contributions from all over the world will be welcome, because
  the original author is Japanese-speaker and this document would be
  on Japanization if there were no contributions.

<doctable>
  <authors "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <maintainer "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <status>
  stalled, obsolete
  </status>
  <availability>
  not yet complete
  <inddpvcs-intro-i18n>
  </availability>
</doctable>
</div>

<hr />

<document "Debian SGML/XML HOWTO" "sgml-howto">

<div class="centerblock">
<p>
  This HOWTO contains practical information about the use of SGML and XML
  on a Debian operating system.

<doctable>
  <authors "Stephane Bortzmeyer">
  <maintainer "Stephane Bortzmeyer">
  <status>
  stalled, obsolete
  </status>
  <availability>

# English only using index.html, so langs set.
  <inddpvcs name="sgml-howto"
            formats="html"
            srctype="SGML"
            vcstype="attic"
/>
  </availability>
</doctable>
</div>

<hr />

<document "Debian XML/SGML Policy" "xml-sgml-policy">

<div class="centerblock">
<p>
  Subpolicy for Debian packages that provide and/or make use
  of XML or SGML resources.

<doctable>
  <authors "Mark Johnson, Ardo van Rangelrooij, Adam Di Carlo">
  <status>
  dead
  </status>
  <availability>
  <inddpvcs-xml-sgml-policy>
  </availability>
</doctable>
</div>

<hr />

<document "DebianDoc-SGML Markup Manual" "markup">

<div class="centerblock">
<p>
  Documentation for the <strong>debiandoc-sgml</strong> system,
  including best practices and hints for maintainers. Future versions
  should includes tips for easier maintenance and building of
  documentation in Debian packages, guidelines for organizing
  translation of documentation, and other helpful information.
  See also <a href="https://bugs.debian.org/43718">bug #43718</a>.

<doctable>
  <authors "Ian Jackson, Ardo van Rangelrooij">
  <maintainer "Ardo van Rangelrooij">
  <status>
  ready
  </status>
  <availability>
  <inpackage "debiandoc-sgml-doc">
  <inddpvcs-debiandoc-sgml-doc>
  </availability>
</doctable>
</div>

<h2 id="misc">Miscellaneous documentation</h2>

<document "Debian Repository HOWTO" "repo">

<div class="centerblock">
<p>
  This document explains how Debian repositories work, how to create
  them, and how to add them to the <tt>sources.list</tt> correctly.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  ready (?)
  </status>
  <availability>
  <inddpvcs name="repository-howto" index="repository-howto"
            formats="html" langs="en fr de uk ta" vcstype="attic">
  </availability>
</doctable>
</div>
