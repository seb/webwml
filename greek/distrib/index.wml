#use wml::debian::template title="Αποκτώντας το Debian"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="0691e0b35ed0aa5df10a4b47799051f77e519d25" maintainer="galaxico"

<p>Το Debian διανέμεται <a href="../intro/free">ελεύθερα</a>
στο Διαδίκτυο. Μπορείτε να το μεταφορτώσετε ολόκληρο από οποιονδήποτε από τους 
<a href="ftplist">καθρέφτες</a> μας.
Το <a href="../releases/stable/installmanual">Εγχειρίδιο 
Εγκατάστασης</a> περιέχει λεπτομερείες οδηγίες για την εγκατάσταση.
Και οι σημειώσεις της έκδοσης μπορούν να βρεθούν <a 
href="../releases/stable/releasenotes">εδώ</a>.
</p>


<p>Αυτή η σελίδα έχει επιλογές για την εγκατάσταση της σταθερής έκδοσης του Debian. Αν ενδιαφέρεστε για την Δοκιμαστική ή την Ασταθή έκδοση, 
επισκεφθείτε την σελίδα των <a href="../releases/">εκδόσεων</a>.</p>
  
  <div class="line">
  <div class="item col50">
    <h2><a href="netinst">Μεταφορτώστε μια εικόνα για την εγκατάσταση</a></h2>
    <p>Ανάλογα με την σύνδεσή σας στο Διαδίκτυο, μπορείτε να μεταφορτώσετε ένα 
από τα ακόλουθα:</p>
    <ul>
      <li>Μια <a href="netinst"><strong>μικρή εικόνα εγκατάστασης</strong></a>:
	    μπορεί να μεταφορτωθεί γρήγορα και θα πρέπει να εγγραφεί σε έναν 
αφαιρέσιμο δίσκο. Για να την χρησιμοποιήσετε, θα χρειαστείτε μια σύνδεση στο 
Διαδίκτυο.
	<ul class="quicklist downlist">
	  <li><a title="Download installer for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bit
	      PC netinst iso</a></li>
	  <li><a title="Download installer for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bit
	      PC netinst iso</a></li>
	</ul>
      </li>
      <li>Μια μεγαλύτερη <a href="../CD/"><strong>πλήρη εικόνα 
εγκατάστασης </strong></a>: περιέχει περισσότερα πακέτα, κάνοντας ευκολότερη 
την εγκατάσταση σε μηχανήματα χωρίς σύνδεση στο Διαδίκτυο.
	<ul class="quicklist downlist">
	  <li><a title="Download DVD torrents for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/bt-dvd/">64-bit PC torrents (DVD)</a></li>
	  <li><a title="Download DVD torrents for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/bt-dvd/">32-bit PC torrents (DVD)</a></li>
	  <li><a title="Download CD torrents for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/bt-cd/">64-bit PC torrents (CD)</a></li>
	  <li><a title="Download CD torrents for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/bt-cd/">32-bit PC torrents (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
  <h2><a href="https://cloud.debian.org/images/cloud/">Χρησιμοποιήστε μια εικόνα του Debian για το Νέφος</a></h2>
    <p>Μια επίσημη <a href="https://cloud.debian.org/images/cloud/"><strong>εικόνα για το Νέφος </strong></a>, μεταγλωττισμένη από την ομάδα
    Νέφου, μποεί να χρησιμοποιηθεί στον:</p>
    <ul>
      <li>στον OpenStack πάροχό σας, σε μορφή qcow2 ή raw.
      <ul class="quicklist downlist">
	   <li>64-bit AMD/Intel (<a title="OpenStack image for 64-bit AMD/Intel qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2">qcow2</a>, <a title="OpenStack image for 64-bit AMD/Intel raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.raw">raw</a>)</li>
       <li>64-bit ARM (<a title="OpenStack image for 64-bit ARM qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.qcow2">qcow2</a>, <a title="OpenStack image for 64-bit ARM raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.raw">raw</a>)</li>
	   <li>64-bit Little Endian PowerPC (<a title="OpenStack image for 64-bit Little Endian PowerPC qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.qcow2">qcow2</a>, <a title="OpenStack image for 64-bit Little Endian PowerPC raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2, είτε ως μια εικόνα μηχανής ή μέσω του AWS Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Amazon Machine Images" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Amazon Machine Images</a></li>
	    <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
	   </ul>
      </li>
      <li>Microsoft Azure, στο Azure Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Debian 11 on Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
        <li><a title="Debian 10 on Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-10?tab=PlansAndPrice">Debian 10 ("Buster")</a></li>
	   </ul>
      </li>
    </ul>
    </div>
</div>
<div class="line">
  <div class="item col50">
  <h2><a href="../CD/vendors/">Αγοράστε ένα σετ από CD ή DVD από έναν από τους
      προμηθευτές που πουλάνε CD του Debian</a></h2>

<p>
      Αρκετοί προμηθευτές πουλάνε τη διανομή για λιγότερα από 5 δολάρια συν το 
κόστος της αποστολής (ελέγξτε την ιστοσελίδα τους για να δείτε αν κάνουν 
διεθνείς αποστολές).
      <br />
      Μερικά από τα <a href="../doc/books">βιβλία σχετικά με το 
Debian</a> έρχονται επίσης με CD.
   </p>


<p>Αυτά είναι τα βασικά πλεονεκτήματα των CD:</p>

   <ul>
     <li>Η εγκατάσταση από ένα CD είναι πιο άμεση.</li>
     <li>Μπορείτε να εγκασταστήσετε σε μηχανήματα χωρίς σύνδεση στο 
Διαδίκτυο.</li>
	 <li>Μπορείτε να εγκαταστήσετε το Debian (σε όσα μηχανήματα θέλετε) 
χωρίς να μεταφορτώσετε οι ίδιοι/ες όλα τα πακέτα.</li>
     <li>Το CD μπορεί να χρησιμοποιηθεί πιο εύκολα για να διασώσετε ένα 
σύστημα Debian που έχει υποστεί κάποια ζημιά.</li>
   </ul>

  <h2><a href="pre-installed">Αγοράστε έναν υπολογιστή με προεγκατεστημένο το 
Debian
       </a></h2>
   <p>Αυτό έχει έναν αριθμό πλεονεκτημάτων:</p>
   <ul>
    <li>Δεν χρειάζεται να εγκαταστήσετε εσείς το Debian.</li>
    <li>Η εγκατάσταση έχει από πριν ρυθμιστεί έτσι ώστε να ταιριάζει στο υλικό 
του υπολογιστή.</li>
    <li>Ο προμηθευτής ενδέχεται να προσφέρει τεχνική υποστήριξη.</li>
   </ul>
  </div>

 <div class="item col50 lastcol">
<h2><a href="../CD/live/">Δοκιμάστε το Debian "ζωντανά" πριν το εγκαταστήσετε</a></h2>
    <p>
      Μπορείτε να δοκιμάσετε το Debian εκκινώντας ένα "ζωντανό" (live) σύστημα 
      από ένα CD, DVD ή κλειδί USB χωρίς να εγκαταστήσετε οποιαδήποτε αρχεία 
      στον υπολογιστή σας. Όταν είστε έτοιμοι/ες μπορείτε να εκτελέσετε τον 
      εγκαταστάτη που συμπεριλαμβάνεται (ξεκινώντας με το Debian 10 Buster, αυτός είναι ο φιλικός για
      τον τελικό χρήστη <a href="https://calamares.io">Εγκαταστάτης Calamares</a>).
      Εφόσον το μέγεθος των διαθέσιμων 
    εικόνων εγκατάστασης, η γλώσσα και η επιλογή πακέτων ικανοποιούν τις 
    απαιτήσεις σας αυτή η μέθοδος ίσως είναι κατάλληλη για σας.      
      Διαβάστε περισσότερα στη σελίδα <a href="../CD/live#choose_live">πληροφορίες σχετικά με αυτή τη 
      μέθοδο</a>
      που θα σας βοηθήσουν να αποφασίσετε.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Download live torrents for 64-bit Intel and AMD PC"
	     href="<live-images-url/>/amd64/bt-hybrid/">64-bit PC live torrents</a></li>
      <li><a title="Download live torrents for normal 32-bit Intel and AMD PC"
	     href="<live-images-url/>/i386/bt-hybrid/">32-bit PC live torrents</a></li>
    </ul>
  </div>
  
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Αν οποιοδήποτε υλικό στο σύστημά σας <strong>απαιτεί την φόρτωση μη ελεύθερου υλισμικού</strong>
με τους οδηγούς συσκευών, μπορείτε να χρησιμοποιήσετε ένα από τα
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
συμπιεσμένα αρχεία πακέτων υλισμικού</a> ή κατεβάστε μια <strong>ανεπίσημη</strong> εικόνα
που περιλαμβάνει αυτό το <strong>μη ελεύθερο</strong> υλισμικό. Οδηγίες για το πώς να
χρησιμοποιήσετε αυτά τα συμπιεσμένα αρχεία και γενικές πληροφορίες σχετικά με τη φόρτωση υλισμικού
σε μια εγκατάσταση μπορούν να βρεθούν στον <a href="../releases/stable/amd64/ch06s04">Οδηγό Εγκατάστασης</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">ανεπίσημες εικόνες
εγκατάστασης για την  <q>σταθερή</q> έκδοση που περιλαμβάνουν υλισμικό</a>
</p>
</div>
