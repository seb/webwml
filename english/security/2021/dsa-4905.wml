<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the Shibboleth Service Provider is prone to a
NULL pointer dereference flaw in the cookie-based session recovery
feature. A remote, unauthenticated attacker can take advantage of this
flaw to cause a denial of service (crash in the shibd daemon/service).</p>

<p>For additional information please refer to the upstream advisory at
<a href="https://shibboleth.net/community/advisories/secadv_20210426.txt">https://shibboleth.net/community/advisories/secadv_20210426.txt</a></p>

<p>For the stable distribution (buster), this problem has been fixed in
version 3.0.4+dfsg1-1+deb10u2.</p>

<p>We recommend that you upgrade your shibboleth-sp packages.</p>

<p>For the detailed security status of shibboleth-sp please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/shibboleth-sp">https://security-tracker.debian.org/tracker/shibboleth-sp</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4905.data"
# $Id: $
