#use wml::debian::template title="Contribute: How you can help Debian" MAINPAGE="true"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#coding">Coding and Maintaining Packages</a></li>
    <li><a href="#testing">Testing and Bug Squashing</a></li>
    <li><a href="#documenting">Writing Documentation and Tagging Packages</a></li>
    <li><a href="#translating">Translating and Localizing</a></li>
    <li><a href="#usersupport">Helping other Users</a></li>
    <li><a href="#events">Organizing Events</a></li>
    <li><a href="#donations">Donate Money, Hardware, or Bandwidth</a></li>
    <li><a href="#usedebian">Use Debian</a></li>
    <li><a href="#organizations">How your Organization can support Debian</a></li>
  </ul>
</div>

<p>Debian is not just an operating system, it's a community. A lot
of people with many different skills contribute to the project: our
software, the artwork, the wiki, and other documentation are the results
of a joint effort by a large group of individuals. Not everyone is a
developer, and you certainly don't have to know how to code if you want
to participate. There are many different ways you can help to make Debian
even better. If you'd like to join in, here are some suggestions for both
experienced and inexperienced users.</p>

<h2><a id="coding">Coding and Maintaining Packages</a></h2>

<aside class="light">
  <span class="fa fa-code-branch fa-5x"></span>
</aside>

<p>Maybe you want to write a new application from scratch, maybe you want
to implement a new feature in an existing program. If you're a developer
and want to contribute to Debian, you can also help us to prepare the
software in Debian for easy installation, we call it "packaging". Have
a look at this list for some ideas on how to get started:</p>

<ul>
  <li>Package applications, for example those you have experience with or consider valuable for Debian. For more information on how to become a package maintainer, visit the <a href="$(HOME)/devel/">Debian Developer's Corner</a>.</li>
  <li>Help to maintain existing applications, for example by contributing fixes (patches) or additional information in the <a href="https://bugs.debian.org/">Bug Tracking System</a>. Alternatively, you can join a group maintenance team or join a software project on <a href="https://salsa.debian.org/">Salsa</a> (our own GitLab instance).</li>
  <li>Assist us by <a href="https://security-tracker.debian.org/tracker/data/report">tracking</a>, <a href="$(HOME)/security/audit/">finding</a>, and <a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">fixing</a> <a href="$(HOME)/security/">security issues</a> in Debian.</li>
  <li>You can also help with the hardening of <a href="https://wiki.debian.org/Hardening">packages</a>, <a href="https://wiki.debian.org/Hardening/RepoAndImages">repositories and images</a>, and <a href="https://wiki.debian.org/Hardening/Goals">other components</a>.</li>
  <li>Interested in <a href="$(HOME)/ports/">porting</a> Debian to some architecture you are familiar with? You can start a new port or contribute to an existing one. </li>
  <li>Help us to improve Debian-related <a href="https://wiki.debian.org/Services">services</a> or create and maintain new ones, <a href="https://wiki.debian.org/Services#wishlist">suggested or requested</a> by the community.</li>
</ul>

<h2><a id="testing">Testing and Bug Squashing</a></h2>

<aside class="light">
  <span class="fa fa-bug fa-5x"></span>
</aside>

<p>Like any other software project, Debian needs users who test the
operating system and its applications. One way to contribute is to install
the latest version and report back to the developers if something doesn't
work the way it should. We also need people to test our installation media,
secure boot and the U-Boot bootloader on different hardware.
</p>

<ul>
  <li>You can use our <a href="https://bugs.debian.org/">Bug Tracking System</a> to report any issues you find in Debian. Before you do so, please make sure the bug hasn't already been reported.</li>
  <li>Visit the bug tracker and try to browse the bugs associated with packages you use. See if you can provide further information and reproduce the issues described.</li>
  <li>Test the Debian <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">installer and live ISO images</a>, <a href="https://wiki.debian.org/SecureBoot/Testing">secure boot support</a>, <a href="https://wiki.debian.org/LTS/TestSuites">LTS updates</a>, and the <a href="https://wiki.debian.org/U-boot/Status">U-Boot</a> bootloader.</li>
</ul>

<h2><a id="documenting">Writing Documentation and Tagging Packages</a></h2>

<aside class="light">
  <span class="fa fa-file-alt fa-5x"></span>
</aside>

<p>If you experience any problems in Debian and can't write code to
solve the issue, maybe taking notes and writing down your solution is an
option for you. That way you can help other users who may have similar
problems. All Debian documentation is written by community members and
there are several ways you can help.</p>

<ul>
  <li>Join the <a href="$(HOME)/doc/ddp">Debian Documentation Project</a> to help with the official Debian documentation.</li>
  <li>Contribute to the <a href="https://wiki.debian.org/">Debian Wiki</a></li>
  <li>Tag and categorize packages on the <a href="https://debtags.debian.org/">Debtags</a> website, so that Debian users can easily find the software they are looking for.</li>
</ul>


<h2><a id="translating">Translating and Localizing</a></h2>

<aside class="light">
  <span class="fa fa-language fa-5x"></span>
</aside>

<p>
Your native language isn't English, but you have good enough
English language skills to understand and translate software or
Debian-related information like web pages, documentation, etc.? Why not
join a translation team and convert Debian applications to your mother
tongue. We're also looking for people to check existing translations
and file bug reports if necessary.
</p>

# Translators, link directly to your group's pages
<ul>
  <li>Everything connected with Debian's internationalization is discussed on the <a href="https://lists.debian.org/debian-i18n/">i18n mailing list</a>.</li>
  <li>Are you a native speaker of a language which isn't supported in Debian yet? Get in touch via the <a href="$(HOME)/international/">Debian International</a> page.</li>
</ul>

<h2><a id="usersupport">Helping other Users</a></h2>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>

<p>You can also contribute to the project by helping other Debian
users. The project uses various support channels, for example mailing
lists in different languages, and IRC channels. For more information,
please visit our <a href="$(HOME)/support">support pages</a>.</p>

# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language
<ul>
  <li>The Debian project uses a lot of different <a href="https://lists.debian.org/">mailing lists</a>; some are for developers and some are for users. Experienced users can help others through the <a href="$(HOME)/support#mail_lists">user mailing lists</a>.</li>
  <li>People from all over the world chat in real-time on IRC (Internet Relay Chat). Visit the channel <tt>#debian</tt> on <a href="https://www.oftc.net/">OFTC</a> to chat with other Debian users.</li>
</ul>

<h2><a id="events">Organizing Events</a></h2>

<aside class="light">
  <span class="fas fa-calendar-check fa-5x"></span>
</aside>

<p>Apart from the annual Debian conference (DebConf), there
are several smaller meetings and get-togethers in different
countries every year. Taking part or helping to organize an <a
href="$(HOME)/events/">event</a> is a great opportunity to meet other
Debian users and developers.</p>

<ul>
  <li>Help out during the annual <a href="https://debconf.org/">Debian conference</a>, for example by recording <a href="https://video.debconf.org/">videos</a> of talks and presentations, greetings attendees and helping speakers, organizing special events during DebConf (like the cheese and wine party), helping with the setup and teardown, etc.</li>
  <li>Additionally, there are several <a href="https://wiki.debian.org/MiniDebConf">MiniDebConf</a> events, local meetings organized by Debian project members.</li>
  <li>You can also create or join a <a href="https://wiki.debian.org/LocalGroups">local Debian group</a> with regular meetings or other activities.</li>
  <li>Also, check out other events like <a href="https://wiki.debian.org/DebianDay">Debian Day parties</a>, <a href="https://wiki.debian.org/ReleaseParty">release parties</a>, <a href="https://wiki.debian.org/BSP">bug squashing parties</a>, <a href="https://wiki.debian.org/Sprints">development sprints</a>, or <a href="https://wiki.debian.org/DebianEvents">other events</a> around the world.</li>
</ul>

<h2><a id="donations">Donate Money, Hardware, or Bandwidth</a></h2>

<aside class="light">
  <span class="fas fa-gift fa-5x"></span>
</aside>

<p>All donations to the Debian project are managed by our Debian Project
Leader (DPL). With your support we can purchase hardware, domains,
cryptographic certificates, etc. We also use funds to sponsor DebConf
and MiniDebConf events, development sprints, presence at other events,
and other things.</p>

<ul>
  <li>You can <a href="$(HOME)/donations">donate</a> money, equipment and services to the Debian project.</li>
  <li>We're constantly looking for <a href="$(HOME)/mirror/">mirrors</a> worldwide.</li>
  <li>For our Debian ports, we rely on our <a href="$(HOME)/devel/buildd/">autobuilder network</a>.</li>
</ul>

<h2><a id="usedebian">Use Debian and talk about it</a></h2>

<aside class="light">
  <span class="fas fa-bullhorn fa-5x"></span>
</aside>

<p>Spread the word and tell others about Debian and the Debian
community. Recommend the operating system to other users, and show them
how to install it. Simply use it and enjoy -- that's probably the easiest
way to give back to the Debian project.</p>

<ul>
  <li>Help to promote Debian by giving a talk and demonstrating it to other users.</li>
  <li>Contribute to our <a href="https://www.debian.org/devel/website/">website</a> and help us to improve Debian's public face.</li>
  <li>Take <a href="https://wiki.debian.org/ScreenShots">screenshots</a> and <a href="https://screenshots.debian.net/upload">upload</a> them to <a href="https://screenshots.debian.net/">screenshots.debian.net</a> so that our users can see what software in Debian looks like before using it.</li>
  <li>You can enable <a href="https://packages.debian.org/popularity-contest">popularity-contest submissions</a> so we know which packages are popular and most useful to everyone.</li>
</ul>

<h2><a id="organizations">How your Organization can support Debian</a></h2>

<aside class="light">
  <span class="fa fa-check-circle fa-5x"></span>
</aside>

<p>
Whether you work at an educational, commercial, non-profit,
or government organization, there are plenty of ways to support
us with your resources. 
</p>

<ul>
  <li>For example, your organization could simply <a href="$(HOME)/donations">donate</a> money or hardware.</li>
  <li>Maybe you'd like to <a href="https://www.debconf.org/sponsors/">sponsor</a> our conferences.</li>
  <li>Your organization could provide <a href="https://wiki.debian.org/MemberBenefits">products or services to Debian contributors</a>.</li>
  <li>We're also looking for <a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">free hosting</a>. </li>
  <li>Of course, setting up mirrors for our <a href="https://www.debian.org/mirror/ftpmirror">software</a>, the <a
href="https://www.debian.org/CD/mirroring/">installation media</a>, or <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">conference videos</a> is also greatly appreciated.</li>
  <li>Maybe you'd also consider selling Debian <a href="https://www.debian.org/events/merchandise">merchandise</a>, <a href="https://www.debian.org/CD/vendors/">installation media</a>, or <a href="https://www.debian.org/distrib/pre-installed">pre-installed systems</a>.</li>
  <li>If your organization offers Debian <a href="https://www.debian.org/consultants/">consulting</a> or <a href="https://wiki.debian.org/DebianHosting">hosting</a>, please let us know.</li>
</ul>

<p>
We're also interested in forming <a href="https://www.debian.org/partners/">partnerships</a>. If you can
promote Debian by <a href="https://www.debian.org/users/">providing
a testimonial</a>, running it on your organization's servers
or desktops, or even encourage your staff to participate in our
project, that's fantastic. Maybe you'd also consider teaching about
the Debian operating system and community, directing your team to
contribute during working hours or sending them to one of our <a
href="$(HOME)/events/">events</a>.</p>

