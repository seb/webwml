<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in the Apache HTTP server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0217">CVE-2019-0217</a>

    <p>A race condition in mod_auth_digest when running in a threaded
    server could allow a user with valid credentials to authenticate
    using another username, bypassing configured access control
    restrictions. The issue was discovered by Simon Kappel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0220">CVE-2019-0220</a>

    <p>Bernhard Lorenz of Alpha Strike Labs GmbH reported that URL
    normalizations were inconsistently handled. When the path component
    of a request URL contains multiple consecutive slashes ('/'),
    directives such as LocationMatch and RewriteRule must account for
    duplicates in regular expressions while other aspects of the servers
    processing will implicitly collapse them.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.4.10-10+deb8u14.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1748.data"
# $Id: $
