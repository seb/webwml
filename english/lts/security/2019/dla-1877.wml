<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security issues have been fixed in otrs2, a well known trouble
ticket system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11563">CVE-2018-11563</a>

    <p>An attacker who is logged into OTRS as a customer can use the ticket
    overview screen to disclose internal article information of their
    customer tickets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12746">CVE-2019-12746</a>

    <p>A user logged into OTRS as an agent might unknowingly disclose their
    session ID by sharing the link of an embedded ticket article with
    third parties. This identifier can be then potentially abused in
    order to impersonate the agent user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13458">CVE-2019-13458</a>

    <p>An attacker who is logged into OTRS as an agent user with
    appropriate permissions can leverage OTRS tags in templates in order
    to disclose hashed user passwords.</p>


<p>Due to an incomplete fix for <a href="https://security-tracker.debian.org/tracker/CVE-2019-12248">CVE-2019-12248</a>, viewing email attachments
was no longer possible. This update correctly implements the new
Ticket::Fronted::BlockLoadingRemoteContent option.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.3.18-1+deb8u11.</p>

<p>We recommend that you upgrade your otrs2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1877.data"
# $Id: $
