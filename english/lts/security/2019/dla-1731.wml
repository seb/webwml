<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10741">CVE-2016-10741</a>

    <p>A race condition was discovered in XFS that would result in a
    crash (BUG). A local user permitted to write to an XFS volume
    could use this for denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5753">CVE-2017-5753</a>

    <p>Further instances of code that was vulnerable to Spectre variant 1
    (bounds-check bypass) have been mitigated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13305">CVE-2017-13305</a>

    <p>A memory over-read was discovered in the keys subsystem's
    encrypted key type. A local user could use this for denial of
    service or possibly to read sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">CVE-2018-3639</a> (SSB)

    <p>Multiple researchers have discovered that Speculative Store Bypass
    (SSB), a feature implemented in many processors, could be used to
    read sensitive information from another context. In particular,
    code in a software sandbox may be able to read sensitive
    information from outside the sandbox. This issue is also known as
    Spectre variant 4.</p>

    <p>This update fixes bugs in the mitigations for SSB for AMD
    processors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5848">CVE-2018-5848</a>

    <p>The wil6210 wifi driver did not properly validate lengths in scan
    and connection requests, leading to a possible buffer overflow.
    On systems using this driver, a local user with the CAP_NET_ADMIN
    capability could use this for denial of service (memory corruption
    or crash) or potentially for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5953">CVE-2018-5953</a>

    <p>The swiotlb subsystem printed kernel memory addresses to the
    system log, which could help a local attacker to exploit other
    vulnerabilities.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12896">CVE-2018-12896</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-13053">CVE-2018-13053</a>

    <p>Team OWL337 reported possible integer overflows in the POSIX
    timer implementation. These might have some security impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16862">CVE-2018-16862</a>

    <p>Vasily Averin and Pavel Tikhomirov from Virtuozzo Kernel Team
    discovered that the cleancache memory management feature did not
    invalidate cached data for deleted files. On Xen guests using the
    tmem driver, local users could potentially read data from other
    users' deleted files if they were able to create new files on the
    same volume.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16884">CVE-2018-16884</a>

    <p>A flaw was found in the NFS 4.1 client implementation. Mounting
    NFS shares in multiple network namespaces at the same time could
    lead to a user-after-free. Local users might be able to use this
    for denial of service (memory corruption or crash) or possibly
    for privilege escalation.</p>

    <p>This can be mitigated by disabling unprivileged users from
    creating user namespaces, which is the default in Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17972">CVE-2018-17972</a>

    <p>Jann Horn reported that the /proc/*/stack files in procfs leaked
    sensitive data from the kernel. These files are now only readable
    by users with the CAP_SYS_ADMIN capability (usually only root)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18281">CVE-2018-18281</a>

    <p>Jann Horn reported a race condition in the virtual memory manager
    that can result in a process briefly having access to memory after
    it is freed and reallocated. A local user permitted to create
    containers could possibly exploit this for denial of service
    (memory corruption) or for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18690">CVE-2018-18690</a>

    <p>Kanda Motohiro reported that XFS did not correctly handle some
    xattr (extended attribute) writes that require changing the disk
    format of the xattr. A user with access to an XFS volume could use
    this for denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18710">CVE-2018-18710</a>

    <p>It was discovered that the cdrom driver does not correctly
    validate the parameter to the CDROM_SELECT_DISC ioctl. A user with
    access to a cdrom device could use this to read sensitive
    information from the kernel or to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19824">CVE-2018-19824</a>

    <p>Hui Peng and Mathias Payer discovered a use-after-free bug in the
    USB audio driver. A physically present attacker able to attach a
    specially designed USB device could use this for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19985">CVE-2018-19985</a>

    <p>Hui Peng and Mathias Payer discovered a missing bounds check in the
    hso USB serial driver. A physically present user able to attach a
    specially designed USB device could use this to read sensitive
    information from the kernel or to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20169">CVE-2018-20169</a>

    <p>Hui Peng and Mathias Payer discovered missing bounds checks in the
    USB core. A physically present attacker able to attach a specially
    designed USB device could use this to cause a denial of service
    (crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20511">CVE-2018-20511</a>

    <p>InfoSect reported an information leak in the AppleTalk IP/DDP
    implemntation. A local user with CAP_NET_ADMIN capability could
    use this to read sensitive information from the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3701">CVE-2019-3701</a>

    <p>Muyu Yu and Marcus Meissner reported that the CAN gateway
    implementation allowed the frame length to be modified, typically
    resulting in out-of-bounds memory-mapped I/O writes.  On a system
    with CAN devices present, a local user with CAP_NET_ADMIN
    capability in the initial net namespace could use this to cause a
    crash (oops) or other hardware-dependent impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3819">CVE-2019-3819</a>

    <p>A potential infinite loop was discovered in the HID debugfs
    interface exposed under /sys/kernel/debug/hid. A user with access
    to these files could use this for denial of service.</p>

    <p>This interface is only accessible to root by default, which fully
    mitigates the issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6974">CVE-2019-6974</a>

    <p>Jann Horn reported a use-after-free bug in KVM. A local user
    with access to /dev/kvm could use this to cause a denial of
    service (memory corruption or crash) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7221">CVE-2019-7221</a>

    <p>Jim Mattson and Felix Wilhelm reported a user-after-free bug in
    KVM's nested VMX implementation. On systems with Intel CPUs, a
    local user with access to /dev/kvm could use this to cause a
    denial of service (memory corruption or crash) or possibly for
    privilege escalation.</p>

    <p>Nested VMX is disabled by default, which fully mitigates the
    issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7222">CVE-2019-7222</a>

    <p>Felix Wilhelm reported an information leak in KVM for x86.
    A local user with access to /dev/kvm could use this to read
    sensitive information from the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9213">CVE-2019-9213</a>

    <p>Jann Horn reported that privileged tasks could cause stack
    segments, including those in other processes, to grow downward to
    address 0. On systems lacking SMAP (x86) or PAN (ARM), this
    exacerbated other vulnerabilities: a null pointer dereference
    could be exploited for privilege escalation rather than only for
    denial of service.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.16.64-1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1731.data"
# $Id: $
