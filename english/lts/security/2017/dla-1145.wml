<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in zoneminder. This update
fixes only a serious file disclosure vulnerability (<a href="https://security-tracker.debian.org/tracker/CVE-2017-5595">CVE-2017-5595</a>).</p>

<p>The application has been found to suffer from many other problems
such as SQL injection vulnerabilities, cross-site scripting issues,
cross-site request forgery, session fixation vulnerability. Due to the
amount of issues and to the relative invasiveness of the relevant patches,
those issues will not be fixed in Wheezy. We thus advise you to restrict
access to zoneminder to trusted users only. If you want to review the
list of ignored issues, you can check the security tracker:
<a href="https://security-tracker.debian.org/tracker/source-package/zoneminder">https://security-tracker.debian.org/tracker/source-package/zoneminder</a></p>

<p>We recommend that you upgrade your zoneminder packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 7 <q>Wheezy</q>, these issues have been fixed in zoneminder version 1.25.0-4+deb7u2</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1145.data"
# $Id: $
