<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the bgpd daemon in the Quagga routing suite
does not properly calculate the length of multi-segment AS_PATH UPDATE
messages, causing bgpd to drop a session and potentially resulting in
loss of network connectivity.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
quagga_0.99.22.4-1+wheezy3+deb7u2.</p>

<p>We recommend that you upgrade your quagga packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1152.data"
# $Id: $
