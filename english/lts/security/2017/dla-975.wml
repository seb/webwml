<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in wordpress, a web blogging
tool. The Common Vulnerabilities and Exposures project identifies the
following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8295">CVE-2017-8295</a>

    <p>Potential unauthorized password reset vulnerability. More
    information are available at:</p>

    <p><url "https://exploitbox.io/vuln/WordPress-Exploit-4-7-Unauth-Password-Reset-0day-CVE-2017-8295.html"></p>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9061">CVE-2017-9061</a>

    <p>A cross-site scripting (XSS) vulnerability exists when someone
    attempts to upload very large files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9062">CVE-2017-9062</a>

    <p>Improper handling of post meta data values in the XML-RPC API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9063">CVE-2017-9063</a>

    <p>Cross-site scripting (XSS) vulnerability in the customizer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9064">CVE-2017-9064</a>

    <p>A Cross Site Request Forgery (CSRF) vulnerability exists in the
    filesystem credentials dialog.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9065">CVE-2017-9065</a>

    <p>Lack of capability checks for post meta data in the XML-RPC API.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.1+dfsg-1~deb7u15.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-975.data"
# $Id: $
