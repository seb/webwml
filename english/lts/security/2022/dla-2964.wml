<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata 2022a for the
Perl bindings. For the list of changes, see DLA-2963-1.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:2.09-1+2022a.</p>

<p>We recommend that you upgrade your libdatetime-timezone-perl packages.</p>

<p>For the detailed security status of libdatetime-timezone-perl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libdatetime-timezone-perl">https://security-tracker.debian.org/tracker/libdatetime-timezone-perl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2964.data"
# $Id: $
