<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One issue has been discovered in lighttpd: fast webserver with minimal memory
footprint.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19052">CVE-2018-19052</a>:

<p>an issue was discovered in mod_alias_physical_handler in
mod_alias.c in lighttpd before 1.4.50. There is potential ../ path traversal of
a single directory above an alias target, with a specific mod_alias
configuration where the matched alias lacks a trailing '/' character, but the
alias target filesystem path does have a trailing '/' character.</p></li>

</ul>

<p>For Debian 9 stretch, this problem has been fixed in version
1.4.45-1+deb9u1.</p>

<p>We recommend that you upgrade your lighttpd packages.</p>

<p>For the detailed security status of lighttpd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lighttpd">https://security-tracker.debian.org/tracker/lighttpd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2887.data"
# $Id: $
