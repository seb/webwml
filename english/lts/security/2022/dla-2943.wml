<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>ruby-sidekiq, a simple, efficient background processing for Ruby,
had a couple of vulnerabilities as follows:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30151">CVE-2021-30151</a>

    <p>Sidekiq allows XSS via the queue name of the live-poll feature
    when Internet Explorer is used.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23837">CVE-2022-23837</a>

    <p>In api.rb in Sidekiq, there is no limit on the number of days
    when requesting stats for the graph. This overloads the system,
    affecting the Web UI, and makes it unavailable to users.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.2.3+dfsg-1+deb9u1.</p>

<p>We recommend that you upgrade your ruby-sidekiq packages.</p>

<p>For the detailed security status of ruby-sidekiq please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-sidekiq">https://security-tracker.debian.org/tracker/ruby-sidekiq</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2943.data"
# $Id: $
