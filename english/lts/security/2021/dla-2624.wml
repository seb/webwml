<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Format string vulnerability in panoFileOutputNamesCreate() in
libpano13 2.9.20~rc2+dfsg-3 and earlier can lead to read and
write arbitrary memory values.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.9.19+dfsg-2+deb9u1.</p>

<p>We recommend that you upgrade your libpano13 packages.</p>

<p>For the detailed security status of libpano13 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libpano13">https://security-tracker.debian.org/tracker/libpano13</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2624.data"
# $Id: $
