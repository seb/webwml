<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in edk2, firmware for
virtual machines. Integer and stack overflows and uncontrolled resource
consumption may lead to a denial-of-service or in a worst case scenario,
allow an authenticated local user to potentially enable escalation of
privilege.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
0~20161202.7bbe0b3e-1+deb9u2.</p>

<p>We recommend that you upgrade your edk2 packages.</p>

<p>For the detailed security status of edk2 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/edk2">https://security-tracker.debian.org/tracker/edk2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2645.data"
# $Id: $
