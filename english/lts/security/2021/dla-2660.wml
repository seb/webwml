<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One security issue has been discovered in libgetdata</p>

    <p><a href="https://security-tracker.debian.org/tracker/CVE-2021-20204">CVE-2021-20204</a></p>

        <p>A heap memory corruption problem (use after free) can be triggered when processing
        maliciously crafted dirfile databases. This degrades the confidentiality,
        integrity and availability of third-party software that uses libgetdata as a library.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.9.4-1+deb9u1.</p>

<p>We recommend that you upgrade your libgetdata packages.</p>

<p>For the detailed security status of libgetdata please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libgetdata">https://security-tracker.debian.org/tracker/libgetdata</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2660.data"
# $Id: $
