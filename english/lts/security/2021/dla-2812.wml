<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One security issue has been discovered in botan1.10: a C++ cryptography
library.</p>


<p>An attacker of a local or a cross-VM may be able to recover bits of
secret exponents as used in RSA, DH, etc. with help of cache analysis.
<a href="https://www.usenix.org/conference/usenixsecurity17/technical-sessions/presentation/wang-shuai">https://www.usenix.org/conference/usenixsecurity17/technical-sessions/presentation/wang-shuai</a></p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.10.17-1+deb9u1.</p>

<p>We recommend that you upgrade your botan1.10 packages.</p>

<p>For the detailed security status of botan1.10 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/botan1.10">https://security-tracker.debian.org/tracker/botan1.10</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2812.data"
# $Id: $
