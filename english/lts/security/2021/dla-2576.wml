<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were a number of integer overflow issues in
Redis, a persistent "NoSQL"-style key-value database. It is currently believed
that the issues only affect 32-bit based systems.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21309">CVE-2021-21309</a></li>
</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
3:3.2.6-3+deb9u4.</p>

<p>We recommend that you upgrade your redis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2576.data"
# $Id: $
