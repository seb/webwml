<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two input sanitization failures have been found in the faxrunq and faxq
binaries in mgetty. An attacker could leverage them to insert commands
via shell metacharacters in jobs id and have them executed with the
privilege of the faxrunq/faxq user.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.1.36-2.1+deb8u1.</p>

<p>We recommend that you upgrade your mgetty packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1502.data"
# $Id: $
