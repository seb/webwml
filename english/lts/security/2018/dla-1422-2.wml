<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The previous update to linux failed to build for the armhf (ARM EABI
hard-float) architecture.  This update corrects that.  For all other
architectures, there is no need to upgrade or reboot again.  For
reference, the relevant part of the original advisory text follows.</p>

<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">CVE-2017-5715</a>

    <p>Multiple researchers have discovered a vulnerability in various
    processors supporting speculative execution, enabling an attacker
    controlling an unprivileged process to read memory from arbitrary
    addresses, including from the kernel and all other processes
    running on the system.</p>

    <p>This specific attack has been named Spectre variant 2 (branch
    target injection) and is mitigated for the x86 architecture (amd64
    and i386) by using new microcoded features.</p>

    <p>This mitigation requires an update to the processor's microcode,
    which is non-free.  For recent Intel processors, this is included
    in the intel-microcode package from version 3.20180425.1~deb8u1.
    For other processors, it may be included in an update to the
    system BIOS or UEFI firmware, or in a later update to the
    amd64-microcode package.</p>

    <p>This vulnerability was already mitigated for the x86 architecture
    by the <q>retpoline</q> feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5753">CVE-2017-5753</a>

    <p>Further instances of code that was vulnerable to Spectre variant 1
    (bounds-check bypass) have been mitigated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1066">CVE-2018-1066</a>

    <p>Dan Aloni reported to Red Hat that the CIFS client implementation
    would dereference a null pointer if the server sent an invalid
    response during NTLMSSP setup negotiation. This could be used by a
    malicious server for denial of service.</p>

    <p>The previously applied mitigation for this issue was not
    appropriate for Linux 3.16 and has been replaced by an alternate
    fix.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1093">CVE-2018-1093</a>

    <p>Wen Xu reported that a crafted ext4 filesystem image could trigger
    an out-of-bounds read in the ext4_valid_block_bitmap() function. A
    local user able to mount arbitrary filesystems could use this for
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1130">CVE-2018-1130</a>

    <p>The syzbot software found that the DCCP implementation of
    sendmsg() does not check the socket state, potentially leading
    to a null pointer dereference.  A local user could use this to
    cause a denial of service (crash).    </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3665">CVE-2018-3665</a>

    <p>Multiple researchers have discovered that some Intel x86
    processors can speculatively read floating-point and vector
    registers even when access to those registers is disabled.  The
    Linux kernel's <q>lazy FPU</q> feature relies on that access control to
    avoid saving and restoring those registers for tasks that do not
    use them, and was enabled by default on x86 processors that do
    not support the XSAVEOPT instruction.</p>

    <p>If <q>lazy FPU</q> is enabled on one of the affected processors, an
    attacker controlling an unprivileged process may be able to read
    sensitive information from other users' processes or the kernel.
    This specifically affects processors based on the <q>Nehalem</q> and
    <q>Westemere</q> core designs.
    
    This issue has been mitigated by disabling <q>lazy FPU</q> by default
    on all x86 processors that support the FXSAVE and FXRSTOR
    instructions, which includes all processors known to be affected
    and most processors that perform speculative execution.  It can
    also be mitigated by adding the kernel parameter: eagerfpu=on</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5814">CVE-2018-5814</a>

    <p>Jakub Jirasek reported race conditions in the USB/IP host driver.
    A malicious client could use this to cause a denial of service
    (crash or memory corruption), and possibly to execute code, on a
    USB/IP server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9422">CVE-2018-9422</a>

    <p>It was reported that the futex() system call could be used by an
    unprivileged user for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10853">CVE-2018-10853</a>

    <p>Andy Lutomirski and Mika Penttilä reported that KVM for x86
    processors did not perform a necessary privilege check when
    emulating certain instructions.  This could be used by an
    unprivileged user in a guest VM to escalate their privileges
    within the guest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10940">CVE-2018-10940</a>

    <p>Dan Carpenter reported that the optical disc driver (cdrom) does
    not correctly validate the parameter to the CDROM_MEDIA_CHANGED
    ioctl.  A user with access to a cdrom device could use this to
    cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11506">CVE-2018-11506</a>

    <p>Piotr Gabriel Kosinski and Daniel Shapira reported that the
    SCSI optical disc driver (sr) did not allocate a sufficiently
    large buffer for sense data.  A user with access to a SCSI
    optical disc device that can produce more than 64 bytes of
    sense data could use this to cause a denial of service (crash
    or memory corruption), and possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12233">CVE-2018-12233</a>

    <p>Shankara Pailoor reported that a crafted JFS filesystem image
    could trigger a denial of service (memory corruption).  This
    could possibly also be used for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000204">CVE-2018-1000204</a>

    <p>The syzbot software found that the SCSI generic driver (sg) would
    in some circumstances allow reading data from uninitialised
    buffers, which could include sensitive information from the kernel
    or other tasks.  However, only privileged users with the
    CAP_SYS_ADMIN or CAP_SYS_RAWIO capability were allowed to do this,
    so this has little or no security impact.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.16.57-1.  This update additionally fixes Debian bug #898165, and
includes many more bug fixes from stable update 3.16.57.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1422-2.data"
# $Id: $
