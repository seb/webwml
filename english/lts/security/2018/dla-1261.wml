<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities have been discovered in clamav, the ClamAV
AntiVirus toolkit for Unix. Effects range from denial of service to
potential arbitrary code execution. Additionally, this version fixes
a longstanding issue that has recently resurfaced whereby a malformed
virus signature database can cause an application crash and denial of
service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12374">CVE-2017-12374</a>

    <p>ClamAV has a use-after-free condition arising from a lack of input
    validation. A remote attacker could exploit this vulnerability with
    a crafted email message to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12375">CVE-2017-12375</a>

    <p>ClamAV has a buffer overflow vulnerability arising from a lack of
    input validation. An unauthenticated remote attacker could send a
    crafted email message to the affected device, triggering a buffer
    overflow and potentially a denial of service when the malicious
    message is scanned.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12376">CVE-2017-12376</a>

    <p>ClamAV has a buffer overflow vulnerability arising from improper
    input validation when handling Portable Document Format (PDF) files.
    An unauthenticated remote attacker could send a crafted PDF file to
    the affected device, triggering a buffer overflow and potentially a
    denial of service or arbitrary code execution when the malicious
    file is scanned.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12377">CVE-2017-12377</a>

    <p>ClamAV has a heap overflow vulnerability arising from improper input
    validation when handling mew packets. An attacker could exploit this
    by sending a crafted message to the affected device, triggering a
    denial of service or possible arbitrary code execution when the
    malicious file is scanned.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12378">CVE-2017-12378</a>

    <p>ClamAV has a buffer overread vulnerability arising from improper
    input validation when handling tape archive (TAR) files. An
    unauthenticated remote attacker could send a crafted TAR file to
    the affected device, triggering a buffer overread and potentially a
    denial of service when the malicious file is scanned.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12379">CVE-2017-12379</a>

    <p>ClamAV has a buffer overflow vulnerability arising from improper
    input validation in the message parsing function. An unauthenticated
    remote attacker could send a crafted email message to the affected
    device, triggering a buffer overflow and potentially a denial of
    service or arbitrary code execution when the malicious message is
    scanned.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12380">CVE-2017-12380</a>

    <p>ClamAV has a NULL dereference vulnerability arising from improper
    input validation in the message parsing function. An unauthenticated
    remote attacker could send a crafted email message to the affected
    device, triggering a NULL pointer dereference, which may result in a
    denial of service.</p>

<li>Debian Bug #824196

    <p>A malformed virus signature database could cause an application
    crash and denial of service.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.99.2+dfsg-0+deb7u4.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1261.data"
# $Id: $
