<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability in xml-security-c, a library for the XML Digital Security
specification, has been found. Different KeyInfo combinations, like
signatures without public key, result in incomplete DSA structures that
crash openssl during verification.</p>

<p>This vulnerability does not have a CVE identifier yet.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.7.2-3+deb8u2.</p>

<p>We recommend that you upgrade your xml-security-c packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1594.data"
# $Id: $
