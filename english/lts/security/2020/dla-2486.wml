<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jan-Niklas Sohn discovered that the XKB extension of the Xorg X server
performed incomplete input validation, which could result in privilege
escalation.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2:1.19.2-1+deb9u7.</p>

<p>We recommend that you upgrade your xorg-server packages.</p>

<p>For the detailed security status of xorg-server please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xorg-server">https://security-tracker.debian.org/tracker/xorg-server</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2486.data"
# $Id: $
