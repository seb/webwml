<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>libmtp is a library for communicating with MTP aware devices. The Media
Transfer Protocol (commonly referred to as MTP) is a devised set of custom
extensions to support the transfer of music files on USB digital audio players
and movie files on USB portable media players.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9831">CVE-2017-9831</a>

    <p>An integer overflow vulnerability in the ptp_unpack_EOS_CustomFuncEx
    function of the ptp-pack.c file allows attackers to cause a denial of
    service (out-of-bounds memory access) or maybe remote code execution by
    inserting a mobile device into a personal computer through a USB cable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9832">CVE-2017-9832</a>

    <p>An integer overflow vulnerability in ptp-pack.c (ptp_unpack_OPL function)
    allows attackers to cause a denial of service (out-of-bounds memory
    access) or maybe remote code execution by inserting a mobile device into
    a personal computer through a USB cable.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.1.8-1+deb8u1.</p>

<p>We recommend that you upgrade your libmtp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2169.data"
# $Id: $
