<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2020-14929">CVE-2020-14929</a></p>

  <p>Alpine before 2.23 silently proceeds to use an insecure connection
  after a /tls is sent in certain circumstances involving PREAUTH, which
  is a less secure behavior than the alternative of closing the connection
  and letting the user decide what they would like to do.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.11+dfsg1-3+deb8u1.</p>

<p>We recommend that you upgrade your alpine packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2254.data"
# $Id: $
