#use wml::debian::translation-check translation="ed1b2e9d8349f3e0333668208c8830371bdc453c" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.10</define-tag>
<define-tag release_date>2021-06-19</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la dixième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction apt "Changements de nom de suite accepté pour les dépôts par défaut (par exemple stable -&gt; oldstable)">
<correction awstats "Correction de problèmes d'accès à des fichiers distants [CVE-2020-29600 CVE-2020-35176]">
<correction base-files "Mise à jour de /etc/debian_version pour cette version">
<correction berusky2 "Correction d'erreur de segmentation au démarrage">
<correction clamav "Nouvelle version amont stable ; correction d'un problème de déni de sécurité [CVE-2021-1405]">
<correction clevis "Correction de la prise en charge des TPM qui ne prennent en charge que SHA256">
<correction connman "dnsproxy : vérification de la longueur des tampons avant memcpy [CVE-2021-33833]">
<correction crmsh "Correction d'un problème d'exécution de code [CVE-2020-35459]">
<correction debian-installer "Utilisation de l'ABI du noyau Linux 4.19.0-17">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction dnspython "XFR : pas de tentative de comparaison à une valeur <q>expiration</q> inexistante">
<correction dput-ng "Correction de plantage dans la méthode de téléversement sftp en cas d'un EACCES à partir du serveur ; mise à jour des noms de code ; <q>dcut dm</q> fonctionnel pour les DD non téléverseurs ; correction de TypeError dans la gestion d'exception de téléversement http  ; pas d'essai de construction de courriel d'envoi à partir du nom d'hôte du système dans les fichiers .dak-commands">
<correction eterm "Correction d'un problème d'exécution de code [CVE-2021-33477]">
<correction exactimage "Correction de construction avec C++11 et OpenEXR 2.5.x">
<correction fig2dev "Correction de dépassement de tampon [CVE-2021-3561] ; plusieurs corrections de sortie ; reconstruction de la suite de tests lors de la construction et dans autopkgtest">
<correction fluidsynth "Correction d'un problème d'utilisation de mémoire après libération [CVE-2021-28421]">
<correction freediameter "Correction d'un problème de déni de service [CVE-2020-6098]">
<correction fwupd "Correction de la génération de la chaîne SBAT du vendeur ; plus d'utilisation de dpkg-dev dans fwupd.preinst ; nouvelle version amont stable">
<correction fwupd-amd64-signed "Synchronisation avec fwupd">
<correction fwupd-arm64-signed "Synchronisation avec fwupd">
<correction fwupd-armhf-signed "Synchronisation avec fwupd">
<correction fwupd-i386-signed "Synchronisation avec fwupd">
<correction fwupdate "Amélioration de la prise en charge de SBAT">
<correction fwupdate-amd64-signed "Synchronisation avec fwupdate">
<correction fwupdate-arm64-signed "Synchronisation avec fwupdate">
<correction fwupdate-armhf-signed "Synchronisation avec fwupdate">
<correction fwupdate-i386-signed "Synchronisation avec fwupdate">
<correction glib2.0 "Correction de plusieurs problèmes de dépassement d'entier [CVE-2021-27218 CVE-2021-27219] ; correction d'une attaque par lien symbolique affectant file-roller [CVE-2021-28153]">
<correction gnutls28 "Correction d'un problème de déréférencement de pointeur NULL [CVE-2020-24659] ; ajout de plusieurs améliorations à la réallocation de mémoire">
<correction golang-github-docker-docker-credential-helpers "Correction d'un problème de double libération [CVE-2019-1020014]">
<correction htmldoc "Correction de problèmes de dépassement de tampon [CVE-2019-19630 CVE-2021-20308]">
<correction ipmitool "Correction de problèmes de dépassement de tampon [CVE-2020-5208]">
<correction ircii "Correction d'un problème de déni de service [CVE-2021-29376]">
<correction isc-dhcp "Correction d'un problème de dépassement de tampon [CVE-2021-25217]">
<correction isync "Rejet des noms de boîtes aux lettres <q>funny</q> provenant de IMAP LIST/LSUB [CVE-2021-20247] ; correction de la gestion du code de réponse inattendu d'APPENDUID [CVE-2021-3578]">
<correction jackson-databind "Correction d'un problème d'expansion d'entité externe [CVE-2020-25649] et plusieurs problèmes liés à la sérialisation [CVE-2020-24616 CVE-2020-24750 CVE-2020-35490 CVE-2020-35491 CVE-2020-35728 CVE-2020-36179 CVE-2020-36180 CVE-2020-36181 CVE-2020-36182 CVE-2020-36183 CVE-2020-36184 CVE-2020-36185 CVE-2020-36186 CVE-2020-36187 CVE-2020-36188 CVE-2020-36189 CVE-2021-20190]">
<correction klibc "malloc : réglage d'errno en cas d'échec ; correction de plusieurs problèmes de dépassement de tampon [CVE-2021-31873 CVE-2021-31870 CVE-2021-31872] ; cpio : correction d'un possible plantage sur les systèmes 64 bits [CVE-2021-31871] ; {set,long}jmp [s390x] : sauvegarde et restauration des registres de FPU corrects">
<correction libbusiness-us-usps-webtools-perl "Mise à jour vers la nouvelle API d'US-USPS">
<correction libgcrypt20 "Correction du chiffrement ElGamal faible avec des clés non générées par GnuPG/libgcrypt [CVE-2021-40528]">
<correction libgetdata "Correction d'un problème d'utilisation de mémoire après libération [CVE-2021-20204]">
<correction libmateweather "Adaptation pour renommer America/Godthab en America/Nuuk dans tzdata">
<correction libxml2 "Correction de lecture hors limites dans xmllint [CVE-2020-24977] ; correction de problème d'utilisation de mémoire après libération dans xmllint [CVE-2021-3516 CVE-2021-3518] ; validation d'UTF8 dans xmlEncodeEntities [CVE-2021-3517] ; propagation d'erreur dans xmlParseElementChildrenContentDeclPriv ; correction d'un problème d'attaque par expansion d'entité exponentielle [CVE-2021-3541]">
<correction liferea "Correction de compatibilité avec webkit2gtk &gt;= 2.32">
<correction linux "Nouvelle version amont stable ; passage de l'ABI à la version 17 ; [rt] mise à jour vers la version 4.19.193-rt81">
<correction linux-latest "Mise à jour pour l'ABI 4.19.0-17">
<correction linux-signed-amd64 "Nouvelle version amont stable ; passage de l'ABI à la version 17 ; [rt] mise à jour vers la version 4.19.193-rt81">
<correction linux-signed-arm64 "Nouvelle version amont stable ; passage de l'ABI à la version 17 ; [rt] mise à jour vers la version 4.19.193-rt81">
<correction linux-signed-i386 "Nouvelle version amont stable ; passage de l'ABI à la version 17 ; [rt] mise à jour vers la version 4.19.193-rt81">
<correction mariadb-10.3 "Nouvelle version amont ; corrections de sécurité [CVE-2021-2154 CVE-2021-2166 CVE-2021-27928] ; correction de la prise en charge d'Innotop ; fourniture de caching_sha2_password.so">
<correction mqtt-client "Correction d'un problème de déni de service [CVE-2019-0222]">
<correction mumble "Correction d'un problème d'exécution de code à distance [CVE-2021-27229]">
<correction mupdf "Correction d'un problème d'utilisation de mémoire après libération [CVE-2020-16600] et d'un problème de double libération de mémoire [CVE-2021-3407]">
<correction nmap "Mise à jour de la liste de préfixes MAC incluse">
<correction node-glob-parent "Correction d'un problème de déni de service relatif aux expressions rationnelles [CVE-2020-28469]">
<correction node-handlebars "Correction de problèmes d'exécution de code [CVE-2019-20920 CVE-2021-23369]">
<correction node-hosted-git-info "Correction d'un problème de déni de service relatif aux expressions rationnelles [CVE-2021-23362]">
<correction node-redis "Correction d'un problème de déni de service relatif aux expressions rationnelles [CVE-2021-29469]">
<correction node-ws "Correction d'un problème de déni de service relatif aux expressions rationnelles [CVE-2021-32640]">
<correction nvidia-graphics-drivers "Correction d'une vulnérabilité de contrôle d'accès incorrect [CVE-2021-1076]">
<correction nvidia-graphics-drivers-legacy-390xx "Correction d'une vulnérabilité de contrôle d'accès incorrect [CVE-2021-1076] ; correction d'échec d'installation sur les versions candidates de Linux 5.11">
<correction opendmarc "Correction d'un problème de dépassement de tas [CVE-2020-12460]">
<correction openvpn "Correction d'un problème de <q>illegal client float</q> [CVE-2020-11810] ; état de la clé comme authentifié assuré avant d'envoyer une réponse PUSH [CVE-2020-15078] ; augmentation de la taille de la file d'attente de listen() à 32">
<correction php-horde-text-filter "Correction d'un problème de script intersite [CVE-2021-26929]">
<correction plinth "Utilisation de la session pour vérifier l'étape d'accueil du premier démarrage">
<correction ruby-websocket-extensions "Correction d'un problème de déni de service [CVE-2020-7663]">
<correction rust-rustyline "Correction de la construction avec la nouvelle version de rustc">
<correction rxvt-unicode "Désactivation de la séquence d'échappement ESC G Q [CVE-2021-33477]">
<correction sabnzbdplus "Correction d'une vulnérabilité d'exécution de code [CVE-2020-13124]">
<correction scrollz "Correction d'un problème de déni de service [CVE-2021-29376]">
<correction shim "Nouvelle version amont ; ajout de la prise en charge de SBAT ; correction de positionnement des binaires i386 ; pas d'appel de QueryVariableInfo() sur les machines avec EFI 1.10 (par exemple les anciens Mac Intel) ; correction de la gestion d'ignore_db et d'user_insecure_mode ; ajout de scripts du responsable aux paquets de modèles de signatures pour gérer l'installation et la suppression de fbXXX.efi et de mmXXX.efi lors de l'installation ou la suppression des paquets shim-helpers-$arch-signed ; sortie propre lors d'une installation sur un système non EFI ; pas d'échec si les appels de debconf renvoient des erreurs">
<correction shim-helpers-amd64-signed "Synchronisation avec shim">
<correction shim-helpers-arm64-signed "Synchronisation avec shim">
<correction shim-helpers-i386-signed "Synchronisation avec shim">
<correction shim-signed "Mise à jour pour la nouvelle version de shim ; plusieurs corrections de bogues dans la gestion de postinst et postrm ; fourniture de binaires non signés pour arm64 (voir NEWS.Debian) ; sortie propre lors d'une installation sur un système non EFI ; pas d'échec si les appels de debconf renvoient des erreurs ; correction des liens de la documentation ; construction avec shim-unsigned 15.4-5~deb10u1 ; ajout d'une dépendance explicite de shim-signed à shim-signed-common">
<correction speedtest-cli "Gestion du cas où <q>ignoreids</q> est vide ou contient des identifiants vides">
<correction tnef "Correction d'un problème de lecture excessive de tampon [CVE-2019-18849]">
<correction uim "libuim-data : <q>Breaks</q> copié à partir de uim-data, corrigeant certains scénarios de mise à niveau">
<correction user-mode-linux "Reconstruction avec le noyau Linux 4.19.194-1">
<correction velocity "Correction d'un problème de possible exécution de code arbitraire [CVE-2020-13936]">
<correction wml "Correction de régression dans la gestion d'Unicode">
<correction xfce4-weather-plugin "Passage à la version 2.0 de l'API met.no">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2021 4848 golang-1.11>
<dsa 2021 4865 docker.io>
<dsa 2021 4873 squid>
<dsa 2021 4874 firefox-esr>
<dsa 2021 4875 openssl>
<dsa 2021 4877 webkit2gtk>
<dsa 2021 4878 pygments>
<dsa 2021 4879 spamassassin>
<dsa 2021 4880 lxml>
<dsa 2021 4881 curl>
<dsa 2021 4882 openjpeg2>
<dsa 2021 4883 underscore>
<dsa 2021 4884 ldb>
<dsa 2021 4885 netty>
<dsa 2021 4886 chromium>
<dsa 2021 4887 lib3mf>
<dsa 2021 4888 xen>
<dsa 2021 4889 mediawiki>
<dsa 2021 4890 ruby-kramdown>
<dsa 2021 4891 tomcat9>
<dsa 2021 4892 python-bleach>
<dsa 2021 4893 xorg-server>
<dsa 2021 4894 php-pear>
<dsa 2021 4895 firefox-esr>
<dsa 2021 4896 wordpress>
<dsa 2021 4898 wpa>
<dsa 2021 4899 openjdk-11-jre-dcevm>
<dsa 2021 4899 openjdk-11>
<dsa 2021 4900 gst-plugins-good1.0>
<dsa 2021 4901 gst-libav1.0>
<dsa 2021 4902 gst-plugins-bad1.0>
<dsa 2021 4903 gst-plugins-base1.0>
<dsa 2021 4904 gst-plugins-ugly1.0>
<dsa 2021 4905 shibboleth-sp>
<dsa 2021 4907 composer>
<dsa 2021 4908 libhibernate3-java>
<dsa 2021 4909 bind9>
<dsa 2021 4910 libimage-exiftool-perl>
<dsa 2021 4912 exim4>
<dsa 2021 4913 hivex>
<dsa 2021 4914 graphviz>
<dsa 2021 4915 postgresql-11>
<dsa 2021 4916 prosody>
<dsa 2021 4918 ruby-rack-cors>
<dsa 2021 4919 lz4>
<dsa 2021 4920 libx11>
<dsa 2021 4921 nginx>
<dsa 2021 4922 hyperkitty>
<dsa 2021 4923 webkit2gtk>
<dsa 2021 4924 squid>
<dsa 2021 4925 firefox-esr>
<dsa 2021 4926 lasso>
<dsa 2021 4928 htmldoc>
<dsa 2021 4929 rails>
<dsa 2021 4930 libwebp>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction compactheader "Incompatible avec les versions actuelles de Thunderbird">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contact Information</h2>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
