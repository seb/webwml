#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Icoutils est un ensemble de programmes ayant trait aux icônes et curseurs de
MS Windows. Les ressources telles que les icônes et les curseurs peuvent être
extraits de fichiers d’exécutables et de bibliothèques de MS Windows avec wrestool.</p>

<p>Trois vulnérabilités ont été découvertes dans ces outils.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6009">CVE-2017-6009</a>

<p>Un dépassement de tampon a été constaté dans wrestool.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6010">CVE-2017-6010</a>

<p>Un dépassement de tampon a été constaté dans la fonction extract_icons. Ce
problème peut être déclenché en traitant un fichier ico corrompu et aboutit
à un blocage d’icotool.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6011">CVE-2017-6011</a>

<p>Une lecture hors limites conduisant à un dépassement de tampon a été constaté
dans icotool.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.29.1-5deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets icoutils.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-854.data"
# $Id: $
