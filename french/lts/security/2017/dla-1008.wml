#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7375">CVE-2017-7375</a>

<p>Validation absente d’entités externes dans xmlParsePEReference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9047">CVE-2017-9047</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-9048">CVE-2017-9048</a>

<p>Un dépassement de tampon a été découvert dans libxml2
version 20904-GITv2.9.4-16-g0741801. La fonction xmlSnprintfElementContent dans
valid.c est supposée vider récursivement la définition du contenu de l’élément
dans un tampon char <q>buf</q> de taille <q>size</q>. La variable len est
assignée à strlen(buf). Si le content->type est XML_ELEMENT_CONTENT_ELEMENT,
alors (1) le content->prefix est joint à buf (s'il y a assez d'espace) à la
suite de quoi (2) content->name est écrit dans le tampon. Cependant, la
vérification pour savoir si le content->name est suffisamment grand utilise aussi
<q>len</q> plutôt que la taille du tampon mis à jour strlen(buf). Cela
permet d’écrire <q>size</q> octets supplémentaires au-delà de la mémoire
allouée. Cette vulnérabilité fait que les programmes utilisant libxml2, tels que
PHP, plantent.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9049">CVE-2017-9049</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-9050">CVE-2017-9050</a>

<p>libxml2 version 20904-GITv2.9.4-16-g0741801 est vulnérable à une lecture hors
limites de tampon basé sur le tas dans la fonction xmlDictComputeFastKey dans
dict.c. Cette vulnérabilité fait que les programmes utilisant libxml2, tels que
PHP, plantent. Cette vulnérabilité existe à cause d’un correctif incomplet pour
libxml2 (Bogue n° 759398).</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.8.0+dfsg1-7+wheezy8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxml2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1008.data"
# $Id: $
