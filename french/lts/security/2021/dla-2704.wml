#use wml::debian::translation-check translation="232aad840d691b8b21afa6f674eb5efc94f23ab7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité dans XStream, une bibliothèque Java pour sérialiser des
objets vers ou à partir du XML, peut permettre à un attaquant distant d’exécuter
des commandes sur l’hôte simplement en manipulant le flux d’entrées traité.
<p><b>Remarque :</b> le projet XStream recommande de configurer son cadriciel de
sécurité avec une liste blanche limitée au minimum des types nécessaires plutôt
que de s’appuyer sur la liste noire (mise à jour pour corriger cette
vulnérabilité). Le projet abandonne aussi progressivement l’entretien de la
liste noire, consultez
<a href="https://x-stream.github.io/security.html">https://x-stream.github.io/security.html</a> .</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.4.11.1-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxstream-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxstream-java, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxstream-java">\
https://security-tracker.debian.org/tracker/libxstream-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2704.data"
# $Id: $
