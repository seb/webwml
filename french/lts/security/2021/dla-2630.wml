#use wml::debian::translation-check translation="8f324fd303528ea188d808a270c54dcea73e2320" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29447">CVE-2021-29447</a>

<p>Wordpress est un système de gestion de contenu au code source ouvert. Un
utilisateur autorisé à télécharger des fichiers (tel un auteur) peut exploiter
un problème d’analyse XML dans la bibliothèque Media conduisant à une attaque
par entité externe (XXE). Cela nécessite que l’installation de WordPress
utilise PHP 8. L’accès à des fichiers internes est possible en cas d’attaque
XXE réussie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29450">CVE-2021-29450</a>

<p>Wordpress est un SGC au code source ouvert. Un des blocs dans l’éditeur de
WordPress peut être exploité d’une façon qui expose les publications et les
pages protégées par des mots de passe. Cela nécessite au moins des privilèges
de contributeur.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.7.20+dfsg-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wordpress, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wordpress">\
https://security-tracker.debian.org/tracker/wordpress</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2630.data"
# $Id: $
