#use wml::debian::translation-check translation="743abd2c13e9d73bee8c5f35f250ee625b5ace77" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été corrigé dans libsdl2, la version la plus récente
de la bibliothèque Simple DirectMedia Layer qui fournit un accès de bas
niveau à une sortie audio, au clavier, à la souris, à une manette de jeu et
à du matériel graphique.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2888">CVE-2017-2888</a>
<a href="https://security-tracker.debian.org/tracker/CVE-2019-7637">CVE-2019-7637</a>

<p>Un dépassement possible dans une allocation de surface a été corrigé.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.0.5+dfsg1-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libsdl2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libsdl2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libsdl2">\
https://security-tracker.debian.org/tracker/libsdl2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2803.data"
# $Id: $
