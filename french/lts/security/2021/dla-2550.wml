#use wml::debian::translation-check translation="b45e1d0724e3eec7454adc338d64c1c8b4fac228" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Diverses erreurs de dépassement ont été repérées et corrigées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27814">CVE-2020-27814</a>

<p>Un dépassement de tampon de tas a été découvert dans la façon dont openjpeg2
gérait certains fichiers au format PNG.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27823">CVE-2020-27823</a>

<p>Mauvais calcul de x1,y1 si l’option -d était utilisée, aboutissant à un
dépassement de tampon de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27824">CVE-2020-27824</a>

<p>Dépassement de tampon général lors d’une conversion irréversible quand trop
de niveaux de décomposition étaient indiqués.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27841">CVE-2020-27841</a>

<p>Une entrée contrefaite pour un traitement par l’encodeur openjpeg pourrait
provoquer une lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27844">CVE-2020-27844</a>

<p>Une entrée contrefaite pour un traitement par l’encodeur openjpeg pourrait
provoquer une écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27845">CVE-2020-27845</a>

<p>Une entrée contrefaite pourrait provoquer une écriture hors limites.</p></li>


</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.1.2-1.1+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openjpeg2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openjpeg2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openjpeg2">\
https://security-tracker.debian.org/tracker/openjpeg2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2550.data"
# $Id: $
