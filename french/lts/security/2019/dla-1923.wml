#use wml::debian::translation-check translation="79b6025f4c3513b54bc5154ee82f11872f18b4e4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans Ansible, un système de
gestion de configuration, de déploiement et d'exécution de tâches.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-3908">CVE-2015-3908</a>

<p>Attaque potentielle de type « homme du milieu » associée avec une
vérification insuffisante de certificat X.509. Ansible ne vérifiait pas que le
nom d’hôte du serveur ne correspond pas à un nom de domaine dans le Common Name
(CN) du sujet ou le champ subjectAltName du certificat X.509. Cela permet à des
attaquants de type « homme du milieu » d’usurper les serveurs SSL à l'aide d'un
certificat arbitraire valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6240">CVE-2015-6240</a>

<p>Attaque par lien symbolique permettant à des utilisateurs locaux de
s’évader d’un environnement restreint (chroot ou jail) à l'aide d'une attaque
par lien symbolique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10875">CVE-2018-10875</a>

<p>Correctif pour l’exécution potentielle de code arbitraire aboutissant
à lire le fichier ansible.cfg à partir d’un répertoire  courant de travail
modifiable par tout le monde. Cette condition fait que désormais ansible émet un
avertissement et ignore le fichier ansible.cfg dans le répertoire courant de
travail modifiable par tout le monde.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10156">CVE-2019-10156</a>

<p>Divulgation d’informations à l’aide d’une substitution inattendue de variable.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.7.2+dfsg-2+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets ansible.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1923.data"
# $Id: $
