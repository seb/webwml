#use wml::debian::translation-check translation="12697b1f6d4cfceff0ded55dee51ef3d9c27d34c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que freeimage, une bibliothèque graphique, était affectée
par les deux problèmes de sécurité suivants :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12211">CVE-2019-12211</a>

<p>Dépassement de tampon de tas causé par un memcpy non valable dans
PluginTIFF. Ce défaut pouvait être exploité par des attaquants distants pour
déclencher un déni de service ou tout autre impact non précisé à l’aide de
données TIFF contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12213">CVE-2019-12213</a>

<p>Épuisement de pile causé par une récursion non voulue dans PluginTIFF. Ce
défaut pouvait être exploité par des attaquants distants pour déclencher un déni
de service à l’aide de données TIFF contrefaites.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.15.4-4.2+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets freeimage.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2031.data"
# $Id: $
