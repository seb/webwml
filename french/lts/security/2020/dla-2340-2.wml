#use wml::debian::translation-check translation="ee6edfb766e5bfcab26e3845715d44db9dda994d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de sqlite3 publiée dans la DLA-2340-1 fournissait un correctif
incomplet pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2019-20218">CVE-2019-20218</a>.
Des paquets sqlite3 mis à jour sont maintenant disponibles pour résoudre ce
problème.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 3.16.2-5+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sqlite3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sqlite3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sqlite3">https://security-tracker.debian.org/tracker/sqlite3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2340-2.data"
# $Id: $
