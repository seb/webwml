#use wml::debian::translation-check translation="5a5bf32fd82b5f24b3287e66bd6b84deeaf3cd8a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème dans le système de calepin interactif
<a href="https://jupyter.org/">Jupyter</a> où un lien contrefait
pourrait rediriger le navigateur vers un site usurpé malveillant.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26215">CVE-2020-26215</a>

<p>Jupyter Notebook avant la version 6.1.5 possédait une vulnérabilité de
redirection ouverte. Un lien contrefait vers un serveur de calepin pourrait
rediriger le navigateur vers un site différent. Tous les serveurs de calepin
sont techniquement touchés, cependant, ces liens malveillants peuvent seulement
pointer en pratique vers des hôtes connus de serveur de calepin. Un lien vers un
serveur de calepin peut paraître sûr, mais rediriger finalement vers un serveur
usurpé de l’Internet public. Le problème est corrigé dans la version 6.1.5.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.2.3-4+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jupyter-notebook.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2477.data"
# $Id: $
