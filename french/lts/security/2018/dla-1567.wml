#use wml::debian::translation-check translation="e03db691eef0bf1e048da79524a1cbc851b57faf" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18718">CVE-2018-18718</a>
– CWE-415 : double libération
<p>Le produit appelle deux fois free() pour la même adresse de mémoire,
conduisant éventuellement à une modification d’emplacements de mémoire
inattendus.</p>

<p>Il existe un bogue de double libération suspecte de zone de mémoire avec
static void add_themes_from_dir() dlg-contact-sheet.c. Cette méthode implique
deux appels successifs à g_free(tampon) (ligne 354 et 373), et il est probable
que cela provoque une double libération de zone de mémoire du tampon. Un
correctif possible pourrait être d’assigner directement le tampon à NULL après
le premier appel à g_free(tampon). Merci à Tianjun Wu
<a href="https://gitlab.gnome.org/GNOME/gthumb/issues/18">https://gitlab.gnome.org/GNOME/gthumb/issues/18</a></p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 3:3.3.1-2.1+deb8u.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gthumb.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1567.data"
# $Id: $
