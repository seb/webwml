#use wml::debian::translation-check translation="070c807eaac7e2495316310253cd88461a3d71dc" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans openjpeg2, le codec 
JPEG 2000 libre.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27842">CVE-2020-27842</a>

<p>Déréférencement de pointeur NULL au moyen d'une entrée contrefaite pour
l'occasion. L'impact le plus élevé de ce défaut est la disponibilité de
l'application.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27843">CVE-2020-27843</a>

<p>Le défaut permet à un attaquant de fournir une entrée contrefaite pour
l'occasion à la fonctionnalité de conversion ou d'encodage, provoquant une
lecture hors limites. L'impact le plus élevé de cette vulnérabilité est la
disponibilité de l'application.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29338">CVE-2021-29338</a>

<p>Un dépassement d'entier permet à des attaquants distants de planter
l'application, provoquant un déni de service. Cela se produit quand
l'attaquant utilise l'option « -ImgDir » en ligne de commande sur un
répertoire contenant 1 048 576 fichiers.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1122">CVE-2022-1122</a>

<p>Un répertoire d'entrée avec un grand nombre de fichiers peut conduire à
une erreur de segmentation et à un déni de service dus à un appel de free()
sur un pointeur non initialisé.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.1.2-1.1+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openjpeg2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openjpeg2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openjpeg2">\
https://security-tracker.debian.org/tracker/openjpeg2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2975.data"
# $Id: $
