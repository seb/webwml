#use wml::debian::translation-check translation="cc48c50aea8222d5149ea79e65d65d9ec778896a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy a découvert que la fonction BN_mod_sqrt() d'OpenSSL
pouvait être piégée dans une boucle infinie. Cela pouvait avoir pour
conséquence un déni de service au moyen de certificats mal formés.</p>

<p>En complément, cette mise à jour corrige un bogue de débordement dans la
procédure x64_64 de la quadrature de Montgomery.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.1.0l-1~deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2952.data"
# $Id: $
