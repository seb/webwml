#use wml::debian::translation-check translation="1d19e4a312c029bd7140e49d3b9ba543127f44c2" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Bullseye Alpha 1</define-tag>
<define-tag release_date>2019-12-05</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la première version alpha pour Debian 11 <q>Bullseye</q>.
</p>

<p>
Il est grand temps que nous commencions à faire cela : beaucoup de
composants ont été mis à jour, remplaçant «CD » ou « CD-ROM » par
« supports d'installation ». Ces changements ne sont pas documentés
individuellement ci-dessous. Cela explique aussi pourquoi beaucoup de
langues ne sont pas complètement traduites dans cette version alpha.
</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>apt-setup :
    <ul>
      <li>mise à jour de la génération des lignes des sources d'apt pour la
        sécurité : renommées de <em>dist</em>/updates en <em>dist</em>-security à partir
        de Bullseye (<a href="https://bugs.debian.org/935540">nº 935540</a>,
        <a href="https://bugs.debian.org/942238">nº 942238</a>) ;</li>
      <li>correction de la préconfiguration pour les dépôts locaux en
        ajoutant la ou les clés dans le répertoire /etc/apt/trusted.gpg.d
         (<a href="https://bugs.debian.org/851774">nº 851774</a>, <a href="https://bugs.debian.org/928931">nº 928931</a>).</li>
    </ul>
  </li>
  <li>base-installer :
    <ul>
      <li>arrêt de l'installation du paquet de transition apt-transport-https.</li>
    </ul>
  </li>
  <li>brltty :
    <ul>
      <li>plus de création de my-at-spi-dbus-bus.desktop dans le profil
        utilisateur, maintenant at-spi2-core lance toujours le bus at-spi.</li>
    </ul>
  </li>
  <li>choose-mirror :
    <ul>
      <li>mise à jour de Mirrors.masterlist ;</li>
      <li>utilisation de « deb.debian.org » comme nom d'hôte par défaut
        pour un miroir HTTP personnalisé.</li>
    </ul>
  </li>
  <li>console-setup :
    <ul>
      <li>correction de problèmes d'internationalisation (<a href="https://bugs.debian.org/924657">nº 924657</a>).</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>cible Bullseye !</li>
      <li>plusieurs espaces permises autour des qualificatifs [] dans les
        entrées de sources.list ;</li>
      <li>rétablissement du raccourci « d » pour le thème dark. « c » ne
        fonctionne pas dans grub (<a href="https://bugs.debian.org/935545">nº 935545</a>) ;</li>
      <li>réglage gfxpayload=keep aussi dans les sous menus pour corriger
        les fontes illisibles dans les affichages HiDPI dans les images
        d'amorçage réseau lancées avec EFI (<a href="https://bugs.debian.org/935546">nº 935546</a>) ;</li>
      <li>conversion de certaines documentations au format DocBook XML 4.5 (<a href="https://bugs.debian.org/907970">nº 907970</a>).</li>
    </ul>
  </li>
  <li>finish-install :
    <ul>
      <li>plus de création de my-at-spi-dbus-bus.desktop dans le profil
        utilisateur, maintenant at-spi2-core lance toujours le bus at-spi.</li>
    </ul>
  </li>
  <li>fonts-sil-abyssinica :
    <ul>
      <li>pas d'élimination de la fonte des caractères Latin pour
        l'installateur.</li>
    </ul>
  </li>
  <li>grub2 :
    <ul>
      <li>ajout du module probe aux images UEFI signées (<a href="https://bugs.debian.org/936082">nº 936082</a>).</li>
    </ul>
  </li>
  <li>netcfg :
    <ul>
      <li>reformulation de template dans la mesure où certains clients DHCP
        ne sont plus disponibles.</li>
    </ul>
  </li>
  <li>parted :
    <ul>
      <li>sélection d'un correctif amont pour supprimer la sortie vers la
        sortie standard à partir d'affs qui perturbe l'installateur
        (<a href="https://bugs.debian.org/941777">nº 941777</a>).</li>
    </ul>
  </li>
  <li>partman-crypto :
    <ul>
      <li>installation de cryptsetup-initramfs à la place de cryptsetup
        (<a href="https://bugs.debian.org/930228">nº 930228</a>).</li>
    </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>debian-installer :
    <ul>
      <li>retrait des images pour QNAP TS-11x/TS-21x/HS-21x, QNAP
        TS-41x/TS-42x et HP Media Vault mv2120 pour des problèmes de taille
        avec le noyau Linux ;</li>
      <li>ARM : sunxi : ajout de la prise en charge d'Olimex
        A20-OLinuXino-Lime2-eMMC ;</li>
      <li>adaptation de la création de mini.iso sur arm pour que l'amorçage
        réseau EFI fonctionne.</li>
    </ul>
  </li>
  <li>hw-detect :
    <ul>
      <li>installation des paquets liés à la virtualisation quand la
        virtualisation est détectée (<a href="https://bugs.debian.org/782287">nº 782287</a>).</li>
    </ul>
  </li>
  <li>linux :
    <ul>
      <li>[arm64] udeb : ajout de i2c-rk3x à i2c-modules ;</li>
      <li>[arm64,armhf] udeb : ajout de rockchip-io-domain à
        kernel-image ;</li>
      <li>udeb : ajout d'atmel_mxt_ts à input-modules ;</li>
      <li>udeb : input-modules : ajout du clavier OLPC AP-SP ;</li>
      <li>[arm64] udeb : ajout de pl330 à kernel-image ;</li>
      <li>[armhf] udeb : retrait de davinci_cpdma de nic-modules ;</li>
      <li>udeb : inclusion de physmap à la place de physmap_of dans
        mtd-modules ;</li>
      <li>udeb : ajout de thermal_sys à kernel-image ;</li>
      <li>udeb : ajout de virtio-gpu pour obtenir une sortie graphique dans
        les instances de VM ;</li>
      <li>[x86] udeb : déplacement de rfkill dans le nouveau paquet
        rfkill-modules pour éviter la duplication ;</li>
      <li>[arm] rétroportage de la prise en charge de DTB pour le module
        Compute Module 3 de Rasperry Pi ;</li>
      <li>[arm64] rétroportage de la prise en charge de DTB pour le module
        Compute Module 3 de Rasperry Pi.</li>
    </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>76 langues sont prises en charge dans cette version.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
