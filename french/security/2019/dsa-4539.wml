#use wml::debian::translation-check translation="62a641e1944c721e80cb7778920f8b8acd1602ef" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Trois problèmes de sécurité ont été découverts dans OpenSSL : une
attaque temporelle à l'encontre d'ECDSA, une attaque d'oracle par
remplissage dans PKCS7_dataDecode() et CMS_decrypt_set1_pkey(),
et il a été découvert qu'une fonction du générateur de nombres aléatoires
(RNG), censée protéger contre le partage d'état du RNG entre les processus
parent et enfant dans l'éventualité d'un appel système fork(), n'était pas
utilisée par défaut.</p>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 1.1.0l-1~deb9u1.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1.1.1d-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4539.data"
# $Id: $
