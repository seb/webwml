#use wml::debian::translation-check translation="8a5eb3970afae92528c267b43ff2cff70683b130" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une augmentation de privilèges, à un déni de service
ou à des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3846">CVE-2019-3846</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-10126">CVE-2019-10126</a>

<p>huangwen a signalé plusieurs dépassements de tampon dans le pilote wifi
de Marvell (mwifiex) qui pourraient être utilisés par un utilisateur
pour provoquer un déni de service ou l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5489">CVE-2019-5489</a>

<p>Daniel Gruss, Erik Kraft, Trishita Tiwari, Michael Schwarz, Ari
Trachtenberg, Jason Hennessey, Alex Ionescu et Anders Fogh ont découvert
que des utilisateurs locaux pourraient utiliser l'appel système mincore()
pour obtenir des informations sensibles d'autres processus qui accèdent au
même fichier mappé en mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9500">CVE-2019-9500</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9503">CVE-2019-9503</a>

<p>Hugues Anguelkov a découvert un dépassement de tampon et un manque de
validation d'accès dans le pilote wifi FullMAC de Broadcom (brcmfmac) qui
pourraient être utilisés par un attaquant sur le même réseau wifi pour
provoquer un déni de service ou l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11477">CVE-2019-11477</a>

<p>Jonathan Looney a signalé qu'une séquence contrefaite pour l'occasion
d'accusés de réception sélectifs («selective acknowledgement » – SACK) TCP
permet un « kernel panic » déclenchable à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11478">CVE-2019-11478</a>

<p>Jonathan Looney a signalé qu'une séquence contrefaite pour l'occasion
d'accusés de réception sélectifs (« selective acknowledgement » – SACK) TCP
fragmente la file de retransmission TCP, permettant à un attaquant de
provoquer une utilisation excessive de ressources.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11479">CVE-2019-11479</a>

<p>Jonathan Looney a signalé qu'un attaquant pourrait forcer le noyau Linux
à segmenter ses réponses en de multiples segments TCP, dont chacun contient
seulement huit octets de données, augmentant de façon drastique la bande
passante nécessaire pour distribuer la même quantité de données.</p>

<p>Cette mise à jour introduit une nouvelle valeur de sysctl pour contrôler
la valeur minimale de la longueur maximum de segment (« Maximum Segment
Size » – MSS) (net.ipv4.tcp_min_snd_mss) qui utilise par défaut la valeur
de 48, auparavant codée en dur. Il est recommandé d'augmenter cette valeur
à 536 sauf si vous savez que votre réseau nécessite une valeur plus basse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11486">CVE-2019-11486</a>

<p>Jann Horn de Google a signalé de nombreuses situations de compétition
dans la discipline de ligne de Siemens R3964. Un utilisateur local pourrait
les utiliser pour provoquer un impact de sécurité non indiqué. Par conséquent,
ce module a été  désactivé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11599">CVE-2019-11599</a>

<p>Jann Horn de Google a signalé une situation de compétition dans
l'implémentation de vidage d'image mémoire (« core dump ») qui pourrait
conduire à une utilisation de mémoire après libération. Un utilisateur
local pourrait utiliser cela pour lire des informations sensibles,
provoquer un déni de service (corruption de mémoire), ou pour une
augmentation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11815">CVE-2019-11815</a>

<p>Une utilisation de mémoire après libération dans le protocole Reliable
Datagram Sockets pourrait avoir pour conséquence un déni de service et
éventuellement une augmentation de privilèges. Le module de ce protocole
(rds) n'est pas chargé automatiquement sur les systèmes Debian, donc ce
problème n'affecte que les systèmes où il a été chargé de façon explicite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11833">CVE-2019-11833</a>

<p>L'implémentation du système de fichiers ext4 écrit des données non
initialisées de la mémoire du noyau dans de nouveaux blocs d'extension. Un
utilisateur local capable d'écrire sur un système de fichiers ext4 et puis
de lire l'image du système de fichiers, par exemple en utilisant un disque
amovible, pourrait être capable d'utiliser cela pour obtenir des
informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11884">CVE-2019-11884</a>

<p>L'implémentation de Bluetooth HIDP ne s'assurait pas que les nouveaux
noms de connexion étaient terminés par un caractère null. Un utilisateur
local doté de la capacité CAP_NET_ADMIN pourrait être capable d'utiliser
cela pour obtenir des informations sensibles de la pile du noyau.</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 4.9.168-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4465.data"
# $Id: $
