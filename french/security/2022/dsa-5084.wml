#use wml::debian::translation-check translation="1d9bcf6bed3d3d7c0303a9d113875c0eb8da2364" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web WPE
WebKit :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22589">CVE-2022-22589</a>

<p>Heige et Bo Qu ont découvert que le traitement d'un message de courriel
contrefait peut conduire à l'exécution de code javascript arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22590">CVE-2022-22590</a>

<p>Toan Pham a découvert que le traitement d'un contenu web contrefait peut
conduire à l'exécution de code arbitraire.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22592">CVE-2022-22592</a>

<p>Prakash a découvert que le traitement d'un contenu web contrefait
peut empêcher l'application du « Content Security Policy ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22620">CVE-2022-22620</a>

<p>Un chercheur anonyme a découvert que le traitement d'un contenu web
contrefait peut conduire à l'exécution de code arbitraire. Apple a été
informé d'un rapport indiquant que ce problème a été activement exploité.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.34.6-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wpewebkit.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wpewebkit, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wpewebkit">\
https://security-tracker.debian.org/tracker/wpewebkit</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5084.data"
# $Id: $
