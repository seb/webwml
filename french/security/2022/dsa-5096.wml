#use wml::debian::translation-check translation="55f28f8ffad4f01a052c4b81349871fabf4f6e93" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou des
fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29374">CVE-2020-29374</a>

<p>Jann Horn de Google a signalé un défaut dans la gestion de la mémoire
virtuelle de Linux. Des processus parent ou enfant partagent initialement
leur mémoire, mais lorsqu'ils écrivent sur une page partagée, la page est
dupliquée et non partagée (copie sur écriture). Cependant, dans le cas où
une opération telle que vmsplice() nécessite que le noyau ajoute une
référence à une page partagée, et qu'une opération copie sur écriture se
produit pendant cette opération, le noyau pourrait avoir accès à la
mauvaise mémoire du processus. Pour quelques programmes, cela pourrait
conduire à une fuite d'informations ou à une corruption de données.</p>

<p>Ce problème a déjà été corrigé pour la plupart des architectures, mais
pas sur MIPS et System z. Cette mise à jour corrige cela.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36322">CVE-2020-36322</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-28950">CVE-2021-28950</a>

<p>L’outil syzbot a trouvé que l'implémentation de FUSE (système de
fichiers en espace utilisateur) ne gérait pas a correctement un serveur
FUSE renvoyant des attributs non valables pour un fichier. Un utilisateur
local autorisé à exécuter un serveur FUSE pouvait utiliser cela pour
provoquer un déni de service (plantage).</p>

<p>Le correctif original pour cela introduisait un autre déni de service
potentiel (boucle infinie dans l’espace noyau) qui a été aussi corrigé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3640">CVE-2021-3640</a>

<p>Lin Ma a découvert une situation de compétition dans l'implémentation du
protocole Bluetooth qui peut conduire à une utilisation de mémoire après
libération. Un utilisateur local pouvait exploiter cela pour provoquer un
déni de service (corruption de mémoire ou plantage) ou éventuellement une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3744">CVE-2021-3744</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-3764">CVE-2021-3764</a>

<p>minihanshen a signalé des bogues dans le pilote ccp pour les
coprocesseurs cryptographiques d'AMD qui pouvaient conduire une à fuite de
ressource. Sur les systèmes utilisant ce pilote, un utilisateur local
pouvait exploiter cela pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3752">CVE-2021-3752</a>

<p>Likang Luo de NSFOCUS Security Team a découvert un défaut dans
l'implémentation de Bluetooth L2CAP qui peut conduire à une utilisation de
mémoire après libération. Un utilisateur local pouvait exploiter cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3760">CVE-2021-3760</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-4202">CVE-2021-4202</a>

<p>Lin Ma a découvert des situations de compétition dans le pilote NCI
(NFC Controller Interface), qui pouvaient conduire à une utilisation de
mémoire après libération. Un utilisateur local pouvait exploiter cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement une élévation de privilèges.</p>

<p>Ce pilote n'est pas activé dans les configurations du noyau officiel de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3772">CVE-2021-3772</a>

<p>Un défaut a été découvert dans l'implémentation du protocole SCTP, qui
pouvait permettre à un attaquant réseau de rompre une association SCTP.
L'attaquant avait seulement besoin de connaître ou de deviner les adresses
IP et les ports de l'association.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4002">CVE-2021-4002</a>

<p>hugetlbfs, le système de fichiers virtuel utilisé par les applications
pour allouer de très grandes pages dans la RAM, ne vidait pas le TLB du
processus dans un cas où cela était nécessaire. Dans certaines
circonstances, un utilisateur local pouvait être capable de lire et
d'écrire ces très grandes pages après qu'elles ont été libérées et
réallouées à un processus différent. Cela pouvait conduire à une élévation
de privilèges, un déni de service ou des fuites d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4083">CVE-2021-4083</a>

<p>Jann Horn a signalé une situation de compétition dans le ramasse-miettes
des sockets locaux (Unix), qui peut conduire à une utilisation de mémoire
après libération. Un utilisateur local pouvait exploiter cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4135">CVE-2021-4135</a>

<p>Un défaut a été découvert dans le pilote netdevsim qui pouvait mener à
une fuite d'informations.</p>

<p>Ce pilote n'est pas activé dans les configurations du noyau officiel de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4155">CVE-2021-4155</a>

<p>Kirill Tkhai a découvert une fuite de données dans la manière dont
l'IOCTL XFS_IOC_ALLOCSP dans le système de fichiers XFS permettait une
augmentation de taille de fichiers ayant une taille non alignée. Un
attaquant pouvait tirer avantage de ce défaut pour une fuite de données sur
le système de fichiers XFS</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4203">CVE-2021-4203</a>

<p>Jann Horn a signalé une situation de compétition dans l'implémentation
des sockets locaux (Unix) qui peut conduire à une utilisation de mémoire
après libération. Un utilisateur local pouvait exploiter cela pour
divulguer des informations sensibles issues du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20317">CVE-2021-20317</a>

<p>La structure de la file d'attente du minuteur pouvait être corrompue
menant les tâches en attente à n'être jamais réveillées. Un utilisateur
local avec certains privilèges pouvait exploiter cela pour provoquer un
déni de service (blocage système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20321">CVE-2021-20321</a>

<p>Une situation de compétition a été découverte dans le pilote du système
de fichiers overlayfs. Un utilisateur local avec l'accès à un montage
overlayfs et à son répertoire racine sous-jacent pouvait exploiter cela
pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20322">CVE-2021-20322</a>

<p>Une fuite d'informations a été découverte dans l'implémentation d'IPv4.
Un attaquant distant pouvait exploiter cela pour découvrir rapidement quels
ports UDP sont utilisés par un système, facilitant son attaque par
empoisonnement de DNS à l'encontre de ce système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22600">CVE-2021-22600</a>

<p>L'outil syzbot a découvert un défaut dans l'implémentation du socket de
paquets (AF_PACKET) qui pouvait conduire à une libération de mémoire
incorrecte. Un utilisateur local doté de la capacité CAP_NET_RAW (dans
n'importe quel espace de noms utilisateur) pouvait exploiter cela pour un
déni de service (corruption de mémoire ou plantage) ou éventuellement une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28711">CVE-2021-28711</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-28712">CVE-2021-28712</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-28713">CVE-2021-28713</a> (XSA-391)

<p>Juergen Gross a signalé que des dorsaux de PV malveillants peuvent
provoquer un déni de service dans les clients servis par ces dorsaux au
moyen d'événements à haute fréquence, même si ces dorsaux sont exécutés
dans un environnement moins privilégié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28714">CVE-2021-28714</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-28715">CVE-2021-28715</a> (XSA-392)

<p>Juergen Gross a découvert que les clients Xen peuvent contraindre le
pilote netback de Linux à accaparer une grande quantité de mémoire du
noyau, avec pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38300">CVE-2021-38300</a>

<p>Piotr Krysiuk a découvert un défaut dans le compilateur JIT classic BPF
(cBPF) pour les architectures MIPS. Un utilisateur local pouvait exploiter
cela pour exécuter du code arbitraire dans le noyau.</p>

<p>Ce problème est pallié en configurant sysctl
net.core.bpf_jit_enable=0, ce qui est le réglage par défaut. Ce *n'est pas*
pallié par la désactivation de l'utilisation non privilégiée d'eBPF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39685">CVE-2021-39685</a>

<p>Szymon Heidrich a découvert une vulnérabilité de dépassement de tampon
dans le sous-système gadget USB, avec pour conséquences la divulgation
d'informations, un déni de service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39686">CVE-2021-39686</a>

<p>Une situation de compétition a été découverte dans le pilote de création
de lien d'Android, qui pouvait conduire à des vérifications de sécurité
incorrectes. Sur les systèmes où le pilote de création de lien est chargé,
un utilisateur local pouvait exploiter cela pour une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39698">CVE-2021-39698</a>

<p>Linus Torvalds a signalé un défaut dans l'implémentation de la
scrutation (« polling ») de fichiers, qui pouvait conduire à une
utilisation de mémoire après libération. Un utilisateur local pouvait
exploiter cela pour un déni de service (corruption de mémoire ou plantage)
ou éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39713">CVE-2021-39713</a>

<p>L'outil syzbot a découvert une situation de compétition dans le
sous-système d'ordonnancement du réseau qui pouvait conduire à une
utilisation de mémoire après libération. Un utilisateur local
pouvait exploiter cela pour un déni de service (corruption de mémoire ou
plantage) ou éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41864">CVE-2021-41864</a>

<p>Un dépassement d'entier a été découvert dans sous-système Extended BPF
(eBPF). Un utilisateur local pouvait exploiter cela pour un déni de service
(corruption de mémoire ou plantage), ou éventuellement une élévation de
privilèges.</p>

<p>Cela peut être pallié en configurant sysctl
kernel.unprivileged_bpf_disabled=1, qui désactive l'utilisation d'eBPF par
les utilisateurs non privilégiés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42739">CVE-2021-42739</a>

<p>Un dépassement de tampon de tas a été découvert dans le pilote firedtv
pour les récepteurs DVB connectés par FireWire. Un utilisateur local avec
l'accès à un périphérique firedtv pouvait exploiter cela pour un déni de
service (corruption de mémoire ou plantage), ou éventuellement une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43389">CVE-2021-43389</a>

<p>L'Active Defense Lab de Venustech a découvert un défaut dans le
sous-système CMTP tel qu'utilisé par Bluetooth, qui pouvait conduire à une
lecture hors limites et une confusion de type d'objet. Un utilisateur local
doté de la capacité CAP_NET_ADMIN dans l'espace de noms utilisateur initial
pouvait exploiter cela pour une déni de service (corruption de mémoire ou
plantage), ou éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43975">CVE-2021-43975</a>

<p>Brendan Dolan-Gavitt a signalé un défaut dans la fonction
hw_atl_utils_fw_rpc_wait() dans le pilote de périphérique Ethernet aQuantia
AQtion qui peut avoir pour conséquence un déni de service ou l'exécution de
code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43976">CVE-2021-43976</a>

<p>Zekun Shen et Brendan Dolan-Gavitt ont découvert un défaut dans la
fonction mwifiex_usb_recv() du pilote WiFi-Ex USB de Marvell. Un
attaquant capable de se connecter à un périphérique USB contrefait peut
tirer avantage de ce défaut pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44733">CVE-2021-44733</a>

<p>Une situation de compétition a été découverte dans le sous-système
Trusted Execution Environment (TEE) pour les processeurs Arm, qui pouvait
conduire à une utilisation de mémoire après libération. Un utilisateur
local autorisé à un périphérique TEE pouvait exploiter cela pour un déni de
service (corruption de mémoire ou plantage) ou éventuellement une élévation
de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45095">CVE-2021-45095</a>

<p>Le pilote du protocole Phone Network (PhoNet) présente une fuite de
nombre de références dans la fonction pep_sock_accept().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45469">CVE-2021-45469</a>

<p>Wenqing Liu a signalé un accès mémoire hors limites dans
l'implémentation de f2fs si un inœud a une dernière entrée xattr non
valable. Un attaquant pouvant monter une image contrefaite pour
l'occasion peut tirer avantage de ce défaut pour un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45480">CVE-2021-45480</a>

<p>Un défaut de fuite de mémoire a été découvert dans la fonction
__rds_conn_create() dans le sous-système du protocole RDS
(Reliable Datagram Sockets).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a> (INTEL-SA-00598)

<p>Des chercheurs de VUSec ont découvert que le tampon « Branch History »
des processeurs Intel peut être exploité pour créer des attaques par canal
auxiliaire d'information avec une exécution spéculative. Ce problème est
semblable à Spectre variante 2, mais demande des palliatifs supplémentaires
sur certains processeurs.</p>

<p>Cela peut être exploité pour obtenir des informations sensibles à
partir d'un contexte de sécurité différent, comme à partir de l'espace
utilisateur vers le noyau ou à partir d'un client KVM vers le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0002">CVE-2022-0002</a> (INTEL-SA-00598)

<p>Il s'agit d'un problème similaire à <a
href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a>,
mais recouvre une exploitation à l'intérieur d'un contexte de sécurité,
comme à partir de code compilé avec JIT dans un bac à sable vers du code de
l'hôte dans le même processus.</p>

<p>Ce problème est partiellement pallié en désactivant eBPF pour les
utilisateurs non privilégiés avec l'option sysctl :
kernel.unprivileged_bpf_disabled=2. Cette mise à jour fait cela par défaut.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0322">CVE-2022-0322</a>

<p>Eiichi Tsukata a découvert un défaut dans la fonction
sctp_make_strreset_req() dans l'implémentation du protocole réseau SCTP qui
peut avoir pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0330">CVE-2022-0330</a>

<p>Sushma Venkatesh Reddy a découvert une absence de vidage de TLB du GPU
dans le pilote i915, ayant pour conséquences un déni de service ou une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0435">CVE-2022-0435</a>

<p>Samuel Page et Eric Dumazet ont signalé un dépassement de pile dans le
module réseau pour le protocole TIPC (Transparent Inter-Process
Communication), ayant pour conséquences un déni de service ou
éventuellement l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0487">CVE-2022-0487</a>

<p>Une utilisation de mémoire après libération a été découverte dans le
pilote de prise en charge du contrôleur hôte MOXART SD/MMC. Ce défaut
n'impacte pas les paquets binaires de Debian dans la mesure où
CONFIG_MMC_MOXART n'est pas configuré.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0492">CVE-2022-0492</a>

<p>Yiqi Sun et Kevin Wang ont signalé que le sous-système cgroup-v1 ne
restreint pas correctement l'accès à la fonction « release-agent ». Un
utilisateur local peut tirer avantage de ce défaut pour une élévation de
privilèges et le contournement de l'isolation d'espace de noms.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0617">CVE-2022-0617</a>

<p>butt3rflyh4ck a découvert un déréférencement de pointeur NULL dans le
système de fichiers UDF. Un utilisateur local qui peut monter une image UDF
contrefaite pour l'occasion peut utiliser ce défaut pour planter le
système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0644">CVE-2022-0644</a>

<p>Hao Sun a signalé une absence de vérification de droits de lecture dans
les appels système finit_module() et kexec_file_load(). L'impact de
sécurité de ce problème n'est pas clair, dans la mesure où ces appels
système sont habituellement seulement disponibles pour le superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22942">CVE-2022-22942</a>

<p>Le mauvais traitement du descripteur de fichier dans le pilote de GPU
virtuel de VMware (vmwgfx) pouvait avoir pour conséquences une fuite
d'informations ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24448">CVE-2022-24448</a>

<p>Lyu Tao a signalé un défaut dans l'implémentation de NFS dans le noyau
Linux lors du traitement de requêtes pour ouvrir un répertoire sur un
fichier ordinaire, qui pouvait avoir pour conséquence une fuite
d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24959">CVE-2022-24959</a>

<p>Une fuite de mémoire a été découverte dans la fonction
yam_siocdevprivate() du pilote YAM pour AX.25, qui pouvait avoir pour
conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25258">CVE-2022-25258</a>

<p>Szymon Heidrich a signalé que le sous-système USB Gadget manque de
certaines validations des requêtes du descripteur d'OS de l'interface,
avec pour conséquence une corruption de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25375">CVE-2022-25375</a>

<p>Szymon Heidrich a signalé que le gadget USB RNDIS manque de validation
de la taille de la commande RNDIS_MSG_SET, avec pour conséquence une fuite
d'informations à partir de la mémoire du noyau.</p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 4.19.232-1. Cette mise à jour comprend en plus beaucoup
plus de corrections de bogue issues des mises à jour de stable, de la
version 4.19.209 à la version 4.19.232 incluse.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5096.data"
# $Id: $
