#use wml::debian::template title="Informations sur la version «&nbsp;Buster&nbsp;» de Debian"
#use wml::debian::translation-check translation="2b2b2d98876137a0efdabdfc2abad6088d4c511f" maintainer="Jean-Pierre Giraud"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/buster/release.data"

# Translators:
# cf. ../<other_release>/index.html

<p>
La version&nbsp;<current_release_buster> de Debian (connue sous le nom
de <em>Buster</em>) a été publiée le <a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "La version 10.0 a été initialement publiée le <:=spokendate('2019-07-06'):>."
/>
Cette version
comprend de nombreuses modifications décrites dans notre <a
href="$(HOME)/News/2019/20190706">communiqué de presse</a> et les <a
href="releasenotes">notes de publication</a>.
</p>

<p><strong>Debian 10 a été remplacée par
<a href="../bullseye/">Debian 11 (<q>Bullseye</q>)</a>.
#Les mises à jour de sécurité sont arrêtées depuis le <:=spokendate('xxxx-xx-xx'):>.
</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>Néanmoins, Buster bénéficie de la prise en charge à long terme
#(<q>Long Term Support</q> – LTS) jusqu'à la fin du mois de xxxxx 20xx.
#Cette prise en charge est limitée aux architectures i386, amd64, armel,
#armhf et arm64. Toutes les autres architectures ne sont plus prises en
#charge dans Buster.
#Pour de plus amples informations, veuillez consulter la
#<a href="https://wiki.debian.org/fr/LTS">section dédiée à LTS du wiki Debian</a>.
#</strong></p>

<p>
Pour obtenir et installer Debian, veuillez vous reporter à la page
des <a href="debian-installer/">informations d'installation</a> et au <a
href="installmanual">guide d'installation</a>. Pour mettre à niveau à partir d'une ancienne
version de Debian, veuillez vous reporter aux instructions des <a
href="releasenotes">notes de publication</a>.
</p>

### Activate the following when LTS period starts.
#<p>Architectures prises en charge durant la prise en charge à long terme :</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>
Les architectures suivantes sont gérées par cette version&nbsp;:
</p>
# <p>Architectures matérielles prises en charge lors de la version initiale de Buster :</p> ### Use this line when LTS starts, instead of the one above.

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous <a href="reportingbugs">signaler d'autres problèmes</a>.
</p>

<p>
Enfin, nous avons une liste de <a href="credits">personnes à remercier</a> pour
leur participation à cette publication.
</p>
