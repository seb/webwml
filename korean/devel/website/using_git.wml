#use wml::debian::template title="Git을 써서 데비안 웹사이트 작업" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="7d4921b13ecaf05ffd68527f4276d1f53cad5e16" maintainer="Sebul"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#work-on-repository">Git 저장소에서 작업</a></li>
<li><a href="#write-access">Git 저장소 쓰기 권한</a></li>
<li><a href="#notifications">알림 받기</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> <a href="https://git-scm.com/">Git</a>은 
<a href="https://en.wikipedia.org/wiki/Version_control">version control system</a>이며 
여러 개발자 간의 작업을 조정하는데 도움이 됩니다. 
모든 사용자는 기본 저장소의 로컬 복사본을 보유할 수 있습니다. 
로컬 복사본은 같은 시스템에 있거나 전 세계에 있을 수 있습니다. 
그 다음, 개발자는 로컬 복사본을 수정하고 준비가 되었을 때 변경 사항을 주 저장소에 다시 커밋할 수 있습니다.</p>
</aside>

<h2><a id="work-on-repository">Git 저장소에서 작업</a></h2>

<p>
바로 시작합니다. 
이 절에서는 기본 저장소의 로컬 복사본을 만드는 방법, 해당 저장소를 최신 상태로 유지하는 방법 및 작업을 제출하는 방법을 배웁니다. 
번역 작업 방법도 설명합니다.
</p>

<h3><a name="get-local-repo-copy">로컬 복사본 받기</a></h3>

<p>
Git을 설치하세요. 다음, 
Git을 구성하고 이름과 이메일을 넣으세요. 
Git을 처음 쓴다면, 일반 git 문서를 먼저 읽는 게 좋습니다. 
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> 
<a href="https://git-scm.com/doc">Git 문서</a></button></p>

<p>
다음 단계는 저장소를 복제(로컬 사본 만들기)하는 겁니다.
두 가지 방법:
</p>

<ul>
  <li><url https://salsa.debian.org/>에 계정을 등록하고 
공개 SSH 키를 Salsa 계정에 올려서 SSH 권한을 활성화 하세요. 
자세한 것은 <a href="https://salsa.debian.org/help/ssh/README.md">Salsa 도움 
페이지</a> 보세요. 그 다음 
  <code>webwml</code> 저장소를 복제하려면 다음 명령을 쓰세요:
<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>
  </li>
  <li>
또는 HTTPS 프로토콜을 사용하여 저장소를 복제할 수 있습니다. 
이렇게 하면 로컬 저장소가 생성되지만 다음과 같이 변경 사항을 직접 push 할 수는 없습니다.
<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>
  </li>
</ul>

<p>
<strong>팁:</strong> 전체 <code>webwml</code> 저장소를 복제하는 것은 약
1.3 GB 데이터를 내려받으며 이것은 느리거나 불안정안 인터넷 연결에서는 너무 느립니다.
따라서, 초기 다운로드에 최소 깊이를 정의할 수 있습니다:
</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>쓸모 있는 (얕은) 저장소를 얻은 다음, 로컬 사본을 심화하고 
결국 전체 로컬 저장소로 변환할 수 있습니다:</p>

<pre>
  git fetch --deepen=1000 # deepen the repo for another 1000 commits
  git fetch --unshallow   # fetch all missing commits, convert the repo to a complete one
</pre>

<p>페이지의 부분집합만 check out 할 수 있습니다:</p>

<ol>
  <li><code>git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git</code></li>
  <li><code>cd webwml</code></li>
  <li><code>git config core.sparseCheckout true</code></li>
  <li>Create the file <code>.git/info/sparse-checkout</code> inside the <code>webwml</code> 
  directory to define the content you want to check out. For example, 
  if you only want to retrieve the base files, English, Catalan, and Spanish translations, 
  the file looks like this:
    <pre>
      /*
      !/[a-z]*/
      /english/
      /catalan/
      /spanish/
    </pre></li>
  <li>마지막으로, 저장소를 체크 아웃: <code>git checkout --</code></li>
</ol>

<h3><a name="submit-changes">로컬 변경 제출</a></h3>

<h4><a name="keep-local-repo-up-to-date">로컬 저장소 최신으로 유지</a></h4>

<p>며칠에 한 번 (그리고 편집 작업 전!)
할 작업</p>

<pre>
  git pull
</pre>

<p>하여, 변경된 저장소에서 파일 검색합니다.</p>

<p>
<code>git pull</code> 수행하기 전에 로컬 Git 작업 디렉토리를 깨끗하게 유지하는 것이 좋습니다. 
현재 분기의 원격 저장소에 없는 커밋되지 않은 변경 사항이나 로컬 커밋이 있으면 <code>git pull</code> 하면 
자동으로 병합 커밋이 생성되거나 충돌로 인해 실패할 수도 있습니다. 
완료되지 않은 작업을 다른 분기에 보관하거나 <code>git stash</code> 같은 명령을 사용하는 것을 고려하십시오.
</p>

<p>
주의: Git은 분산(중앙 집중 아님) 버전 제어 시스템입니다. 
즉, 변경 사항을 커밋하면 로컬 저장소에만 저장됩니다. 
다른 사람들과 공유하려면 변경 사항을 Salsa의 중앙 저장소로 push해야 합니다.</p>

<h4><a name="example-edit-english-file">예시: 몇 파일 편집</a></h4>

<p>
좀 더 실용적인 예와 일반적인 편집 세션을 살펴보겠습니다. 
<code>git clone</code>을 사용하여 저장소의 <a href="#get-local-repo-copy">local copy</a>을 얻었다고 가정합니다. 
다음 단계:
</p>

<ol>
  <li><code>git pull</code></li>
  <li>이제 편집을 시작하고 파일을 변경합니다.</li>
  <li>다 되면, 여러분의 변경을 로컬 저장소에 커밋:
    <pre>
    git add path/to/file(s)
    git commit -m "커밋 메시지"
    </pre></li>
  <li><a href="#write-access">무제한 쓰기 권한</a>이 원격 <code>webwml</code> 저장소에 있으면, 
  Salsa 저장소에: <code>git push</code></li>
  <li><code>webwml</code> 저장소에 직접 쓰기 권한이 없다면, 
  여러분의 변경을 <a href="#write-access-via-merge-request">merge request</a> 또는 다른 개발자에게 도움을 위해 연락.</li>
</ol>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/docs/gittutorial">Git 문서</a></button></p>

<h4><a name="closing-debian-bug-in-git-commits">Closing Debian Bugs in Git Commits</a></h4>

<p>
If you include <code>Closes: #</code><var>nnnnnn</var> in your commit log
entry, then bug number <code>#</code><var>nnnnnn</var> will be closed
automatically when you push your changes. The precise form of this is the same as
<a href="$(DOC)/debian-policy/ch-source.html#id24">in Debian policy</a>.</p>

<h4><a name="links-using-http-https">HTTP/HTTPS 사용하는 링크</a></h4>

<p>
많은 데비안 웹사이트가 SSL/TLS를 지원하므로 가능하면 HTTPS 링크를 사용하십시오. 
<strong>그러나</strong>, 일부 Debian/DebConf/SPI/etc 웹사이트는 HTTPS를 지원하지 않거나 SPI CA만 사용합니다
(모든 브라우저가 SSL CA를 신뢰하는 건 아닙니다). 
비데비안 사용자에게 오류 메시지가 발생하지 않도록 하려면 HTTPS를 사용하여 이러한 사이트에 링크하지 마십시오.</p>
<p>
Git 저장소는 HTTPS를 지원하는 Debian 웹사이트용 플레인 HTTP 링크를 포함하거나 HTTPS를 지원하지 않거나 SPI가 서명한 증명서를 사용하는 것으로 알려진 Debian/DebConf/SPI 웹사이트용 HTTPS 링크를 포함하는 커밋을 거부합니다.</p>

<h3><a name="translation-work">번역 작업</a></h3>

<p>
번역은 항상 해당 영어 파일과 함께 최신 상태로 유지되어야 합니다. 
번역 파일의 <code>translation-check</code> 헤더는 현재 번역의 기반이 된 영어 파일 버전을 추적하는 데 사용됩니다. 
번역된 파일을 변경하는 경우 영어 파일에서 해당 변경 사항의 Git 커밋 해시와 일치하도록 <code>translation-check</code> 헤더를 업데이트해야 합니다. 
다음 명령으로 해시를 식별할 수 있습니다.
</p>

<pre>
  git log path/to/english/file
</pre>

<p>
파일의 새로운 번역을 할 경우 <code>copypage.pl</code> 스크립트를 사용하십시오. 
올바른 번역 헤더를 포함하여 여러분 언어에 대한 템플릿을 생성합니다.</p>

<h4><a name="translation-smart-change">smart_change.pl 사용한 번역 변경</a></h4>

<p><code>smart_change.pl</code>은 원본 파일과 해당 번역을 함께 업데이트하기 쉽게 하기
위한 스크립트입니다. 두 가지 방법이 있는데, 무엇을 바꾸냐에 따라 다릅니다.</p>

<p>이것은 <code>smart_change.pl</code>을 어떻게 쓰며, 수동으로 파일 작업 할 때 어떻게 <code>translation-check</code> 헤더를
업데이트 하는지 입니다.
</p>

<ol>
  <li>원본 파일을 변경하고 변경을 커밋.</li>
  <li>번역을 업데이트.</li>
  <li><code>smart_change.pl -c COMMIT_HASH</code> 실행(원본 파일 변경 커밋 해시 사용). 
  변경을 선택하고 번역 파일 헤더를 업데이트.</li>
  <li>변경을 리뷰 (예. <code>git diff</code>).</li>
  <li>번역 변경 커밋.</li>
</ol>

<p>
또는, 정규식으로 작업하여 한 번에 여러 파일을 변경할 수 있습니다.
</p>

<ol>
  <li>실행할 것 <code>smart_change.pl -s s/FOO/BAR/ origfile1 origfile2 ...</code></li>
  <li>변경 리뷰 (예. <code>git diff</code>).</li>
  <li>원본 파일 커밋.</li>
  <li>실행할 것 <code>smart_change.pl origfile1 origfile2</code>
    (즉 이번에는 <strong>regexp 없이</strong>). 
번역 파일 안의 헤더를 업데이트.</li>
  <li>마지막으로, 번역 변경을 커밋.</li>
</ol>

<p>
물론, 이것은 두 개의 커밋을 포함하기 때문에 첫 번째 예제보다 약간 더 많은 노력이 필요하지만, Git 해시 작동 방식으로 인해 피할 수 없습니다.
</p>

<h2><a id="write-access">Git 저장소 쓰기 권한</a></h2>

<p>
데비안 웹사이트의 소스 코드는 Git으로 관리되며 <url https://salsa.debian.org/webmaster-team/webwml/>에 있습니다. 
기본적으로 게스트는 소스 코드 리포지토리에 커밋을 푸시할 수 없습니다. 
데비안 웹사이트에 기여하려면 저장소에 대한 쓰기 권한을 얻기 위해 일종의 권한이 필요합니다.
</p>

<h3><a name="write-access-unlimited">무제한 쓰기 권한</a></h3>

<p>
저장소에 무제한 쓰기 권한 필요하면 예를 들어. 잦은 기여를 하려면,  
<url https://salsa.debian.org/webmaster-team/webwml/> 웹 인터페이스를 통해 데비안 Salsa 플랫폼에 로그인 해서
요청하세요.
</p>

<p>
데비안 웹사이트 개발이 처음이고 이전 경험이 없는 경우, 무제한 쓰기 권한을 요청하기 전에 
<a href="mailto:debian-www@lists.debian.org">
debian-www@lists.debian.org</a> 로 이메일을 보내 자신을 소개하십시오. 
예를 들어 웹사이트의 어느 부분에서 작업할 계획인지, 어떤 언어를 사용하는지, 
보증할 수 있는 다른 데비안 팀원이 있는지 등 자신에 대해 좀 더 친절하게 알려주시겠습니까?
</p>

<h3><a name="write-access-via-merge-request">병합 요청</a></h3>

<p>
저장소에 대한 무제한 쓰기 액세스 권한을 얻을 필요는 없습니다. 
언제든지 병합 요청을 제출하고 다른 개발자가 작업을 검토하고 수락하도록 할 수 있습니다. 
웹 인터페이스를 통해 Salsa GitLab 플랫폼에서 제공하는 병합 요청에 대한 표준 절차를 따르고 
다음 두 문서를 읽으십시오.
</p>

<ul>
  <li><a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Project forking workflow</a></li>
  <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">When you work in a fork</a></li>
</ul>

<p>
모든 웹사이트 개발자가 병합 요청을 모니터링하는 것은 아닙니다. 
따라서 피드백을 받기까지 시간이 걸릴 수 있습니다. 
여러분의 기여가 승인될지 여부가 궁금하면 
<a href="https://lists.debian.org/debian-www/">debian-www</a> 메일링 리스트에 이메일을 보내서 
검토를 요청하십시오.
</p>

<h2><a id="notifications">알림 받기</a></h2>

<p>
데비안 웹사이트에서 작업하고 있다면 <code>webwml</code> 저장소에서 무슨 일이 일어나고 있는지 알고 싶을 것입니다. 
루프를 유지하는 방법 2가지: 커밋 알림과 병합 요청 알림.
</p>

<h3><a name="commit-notifications">커밋 알림 받기</a></h3>

<p>We have configured the <code>webwml</code> project in Salsa so that commits are
shown in the IRC channel #debian-www.</p>

<p>
If you want to receive notifications about commits 
in the <code>webwml</code> repo via email, please subscribe to the <code>www.debian.org</code> 
pseudo package via tracker.debian.org and activate the <code>vcs</code> keyword 
there, following these steps (only once):</p>

<ol>
  <li>Open a web browser and go to <url https://tracker.debian.org/pkg/www.debian.org>.</li>
  <li>Subscribe to the <code>www.debian.org</code> pseudo package. (You can authenticate
      via SSO or register an email and password, if you aren't already using
      tracker.debian.org for other purposes).</li>
  <li>Go to <url https://tracker.debian.org/accounts/subscriptions/>, then to <code>modify
      keywords</code>, check <code>vcs</code> (if it's not checked) and save.</li>
  <li>From now on you will get emails when somebody commits to the
      <code>webwml</code> repo.</li>
</ol>

<h3><a name="merge-request-notifications">병합 요청 알림 받기</a></h3>

<p>
Salsa의 <code>webwml</code> 저장소에 제출된 새로운 병합 요청이 있을 때마다 알림 이메일을 받으려면 
다음 단계에 따라 웹 인터페이스에서 알림 설정을 구성할 수 있습니다.
</p>

<ol>
  <li>Salsa 계정에 로그인하고 project 페이지로.</li>
  <li>project 홈페이지 꼭대기 종 아이콘 클릭.</li>
  <li>좋아하는 알림 레벨 선택.</li>
</ol>
