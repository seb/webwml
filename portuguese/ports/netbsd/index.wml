#use wml::debian::template title="Debian GNU/NetBSD" BARETITLE="yes" NOHEADER="yes"
#use wml::fmt::verbatim
#use wml::debian::translation-check translation="c9a7e0f78250fe2fea728e669907c9ee47374e1c"

#############################################################################
<div class="important">
<p><strong>
Este trabalho de porte foi abandonado faz tempo. Ele não tem atualizações desde
outubro de 2002. A informação nesta página é somente para propósitos históricos
</strong></p>
</div>

<h1>
Debian GNU/NetBSD
</h1>

<p>
O Debian GNU/NetBSD (i386) é um porte do sistema operacional Debian para o
kernel NetBSD e a libc (não confundir com outros portes Debian BSD
baseados em glibc). Na época em que foi abandonado (por volta de outubro de
2002), estava em um estágio inicial de desenvolvimento - no entanto, era
instalável desde o início. 
</p>

<p>
Houve também uma tentativa de iniciar um port Debian GNU/NetBSD (alpha), que
poderia ser executado a partir de um chroot em um sistema NetBSD (alpha) nativo,
mas não era capaz de inicializar por conta própria, e usava a maior parte da
bibliotecas nativas do NetBSD.
Uma <a
href = "https://lists.debian.org/debian-bsd/2002/debian-bsd-200201/msg00203.html">mensagem de status</a>
foi enviada para a lista.
</p> 

<h1>Notícias históricas</h1>

<dl class="gloss">
  <dt class="new">2002-10-06:</dt>
  <dd>
      Disquetes de instalação experimental agora estão disponíveis para a
      instalação de um sistema Debian GNU/NetBSD.
  </dd>
  <dt>2002-03-06:</dt>
  <dd>
      Matthew hackeou o <a href="https://packages.debian.org/ifupdown">ifupdown</a>
      para um estado funcional.
  </dd>
  <dt>2002-02-25:</dt>
  <dd>
      Matthew relatou que o suporte a shadow e PAM funciona no NetBSD
      agora. O <a href="https://packages.debian.org/fakeroot">fakeroot</a>
      parece funcionar no FreeBSD, mas ainda tem problemas no NetBSD.
  </dd>
  <dt>2002-02-07:</dt>
  <dd>
      Nathan acabou de <a
      href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00091.html">relatar</a>
      que conseguiu que o Debian GNU/FreeBSD fizesse boot multiusuário(a). Além
      disso, ele está trabalhando em uma instalação exclusiva de pacotes
      (usando um debootstrap hackeado) com foco em um tarball
      consideravelmente menor.
  </dd>
  <dt>2002-02-06:</dt>
  <dd>
      De acordo com Joel, o gcc-2.95.4 passou pela maioria de suas suítes de
      testes e está empacotado.
  </dd>
  <dt>2002-02-06:</dt>
  <dd>O X11 funciona no NetBSD!  Novamente, um salve para o Joel Baker
  </dd>
  <dt>2002-02-04:</dt>
  <dd>Os primeiros passos para um repositório Debian/*BSD: <br />
    <a href="mailto:lucifer@lightbearer.com">Joel Baker</a>
    <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00067.html">
     anunciou</a> um repositório com <kbd>dupload</kbd> para pacotes Debian
     FreeBSD e NetBSD.
  </dd>
  <dt>2002-02-03:</dt>
  <dd>O Debian GNU/NetBSD agora com
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00043.html">
        hospedagem própria</a>!  Note que ela ainda precisa de um NetBSD
	funcionando para a instalação.
  </dd>
  <dt>2002-01-30:</dt>
  <dd>O porte do Debian GNU/*BSD agora tem uma página web!</dd>
</dl>

<h1>Por que Debian GNU/NetBSD?</h1>

<ul>
<li>O NetBSD roda em hardware não suportado pelo Linux. Portar o Debian para
o kernel NetBSD aumenta o número de plataformas que podem rodar um
sistema operacional baseado em Debian.</li>

<li>O projeto Debian GNU/Hurd demonstra que o Debian não está amarrado
a um kernel específico. Entretanto, o kernel Hurd ainda está relativamente
imaturo - o sistema Debian GNU/NetBSD seria utilizável em um nível
produtivo.</li>

<li>As lições aprendidas a partir do porte do Debian para o NetBSD podem ser
usadas no porte do Debian para outros kernels (como aqueles do <a
href="https://www.freebsd.org/">FreeBSD</a> e <a
href="http://www.openbsd.org/">OpenBSD</a>).</li>

<li>Em contraste com projetos como <a href="http://www.finkproject.org/">Fink</a>
ou <a href="http://debian-cygwin.sf.net/">Debian GNU/w32</a>, o Debian
GNU/NetBSD não existe para fornecer software extra ou um ambiente
estilo Unix para um SO existente (as árvores dos portes *BSD já são
abrangentes e elas sem dúvidas fornecem um ambiente estilo Unix). Ao contrário,
um(a) usuário(a) ou um(a) administrador(a) acostumado(a) a um sistema
Debian mais tradicional deve se sentir confortável com um sistema
Debian GNU/NetBSD imediatamente e de forma competente em um relativo curto
período de tempo.</li>

<li>Nem todo mundo gosta da árvore de portes do *BSD ou do espaço de usuário(a)
*BSD (isto é uma preferência pessoal em vez de qualquer tipo de comentário
sobre qualidade). As distribuições Linux têm sido produzidas para fornecer
portes estilo *BSD ou um espaço de usuário(a) estilo *BSD para aqueles(as)
que gostam do ambiente de usuário(a) BSD, mas também desejam usar o kernel
Linux - o Debian GNU/NetBSD é a lógica reversa disso, permitindo que as
pessoas que gostem do espaço de usuário(a) GNU ou de um sistema de
empacotamento estilo Linux usem o kernel NetBSD.</li>

<li>Porque nós podemos.</li>
</ul>

<h2>
Recursos
</h2>

<p>
Existe uma lista de discussão Debian GNU/*BSD. A maioria das discussões
históricas sobre este porto aconteceu lá, que podem ser acessados a partir dos
arquivos web em
<url "https://lists.debian.org/debian-bsd/" />.
</p>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:
