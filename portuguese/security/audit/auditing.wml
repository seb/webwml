#use wml::debian::template title="Conduzindo uma auditoria"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="0c51b8ff34c17868bd2f86ac91fef7abc581e1e9"


<p>Esta página oferece uma visão geral sobre os passos necessários para se
conduzir uma auditoria de um pacote.</p>

<p>O primeiro passo é escolher um pacote para examinar, você deve
selecionar um que é mais crítico em relação à segurança.</p>

<p>Veja <a href="$(HOME)/security/audit/packages">a lista de pacotes
que pensamos ser os mais importantes para serem auditados</a>, para sugestões
de como tomar sua decisão.</p>

<p>Uma coisa que deve estar clara é que nós <em>não</em> estamos tentando
fazer com que um pacote seja auditado somente uma vez. Se muitas pessoas
decidirem examinar um mesmo pacote isso é uma coisa boa, já que demonstra que
muitas pessoas acreditam que o pacote é sensível à segurança.</p>

<p>Por permitir uma seleção essencialmente aleatória de pacotes, nós
simplificamos a coordenação e eliminamos o problema de <q>como você pode
confiar na pessoa X para fazer um bom trabalho?</q> (Nós não precisamos
confiar, já que assumimos que cedo ou tarde alguém decidirá examinar o
mesmo programa).</p>

<h2>Começando a auditoria</h2>

<p>Após fazer a escolha do seu pacote, você deve iniciar a auditoria.</p>

<p>Se você não tem certeza sobre o tipo de problemas que está procurando,
primeiro leia um livro sobre como desenvolver softwares seguros.</p>

<p>O <a href="http://www.dwheeler.com/secure-programs">HOWTO programação
segura para Linux e Unix</a> (em inglês) tem muitas e boas informações
que podem te ajudar.
<a href="http://www.securecoding.org/">Código seguro: Princípios &amp; Práticas</a>
(em inglês) do Mark G. Graff e Kenneth R. van Wyk também é um livro
excelente.</p>

<p>Embora as ferramentas sejam imperfeitas, elas ainda assim podem ser
extremamente úteis para encontrar prováveis vulnerabilidades. Veja a
<a href="tools">página de ferramentas de auditoria</a> para mais informações
sobre algumas das ferramentas de auditoria disponíveis e como elas podem
ser usadas.</p>

<p>Tão importante quanto olhar o código mesmo é ler a
documentação do pacote e tentar instalá-lo usando essa documentação.</p>

<p>Isto pode permitir que você pense em maneiras de subverter o programa em
suas operações típicas.</p>

<h2>Relatando problemas</h2>

<p>Se você descobrir um problema dentro do pacote que está examinando, então
você deve relatá-lo. Quando relatar um bug de segurança, tente fornecer
também uma correção, de modo que os(as) desenvolvedores(as) possam
consertá-lo em um período oportuno. Não há necessidade de fornecer uma
demonstração de ataque (frequentemente chamada de <em>vulnerabilidade (exploit)</em>
ou <em>validação do conceito (proof of concept)</em>), já que a correção
deve falar por si mesma. É geralmente melhor investir tempo no fornecimento
de uma correção adequada do que fornecer um ataque de sucesso que
explore o bug.</p>

<p>Aqui está uma lista de passos recomendados uma vez que você tenha encontrado
um bug de segurança no Debian.</p>

<ol>

<li>Tente produzir uma correção para o bug ou obter informação
suficiente para que outros(as) possam determinar a existência do bug.
Idealmente, cada relatório deve conter uma correção para o problema que foi
descoberto, correção que foi testada e verificada para realmente acabar com
o problema.

<p>Se você não tiver uma correção, o melhor é dar mais detalhes
sobre o escopo do problema, sobre a severidade relativa ao problema e qualquer
forma de contorná-lo.</p></li>

<li>Primeiro revise se o bug de segurança está presente na versão estável
(stable) do Debian ou se ele poderia estar presente em outras distribuições
ou em uma versão fornecida pelos(as) mantenedores(as) do código.</li>

<li>Baseado na revisão acima, relate o problema:

<ul>

<li>Para os(as) mantenedores(as) do código através do e-mail de contato de
segurança deles(as), fornecendo a análise e a correção.</li>

<li>Para o time de segurança do Debian se o bug está presente em uma versão
lançada do Debian. O time de segurança do Debian normalmente atribuirá um
<a href="$(HOME)/security/cve-compatibility">nome CVE</a> para a
vulnerabilidade. O time de segurança coordenará com outras distribuições Linux
se necessário e fará o contato com o(a) mantenedor(a) do pacote em seu nome.
Você também pode, contudo, enviar uma cópia do e-mail para o(a) mantenedor(a)
do pacote. Faça isso somente quando trabalhar com vulnerabilidades de baixo
risco (veja abaixo).</li>

<li>Se o bug não está presente em uma versão lançada do Debian e o
aplicativo poderia estar presente em outras distribuições ou sistemas
operacionais, então mande um e-mail (em inglês) para <a
href="http://oss-security.openwall.org/wiki/mailing-lists/oss-security">oss-security</a>
(uma lista de discussão pública usada para relatar e discutir bugs de segurança
que foram divulgados publicamente). Você não precisa fazer isso se já enviou
o bug para o time de segurança do Debian, já que o time também vai encaminhar o
e-mail para a lista.</li>

<li>Se o bug <strong>não</strong> está presente em uma versão lançada do Debian
e você está absolutamente certo(a) de que a aplicação <strong>não</strong> está
incluída em outras distribuições ou sistemas operacionais, então relate-o
através do sistema de rastreamento de bugs.</li>

</ul></li>

<li>Tão logo a vulnerabilidade for publicada (ou seja, quando o time de
segurança do Debian ou outro(a) fornecedor(a) enviou um alerta), um bug com
todas as informações relevantes deve ser registrado no sistema de rastreamento
de bugs do Debian para que se acompanhe o problema de segurança nas versões
não lançadas do Debian (isto é, <em>sid</em> ou <em>teste (testing)</em>).
Isto é feito pelo próprio time de segurança se você descobrir que eles não
fizeram isso ou você não relatou o bug para o time de segurança, então você
pode relatá-lo você mesmo(a). Certifique-se de colocar a tag adequada ao bug
(use a tag <em>security</em>, e de definir a prioridade adequada
(geralmente <em>grave</em> ou maior). Também certifique-se de que o título
do bug inclua o <a href="$(HOME)/security/cve-compatibility">nome CVE</a>
adequado se este nome já foi atribuído ao bug. Isto fornece uma maneira de
acompanhar os bugs de segurança de modo que eles sejam corrigidos tanto nas
versões lançadas como nas versão não lançadas do Debian.</li>

<li>Se desejar, uma vez que o problema seja público, você pode encaminhar esta
informação para listas de discussões de divulgação pública maior, como a
<a href="https://lists.grok.org.uk/mailman/listinfo/full-disclosure">full-disclosure</a>
ou a
<a href="http://www.securityfocus.com/archive/1">Bugtraq</a>.</li>

</ol>

<p>Observe que esses passos podem variar, baseados no risco associado
à vulnerabilidade encontrada. Você precisa avaliar o risco baseado(a) em:</p>

<ul>
<li>Se a vulnerabilidade é remota ou local.</li>
<li>As consequências da vulnerabilidade, se explorada.</li>
<li>O uso generalizado do software afetado pela vulnerabilidade.</li>
</ul>

<p>Diferentes etapas devem ser tomadas, por exemplo, uma coisa é relatar um
ataque de symlink local que só pode ser usado por usuários(as) autenticados(as)
e que somente fornece um caminho de dano para o sistema. Outra coisa é relatar
um buffer overflow remoto que fornece privilégios administrativos e está
presente em um software que é largamente utilizado.</p>

<p>Na maior parte dos casos, como a maioria dos bugs de segurança geralmente
não deve ser divulgada até depois que tenha sido corrigida, <em>não</em> relate,
como é usual, os bugs através do <a href="https://bugs.debian.org/">sistema de
rastreamento de bugs do Debian</a>. Em vez disso, relate o problema diretamente
para <a href="$(HOME)/security/">o time de segurança</a> que cuidará do
lançamento de um pacote atualizado e, uma vez corrigido, o relatará para o
BTS.</p>
