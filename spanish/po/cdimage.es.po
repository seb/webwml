# Traducción del sitio web de Debian
# Copyright (C) 2004 SPI Inc.
# Javier Fernández-Sanguino <jfs@debian.org>, 2004
#
msgid ""
msgstr ""
"Project-Id-Version: Templates webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-08-09 12:57+0200\n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:13
msgid "      Key fingerprint"
msgstr "      Huella de clave"

#: ../../english/devel/debian-installer/images.data:90
msgid "ISO images"
msgstr "imágenes ISO"

#: ../../english/devel/debian-installer/images.data:91
msgid "Jigdo files"
msgstr "archivos Jigdo"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "&middot;"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />Preguntas frecuentes"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "Descargar con jigdo"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "Descargar mediante HTTP/FTP"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "Comprar CD o DVD"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "Instalación via red"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />Descargar"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />miscelánea"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "<void id=\"dc_artwork\" />Arte"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />Réplicas"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />Réplicas rsync"

#: ../../english/template/debian/cdimage.wml:43
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_verify\" />Verificar"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />Descargar con Torrent"

#: ../../english/template/debian/cdimage.wml:49
msgid "Debian CD team"
msgstr "Equipo de CD de Debian"

#: ../../english/template/debian/cdimage.wml:52
msgid "debian_on_cd"
msgstr "debian_en_cd"

#: ../../english/template/debian/cdimage.wml:55
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />faq"

#: ../../english/template/debian/cdimage.wml:58
msgid "jigdo"
msgstr "jigdo"

#: ../../english/template/debian/cdimage.wml:61
msgid "http_ftp"
msgstr "http_ftp"

#: ../../english/template/debian/cdimage.wml:64
msgid "buy"
msgstr "comprar"

#: ../../english/template/debian/cdimage.wml:67
msgid "net_install"
msgstr "instalación_por_red"

#: ../../english/template/debian/cdimage.wml:70
msgid "<void id=\"misc-bottom\" />misc"
msgstr "<void id=\"misc-bottom\" />misc"

#: ../../english/template/debian/cdimage.wml:73
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"<a href=\"/MailingLists/disclaimer\">Lista pública de correo</a> en inglés "
"para CD/DVD:"

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id=\"dc_relinfo\" />Información de imágenes de distribución"
