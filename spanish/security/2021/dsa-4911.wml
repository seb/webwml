#use wml::debian::translation-check translation="35405b68aa31c415947c277663980744599dc1e0"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el navegador web Chromium:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21227">CVE-2021-21227</a>

    <p>Gengming Liu descubrió un problema de validación de datos en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21228">CVE-2021-21228</a>

    <p>Rob Wu descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21229">CVE-2021-21229</a>

    <p>Mohit Raj descubrió un error en la interfaz de usuario de la descarga de ficheros.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21230">CVE-2021-21230</a>

    <p>Manfred Paul descubrió el uso de un tipo incorrecto.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21231">CVE-2021-21231</a>

    <p>Sergei Glazunov descubrió un problema de validación de datos en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21232">CVE-2021-21232</a>

    <p>Abdulrahman Alqabandi descubrió un problema de «uso tras liberar» en las herramientas de
    programación.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21233">CVE-2021-21233</a>

    <p>Omair descubrió un problema de desbordamiento de memoria en la biblioteca ANGLE.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 90.0.4430.93-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de chromium.</p>

<p>Para información detallada sobre el estado de seguridad de chromium, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4911.data"
