#use wml::debian::template title="Support"
#use wml::debian::translation-check translation="0d2b6f2d18fa25263c9500a1977d01a576281ca0"
#use wml::debian::toc

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<p>Debian och dess support drivs av en gemenskap av frivilliga. Om denna
gemenskapsdrivna support inte uppfyller dina behov kan
du läsa vår <a href="doc">dokumentation</a> eller hyra en
<a href="consultants">konsult</a>.</p>

<toc-display />

<toc-add-entry name="irc">Direkthjälp via IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a>
är ett sätt att chatta med folk över hela
världen i realtid.
IRC-kanaler avsedda för Debian finns på
<a href="https://www.oftc.net/">OFTC</a>.
</p>

<p>
För att ansluta sig till den behöver du en IRC-klient. Några av de mest
populära är
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a>
samt
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
alla tillgängliga som Debianpaket. OFTC tillhandahåller även ett
<a href="https://www.oftc.net/WebChat/">WebChat</a>-gränssnitt som
tillåter dig att ansluta till IRC med en webbläsare utan att behöva
installera en lokal IRC-klient.
</p>

<p>När du har installerat en klient måste du tala om för den att ansluta sig
till vår server. I de flesta klienter gör du detta genom att skriva:
</p>

<pre>
/server irc.debian.org
</pre>

<p>I vissa klienter (som irssi) måste du skriva följande istället:</p>

<pre>
/connect irc.debian.org
</pre>

<p>
När du väl har anslutit dig går du in på kanalen <code>#debian</code>
genom att skriva
</p>

<pre>
/join #debian
</pre>

<p>
Observera att grafiska klienter såsom HexChat ofta har andra, grafiska
gränssnitt för att byta server och kanal.
</p>

<p>
Nu kommer du att finna dig bland en vänlig folkhop bestående
av <code>#debian</code>-invånare.
Kanalens frågor och svardokument finns på
<url "https://wiki.debian.org/DebianIRC" />.
</p>

<p>
En svenskspråkig kanal med liknande publik är <code>#debian.se</code>.
</p>


<p>
Det finns också ett antal andra IRC-nätverk där du kan prata om Debian.
</p>


<toc-add-entry name="mail_lists" href="MailingLists/">Sändlistor</toc-add-entry>

<p>Debian utvecklas utbrett över hela världen, och därför är
sändlistor det sätt som föredras för att diskutera saker.
En stor del av konversationen mellan Debians utvecklare och användare sker på
sändlistorna.
</p>

<p>Det finns flera sändlistor som är allmänt tillgängliga.
För ytterligare information, se
<a href="MailingLists/">Debians sändlistesida</a>.
</p>

<p>
För användarsupport på svenska, kontakta
sändlistan
<a href="https://lists.debian.org/debian-user-swedish/">debian-user-swedish</a>.
Den engelska motsvarigheten är
<a href="https://lists.debian.org/debian-user/">debian-user</a>.
</p>

<p>
För användarsupport på andra språk, se
<a href="https://lists.debian.org/users.html">indexet över
sändlistor för användare</a>.
</p>

<p>Det finns förstås många andra sändlistor tillägnade någon aspekt
av det rika Linuxekosystemet, vilka inte är Debianspecifika. Använd din
favoritsökmotor för att hitta den lista som passar ditt ändamål bäst.</p>

<toc-add-entry name="usenet">Diskussionsgrupper</toc-add-entry>

<p>
Många av våra <a href="#mail_lists">sändlistor</a> kan även läsas som
diskussionsgrupper (<span lang="en">news</span>) i hierarkin
<kbd>linux.debian.*</kbd>.
De kan även nås via webbgränssnitt som
<a href="https://groups.google.com/forum/">Google Groups</a>.
</p>

<toc-add-entry name="web">Webbplatser</toc-add-entry>

<h3 id="forums">Forum</h3>

<p><a href="https://forums.debian.net">Debian User Forums</a>
är en webbportal där du kan diskutera Debian-relaterade ämnen,
ställa frågor om Debian och få dem
besvarade av andra användare.
</p>

<toc-add-entry name="maintainers">Nå paketens ansvariga</toc-add-entry>

<p>Det finns två sätt att nå de ansvariga för paketen i Debian.
Om du vill kontakta den ansvarige på grund av ett programfel (bugg) skickar
du helt enkelt in en felrapport (se avsnittet om felrapporteringssystemet
nedan), varpå den ansvarige får en kopia av felrapporten.
</p>

<p>Om du bara vill kommunicera med den ansvarige kan du använda de
speciella e-postalias som har satts upp för varje paket.
All post som skickas till &lt;<em>paketnamn</em>&gt;@packages.debian.org
kommer att
skickas vidare till den som är ansvarig för just det paketet.
</p>

<toc-add-entry name="bts" href="Bugs/">Felrapporteringssystemet</toc-add-entry>

<p>Debian har ett felrapporteringssystem vilket
innehåller detaljer om fel som har rapporteras av såväl användare som
utvecklare.
Varje felrapport ges ett nummer, och hålls kvar i arkivet tills det har
markerats som avhjälpt.
</p>

<p>För att rapportera ett fel kan du läsa felsidan nedan; vi rekommenderar
att du använder dig av Debianpaketet <q>reportbug</q> för att automatiskt
skapa en felrapport.</p>

<p>Information om hur du skickar in fel, tittar på de befintliga felen, och
om felrapporteringssystemet i allmänhet finns på
<a href="Bugs/">felrapporteringens webbsidor</a>.
</p>

<toc-add-entry name="doc" href="doc/">Dokumentation</toc-add-entry>

<p>En viktig del av alla operativsystem är dess dokumentation, de
tekniska manualerna som beskriver hantering och användning av program. Som
en del av dess mål för att skapa ett högkvalitativt fritt operativsystem
gör Debianprojektet insatser för att tillhandahålla alla sina användare
bra dokumentation i ett lätttillgängligt format.</p>

<p>Se <a href="doc/">dokumentationssidan</a> för en lista på Debianmanualer
och annan dokumentation, inklusive installationsguiden, Debian FAQ, och
andra användar- och utvecklarmanualer.</p>

<toc-add-entry name="consultants" href="consultants/">Konsulter</toc-add-entry>

<p>Debian är fri programvara och erbjuder hjälp genom sändlistor.
Vissa har inte tid, eller har speciella krav, och är villiga att
anlita någon att antingen underhålla eller lägga till ytterligare
funktionalitet till sitt Debiansystem.
Se <a href="consultants/">konsultsidan</a> för en lista över personer och
företag som erbjuder sådana tjänster.
</p>


<toc-add-entry name="release" href="releases/stable/">Kända problem</toc-add-entry>

<p>Begränsningar och allvarliga problem i den aktuella stabila utgåvan (om
sådana finns) beskrivs i <a href="releases/stable/">utgåvesidorna</a>.</p>

<p>Uppmärksamma speciellt <a href="releases/stable/releasenotes">\
versionsfakta</a> samt <a href="releases/stable/errata">errata</a>.</p>
