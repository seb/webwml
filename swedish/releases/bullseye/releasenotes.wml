#use wml::debian::template title="Debian 11 -- Versionsfakta" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="ce7e85638a047b969574a7abeb8a634c27b9086b"

<if-stable-release release="stretch">
<p>Detta är en <strong>version där arbete pågår</strong> av versionsfakta 
för Debian 10, med kodnamn Buster, som inte har släppts ännu. Informationen
som presenteras här kan vara felaktig och gammal och är med stor sannolikhet
ofullständig.</p>
</if-stable-release>

<if-stable-release release="buster">
<p>Detta är en <strong>version där arbete pågår</strong> av versionsfakta 
för Debian 11, med kodnamn Bullseye, som inte har släppts ännu. Informationen
som presenteras här kan vara felaktig och gammal och är med stor sannolikhet
ofullständig.</p>
</if-stable-release>

<p>För att få reda på vad som är nytt i Debian 11, se versionsfakta för din
arkitektur:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Versionsfakta'); :>
</ul>

<p>Versionsfakta innehåller även information för användare som uppgraderar från
tidigare utgåvor.</p>

<p>Om du har ställt in din webbläsares språkanpassning korrekt kommer länkarna
ovan att automatiskt hämta rätt HTML-version &mdash; se 
<a href="$(HOME)/intro/cn">innehållsförhandling</a>. Annars, välj den 
kombination av arkitektur, språk och format du vill ha från tabellen nedan.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arkitektur</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Språk</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
