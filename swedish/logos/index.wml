#use wml::debian::template title="Debianlogotyper" BARETITLE=true
#include "$(ENGLISHDIR)/logos/index.data"
#use wml::debian::translation-check translation="97732ca8593e39ce8b981adf7f81657417b62c73"

<p>Trots att Debian är fritt hämtbart, och alltid kommer att så förbli, har
vissa händelser, som problemet med ägarskapet av termen "Linux" visat att
Debian måste skydda sin egendom mot användning som kan skada dess rykte.</p>

<p>Debian har valt att framställa två logotyper:
En för <a href="#open-use">officiellt bruk</a> (även känd som
<q>open use logo</q>, eller på svenska <q>Debians logotyp för fri 
användning</q>) 
som innehåller Debians välkända <q>virvel</q> och representerar bäst Debians 
visuella identitet. En annan <a href="#restricted-use">logotyp med begränsad 
användning</a> finns också, endast för användning av Debianprojektet och dess 
medlemmar. För att referera till Debianprojektet ombeds man använda logotypen 
för fri användning.</p>

<hr>

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
	<tr>
		<th colspan="2"><a name="open-use">Debian Open Use logotyp</a></th>
	</tr>
	<tr>
		<td>
			 <p>Debian Open Use Logo (Debians logotyp för fri användning) finns i 
    	    två varianter, med och utan texten &ldquo;Debian&rdquo;.</p>

    	    <p>Debian Open Use Logo(s) är upphovsrättsligt skyddad (c) 1999 av
    	    <a href="https://www.spi-inc.org/">Software in the Public Interest, 
    	    Inc.</a>, och licensierad enligt villkoren i
    	    <a href="https://www.gnu.org/copyleft/lgpl.html">GNU Lesser General 
    	    Public License</a>, version 3 eller senare version, eller, 
    	    om önskvärt, under 
    	    <a href="https://creativecommons.org/licenses/by-sa/3.0/deed.sv">\
    	    Creative Commons Attribution-ShareAlike 3.0 Unported License</a> 
    	    (Creative Commons Erkännande-DelaLika 3.0 Unported).</p>

    	    <p>Observera: vi ser gärna att du låter bilden länka till
    	    <a href="$(HOME)">https://www.debian.org/</a> om du använder den på
    	    en webbsida.</p>
		</td>
		<td>
			<openlogotable>
		</td>
	</tr>
</table>

<hr>

<table cellspacing="0" cellpadding="5" width="100%">
	<colgroup span="2">
	<col width="65%">
	<col width="35%">
	<tr>
		<th colspan="2"><a name="restricted-use">Debian Restricted Use logotyp</a></th>
	</tr>
	<tr>
		<td>
			<h3>Licens för Debians officiella logotyp</h3>

			<p>Obs! Den <a href="index.en.html">engelskspråkiga sidan</a> innehåller den
			exakta ordalydelsen, detta är bara en översättning.</p>

			<p>Upphovsrättsskyddad © 1999 Software in the Public Interest
			<ol>
			<li>Denna logotyp får endast användas då
				<ul>
				<li>produkten den används för är tillverkad i enlighet med ett
					dokumenterat tillvägagångssätt, såsom publicerats på www.debian.org
					(exempelvis framställande av officiella cd-skivor),
					eller
				</li>
				<li>officiellt godkännande ges av Debian för användning för detta
					specifika ändamål
				</li>
				</ul>
			</li>
			<li>Den kan användas för en officiell del av Debian (vilket bestäms
				enligt reglerna i I), vilken är del av en komplett produkt, om
				det görs klart att bara denna del är officiellt godkänd
			</li>
			<li>Vi reserverar rätten att återkalla licensen för en produkt
			</li>
		</ol>
		<p>Tillstånd har givits för att använda den officiella logotypen på
			klädesplagg (tröjor, kepsar, osv.) så länge de görs av en Debianutvecklare
			och inte säljs med vinstintresse.
		</td>
		<td>
		<officiallogotable>
		</td>
	</tr>
</table>

<hr>

<h2>Om Debians logotyper</h2>
<p>
Debians logotyper valdes med hjälp av en omröstning bland Debianutvecklarna 
1999. De skapades av <a href="mailto:rsilva@debian.org">Raul Silva</a>. Den röda
färgen som används i typsnittet är nominellt <strong>Rubine Red 2X CVC</strong>.
Den aktuella motsvarigheten är antingen PANTONE Strong Red C (Renderas i RGB
som #CE0056) eller PANTONE Rubine Red C (renderas i RGB som #CE0058). Du hittar
mer detaljer om logotyperna och orsaken till att PANTONE Rubine Red 2X CVC
har ersatts och om andra motsvarigheter till den röda färgen på
<a href="https://wiki.debian.org/DebianLogo">wikisidan för Debianloggan</a>.</p>

<h2>Andra marknadföringsbilder</h2>

<h3>Debianknappar</h3>

<p><img class="ico" src="button-1.gif" alt="[Debian GNU/Linux]">
Detta är den första knappen som gjorts för projektet.
Licensen för denna logotyp är densamma som för den öppna logotypen.
Logotypen skapades av
<a href="mailto:csmall@debian.org">Craig Small</a>.</p>

<p>Här är några ytterligare knappar som har gjorts för Debian:
<br />
<morebuttons>

<h3>Debians mångfaldslogotyp</h3>

<p>Det finns en variant på Debianloggan för att främja mångfald i vår gemenskap,
som kallas för Debians mångfaldslogotyp:
<br/>
<img src="diversity-2019.png" alt="[Debians mångfaldslogotyp]" />
<br/>
Logotypen skapades av <a href="https://wiki.debian.org/ValessioBrito">Valessio Brito</a>
och licensieras under GPLv3.
Källkoden (SVG-format) finns tillgänglig i skaparens <a href="https://gitlab.com/valessiobrito/artwork">Gitförråd</a>.
<br/>
</p>

<h3>Debians hexagonala klistermärke</h3>

<p>Detta är ett klistermärke som följer <a
href="https://github.com/terinjokes/StickerConstructorSpec">specifikationen
för hexagonala klistermärken</a>:
<br/>
<img src="hexgonal.png" alt="[Debian GNU/Linux]" />
<br/>
Källkoden (SVG-format) och en Makefil för att generera png- och svg-förhandsgranskningarna
finns tillgängliga i <a 
href="https://salsa.debian.org/debian/debian-flyers/tree/master/hexagonal-sticker">\
Debians förråd för flygblad</a>.

Licensen för detta klistermärke är ekvivalent med Open Use logo licensen.
Klistermärket skapades av <a href="mailto:valhalla@trueelena.org">Elena Grandi</a>.</p>
<br />
