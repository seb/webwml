<define-tag pagetitle>Debian 9 aktualisiert: 9.6 veröffentlicht</define-tag>
<define-tag release_date>2018-11-10</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="ea6a13e28f99e15f67ccfd6b74b4cc4bb185fdbd" maintainer="Erik Pfannenstein"

<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die sechste Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Stable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können. 
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser 
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen ird. Eine vollständige Liste der Spiegelserver ist verfügbar unter: 
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction accerciser "Zugriffszeiten ohne Compositor korrigiert; Python-Konsole überarbeitet; fehlende Abhängigkeit von python3-xlib">
<correction apache2 "mod_http2: Dienstblockade durch Worker-Erschöpfung [CVE-2018-1333] und durch kontinuierliche SETTINGS [CVE-2018-11763]; mod_proxy_fcgi: Speicherzugriffsfehler behoben">
<correction base-files "/etc/debian_version auf die Zwischenveröffentlichung aktualisiert">
<correction brltty "Polkit-Authentifizierung überarbeitet">
<correction canna "Dateikonflikt zwischen canna-dbgsym und canna-utils-dbgsym behoben">
<correction cargo "Neues Paket, um Firefox ESR60 zu unterstützen">
<correction clamav "Neue Version der Originalautoren; Anfälligkeiten für HWP-Integer-Überlauf und Endlosschleife behoben [CVE-2018-0360]; Problem mit PDF-Objektlängenprüfung behoben, wegen dem das Auswerten recht kleiner Dateien unzumutbar lange gedauert hat [CVE-2018-0361]; neue Version der Originalautoren; Dienstblockade behoben [CVE-2018-15378]; Endlosschleife in dpkg-reconfigure behoben">
<correction confuse "Lesezugriff außerhalb der Grenzen in trim_whitespace behoben [CVE-2018-14447]">
<correction debian-installer "Auf -8er-Kernel-ABI aktualisiert">
<correction debian-installer-netboot-images "Neubau für die Zwischenveröffentlichung">
<correction dnsmasq "trust-anchors.conf: neuesten DNS-Vertrauensanker KSK-2017 eingepflegt">
<correction dom4j "XML-Injektions-Angriff behoben [CVE-2018-1000632]; mit source/target 1.5 kompiliert, um ein Problem mit String.format zu beseitigen">
<correction dpdk "Neue Version der Originalautoren">
<correction dropbear "Anfälligkeit für Anwender-Enumeration behoben [CVE-2018-15599]">
<correction easytag "OGG-Korrumpierung behoben">
<correction enigmail "Kompatibilität mit neueren Thunderbird-Versionen hinzugefügt">
<correction espeakup "espeakup.service: Beim Daemon-Start automatisch speakup_soft laden">
<correction fastforward "Speicherzugriffsfehler auf 64-bit-Architekturen behoben">
<correction firetray "Kompatibilität mit neueren Thunderbird-Versionen hinzugefügt">
<correction firmware-nonfree "Sicherheitsprobleme in Broadcom-WLAN-Firmware behoben [CVE-2016-0801 CVE-2017-0561 CVE-2017-9417 CVE-2017-13077 CVE-2017-13078 CVE-2017-13079 CVE-2017-13080 CVE-2017-13081]; Übergangspaket für firmware-{adi,ralink} wieder hinzugefügt">
<correction fofix-dfsg "Fehler beim Start behoben">
<correction fuse "autofs und FAT als valide Einhängepunkt-Dateisysteme weißlisten">
<correction ganeti "SSL-Zertifikate während VM-Export ordnungsgemäß verifizieren; generierte Zertifikate statt mit SHA1 mit SHA256 signieren1; Bash-Vervollständigungen automatisch ladbar machen">
<correction globus-gsi-credential "Problem mit voms-Proxy und openssl 1.1 behoben">
<correction gnupg2 "Sicherheitskorrekturen; Funktionsmerkmal zurückportiert, welches vom neuen Enigmail benötigt wird">
<correction gnutls28 "Sicherheitsprobleme behoben [CVE-2018-10844 CVE-2018-10845]">
<correction gphoto2-cffi "python3-gphoto2cffi wieder zum Laufen gebracht">
<correction grub2 "grub-mknetdir: Unterstützung für ARM64-EFI hinzugefügt; standardmäßige TSC-Kalibrierungsmethode auf EFI-Systemen auf pmtimer umgestellt">
<correction hdparm "APM nur auf Geräten erlauben, die es bewerben">
<correction https-everywhere "Neue Version der Originalautoren zurückportiert, damit sie kompatibel mit Firefox ESR 60 ist">
<correction i3-wm "Absturz bei Neustart behoben, wenn Markierungen verwendet werden">
<correction iipimage "Apache-Konfiguration korrigiert">
<correction jhead "Sicherheitsprobleme behoben [CVE-2018-17088 CVE-2018-16554]">
<correction lastpass-cli "Hartkodierte Zertifikats-Pinnungen von lastpass-cli 1.3.1 zurückportiert, um die Änderungen im gehosteten Lastpass.com-Dienst widerzuspiegeln">
<correction ldap2zone "Endlosschleife beim Prüfen der Zonenreihe behoben">
<correction libcgroup "Allgemein zugängliche (und schreibbare) Protokolldateien abgesperrt [CVE-2018-14348]">
<correction libclamunrar "Neue Version der Originalautoren">
<correction libdap "Inhalte von libdap-doc überarbeitet">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisieren">
<correction libgd2 "Bmp: Rückgabewert in gdImageBmpPtr [CVE-2018-1000222] prüfen; potenzielle Endlosschleife in gdImageCreateFromGifCtx [CVE-2018-5711] behoben">
<correction libmail-deliverystatus-bounceparser-perl "Nicht verteilungsfähige Beispiel-Spam und -Viren entfernt">
<correction libmspack "Schreibzugriffe außerhalb der Grenzen [CVE-2018-18584] und Akzeptanz <q>leerer</q> Dateinamen behoben [CVE-2018-18585]">
<correction libopenmpt "<q>up11: Lesezugriffe außerhalb der Grenzen beim Laden von IT-/MO3-Dateien mit vielen Musterschleifen</q> behoben [CVE-2018-10017]">
<correction libseccomp "Unterstützung für Linux-4.9-Systemaufrufe hinzugefügt: preadv2, pwritev2, pkey_mprotect, pkey_alloc und pkey_free; Unterstützung für statx hinzugefügt">
<correction libtirpc "rendezvous_request: Rückgabewert von makefd_xprt prüfen [CVE-2018-14622]">
<correction libx11 "Mehrere Sicherheitsprobleme gelöst [CVE-2018-14598 CVE-2018-14599 CVE-2018-14600]">
<correction libxcursor "Dienstblockade oder potenzielle Codeausführung durch einen Ein-Byte-Heap-Überlauf behoben [CVE-2015-9262]">
<correction libxml-stream-perl "Standardmäßigen CA-Pfad eingetragen">
<correction libxml-structured-perl "Fehlende Bau- und Laufzeitabhängigkeiten von libxml-parser-perl nachgetragen">
<correction linux "Xen: Boot-Regression in PV-Domains behoben; xen-netfront: Regressionen behoben; ext4: Falsch-Negative *und* Falsch-Positive in ext4_check_descriptors() behoben; udeb: virtio_console zu virtio-modules hinzugefügt; cdc_ncm: Padding übers Ende von skb verhindern; <q>sit: iphdr in ipip6_rcv neu laden</q> zurückgenommen; neue Version der Originalautoren">
<correction lxcfs "Betriebszeit-Virtualisierung zurückgenommen, damit die Prozess-Startzeiten wieder stimmen">
<correction magicmaze "Von fonts-isabella abhängen, jetzt wo ttf-isabella ein virtuelles Paket ist">
<correction mailman "Anfälligkeit für eigenmächtige Text-Injizierung in Mailman-CGIs behoben [CVE-2018-13796]">
<correction multipath-tools "Deadlock in udev-Auslösern behoben">
<correction nagstamon "IcingaWeb2 Basic-Authentifizierungsproblem behoben">
<correction network-manager "libnm: Zugriff auf aktivierte und gemessene Eigenschaften überarbeitet; Heap-Schreibzugriff außerhalb der Grenzen im dhcpv6 option handling [CVE-2018-15688] und verschiedene andere Probleme im sd-network-basierten dhcp=internal-Plugin behoben">
<correction network-manager-applet "libnma/pygobject: libnma/NMA muss statt veralteter Bibliotheken libnm/NM benutzen">
<correction ola "Tippfehler in /etc/init.d/rdm_test_server korrigiert; Dateinamen für jquery in den statischen HTML-Dateien des rdm-Testservers korrigiert">
<correction opensc "Unbegrenzte Rekursion und mehrere Schreib- oder Lesezugriffe außerhalb der Grenzen behoben [CVE-2018-16391 CVE-2018-16392 CVE-2018-16393 CVE-2018-16418 CVE-2018-16419 CVE-2018-16420 CVE-2018-16421 CVE-2018-16422 CVE-2018-16423 CVE-2018-16424 CVE-2018-16425 CVE-2018-16426 CVE-2018-16427]">
<correction pkgsel "Neue Abhängigkeiten installieren, wenn safe-upgrade (Standard) gewählt worden ist">
<correction publicsuffix "Enthaltene Daten aktualisiert">
<correction python-django "Standardmäßig Spatialite &gt;= 4.2 unterstützen">
<correction python-imaplib2 "Das richtige Modul für Python 3 installieren; TIMEOUT_MAX nicht verwenden">
<correction rustc "Bau auf zusätzlichen Architekturen ermöglichen: arm64, armel, armhf, i386, ppc64el, s390x">
<correction sddm "PAMs umgebende Zusatzgruppen akzeptieren; fehlende Handhabung von utmp/wtmp/btmp hinzugefügt">
<correction serf "Nullzeigerdereferenzierung behoben">
<correction soundconverter "Opus-VBR-Einstellung korrigiert">
<correction spamassassin "Neue Version der Originalautoren; Dienstblockade [CVE-2017-15705], Codeausführung aus der Ferne [CVE-2018-11780], Codeinjizierung [CVE-2018-11781]  und unsichere Verwendung von <q>.</q> in @INC [CVE-2016-1238] behoben; spamd-Dienstverwaltung bei Paket-Upgrades überarbeitet">
<correction spice-gtk "Pufferüberlauf in flexiblen Arrays behoben [CVE-2018-10873]">
<correction sqlcipher "Absturz beim Öffnen einer Datei vermeiden">
<correction subversion "Durch die Maßnahmen gegen SHA1-Kollisionen verursachte Regression behoben, bei der Commits fälschlicherweise mit der Fehlermeldung <q>Dateisystem ist korrumpiert</q> fehlschlagen, wenn die Delta-Länge ein Vielfaches von 16k ist">
<correction systemd "networkd: manager_connect_bus() nicht fehlschlagen lassen, wenn dbus noch nicht aktiv ist; dhcp6: Sicherstellen, dass genug Platz für den DHCP6-Optionsheader vorhanden ist [CVE-2018-15688]">
<correction systraq "Logik umgekehrt, sodass das Programm erfolgreich beendet wird, wenn /e/s/Makefile fehlt">
<correction tomcat-native "OSCP-Responder-Problem behoben, welches Benutzern mit zurückgezogenen Zertifikaten die Authentifizierung ermöglicht hat, wenn beiderseitiges (mutual) TLS verwendet wird [CVE-2018-8019 CVE-2018-8020]">
<correction tor "Änderung an Directory-Autorität: <q>Bifroest</q> Bridge Authority ausmustern und mit <q>Serge</q> ersetzen; eine IPv6-Adresse für die <q>dannenberg</q>-Directory-Authority">
<correction tzdata "Neue Version der Originalautoren">
<correction ublock-origin "Neue Version der Originalautoren zurückportieren, damit sie kompatibel mit Firefox ESR 60 ist">
<correction unbound "Sicherheitslücke beim Verarbeiten von Wildcard-Synthetik-NSEC-Records behoben [CVE-2017-15105]">
<correction vagrant "VirtualBox 5.2 unterstützen">
<correction vmtk "python-vmtk: Fehlende Abhängigkeit von python-vtk6 hinzufügen">
<correction wesnoth-1.12 "Das Laden von lua-Bytecode via load/dofile verbieten [CVE-2018-1999023]">
<correction wpa "Unauthentifizierte verschlüsselte EAPOL-Schlüsseldaten ignorieren [CVE-2018-14526]">
<correction x11vnc "Zwei Pufferüberläufe behoben">
<correction xapian-core "Glass-Backend-Fehler mit langlebigen Positionszeigern auf einer Tabelle in einer WritableDatabase behoben, der zu einem fälschlicherweise ausgeworfenen DatabaseCorruptError führen kann, obwohl die Datenbank in Ordnung ist">
<correction xmotd "Absturz mit Härtungs-Flags vermeiden">
<correction xorg-server "GLX: keine sRGB-Konfiguration für 32-bit-RGBA-Visual annehmen – behebt verschiedene Blending-Probleme mit kwin und Mesa &gt;= 18.0 (z. B. Mesa aus stretch-backports)">
<correction zutils "Pufferüberlauf in zcat behoben [CVE-2018-1000637]">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2017 4074 imagemagick>
<dsa 2018 4103 chromium-browser>
<dsa 2018 4182 chromium-browser>
<dsa 2018 4237 chromium-browser>
<dsa 2018 4242 ruby-sprockets>
<dsa 2018 4243 cups>
<dsa 2018 4244 thunderbird>
<dsa 2018 4245 imagemagick>
<dsa 2018 4246 mailman>
<dsa 2018 4247 ruby-rack-protection>
<dsa 2018 4248 blender>
<dsa 2018 4249 ffmpeg>
<dsa 2018 4250 wordpress>
<dsa 2018 4251 vlc>
<dsa 2018 4252 znc>
<dsa 2018 4253 network-manager-vpnc>
<dsa 2018 4254 slurm-llnl>
<dsa 2018 4256 chromium-browser>
<dsa 2018 4257 fuse>
<dsa 2018 4258 ffmpeg>
<dsa 2018 4260 libmspack>
<dsa 2018 4261 vim-syntastic>
<dsa 2018 4262 symfony>
<dsa 2018 4263 cgit>
<dsa 2018 4264 python-django>
<dsa 2018 4265 xml-security-c>
<dsa 2018 4266 linux>
<dsa 2018 4267 kamailio>
<dsa 2018 4268 openjdk-8>
<dsa 2018 4269 postgresql-9.6>
<dsa 2018 4270 gdm3>
<dsa 2018 4271 samba>
<dsa 2018 4272 linux>
<dsa 2018 4273 intel-microcode>
<dsa 2018 4274 xen>
<dsa 2018 4275 keystone>
<dsa 2018 4276 php-horde-image>
<dsa 2018 4277 mutt>
<dsa 2018 4278 jetty9>
<dsa 2018 4279 linux>
<dsa 2018 4279 linux-latest>
<dsa 2018 4280 openssh>
<dsa 2018 4281 tomcat8>
<dsa 2018 4282 trafficserver>
<dsa 2018 4283 ruby-json-jwt>
<dsa 2018 4284 lcms2>
<dsa 2018 4285 sympa>
<dsa 2018 4286 curl>
<dsa 2018 4287 firefox-esr>
<dsa 2018 4288 ghostscript>
<dsa 2018 4289 chromium-browser>
<dsa 2018 4290 libextractor>
<dsa 2018 4291 mgetty>
<dsa 2018 4292 kamailio>
<dsa 2018 4293 discount>
<dsa 2018 4294 ghostscript>
<dsa 2018 4295 thunderbird>
<dsa 2018 4296 mbedtls>
<dsa 2018 4297 chromium-browser>
<dsa 2018 4298 hylafax>
<dsa 2018 4299 texlive-bin>
<dsa 2018 4300 libarchive-zip-perl>
<dsa 2018 4301 mediawiki>
<dsa 2018 4302 openafs>
<dsa 2018 4303 okular>
<dsa 2018 4304 firefox-esr>
<dsa 2018 4305 strongswan>
<dsa 2018 4306 python2.7>
<dsa 2018 4307 python3.5>
<dsa 2018 4308 linux>
<dsa 2018 4309 strongswan>
<dsa 2018 4310 firefox-esr>
<dsa 2018 4311 git>
<dsa 2018 4312 tinc>
<dsa 2018 4313 linux>
<dsa 2018 4314 net-snmp>
<dsa 2018 4315 wireshark>
<dsa 2018 4316 imagemagick>
<dsa 2018 4317 otrs2>
<dsa 2018 4318 moin>
<dsa 2018 4319 spice>
<dsa 2018 4320 asterisk>
<dsa 2018 4321 graphicsmagick>
<dsa 2018 4322 libssh>
<dsa 2018 4323 drupal7>
<dsa 2018 4324 firefox-esr>
<dsa 2018 4325 mosquitto>
<dsa 2018 4326 openjdk-8>
<dsa 2018 4327 thunderbird>
<dsa 2018 4328 xorg-server>
<dsa 2018 4329 teeworlds>
<dsa 2018 4331 curl>
</table>

<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction adblock-plus-element-hiding-helper "Inkompatibel mit neueren firefox-esr-Versionen">
<correction all-in-one-sidebar "Inkompatibel mit neueren firefox-esr-Versionen">
<correction autofill-forms "Inkompatibel mit neueren firefox-esr-Versionen">
<correction automatic-save-folder "Inkompatibel mit neueren firefox-esr-Versionen">
<correction classic-theme-restorer "Inkompatibel mit neueren firefox-esr-Versionen">
<correction colorfultabs "Inkompatibel mit neueren firefox-esr-Versionen">
<correction custom-tab-width "Inkompatibel mit neueren firefox-esr-Versionen">
<correction dactyl "Inkompatibel mit neueren firefox-esr-Versionen">
<correction downthemall "Inkompatibel mit neueren firefox-esr-Versionen">
<correction dvips-fontdata-n2bk "Leeres Paket">
<correction firebug "Inkompatibel mit neueren firefox-esr-Versionen">
<correction firegestures "Inkompatibel mit neueren firefox-esr-Versionen">
<correction firexpath "Inkompatibel mit neueren firefox-esr-Versionen">
<correction flashgot "Inkompatibel mit neueren firefox-esr-Versionen">
<correction form-history-control "Inkompatibel mit neueren firefox-esr-Versionen">
<correction foxyproxy "Inkompatibel mit neueren firefox-esr-Versionen">
<correction gitlab "Offene Sicherheitslücken, für die Lösungen schwierig zurückzuportieren sind">
<correction greasemonkey "Inkompatibel mit neueren firefox-esr-Versionen">
<correction intel-processor-trace [s390x] "Nur auf Intel-Architekturen nützlich">
<correction itsalltext "Inkompatibel mit neueren firefox-esr-Versionen">
<correction knot-resolver "Sicherheitsprobleme; schwierig, Korrekturen zurückzuportieren">
<correction lightbeam "Inkompatibel mit neueren firefox-esr-Versionen">
<correction livehttpheaders "Inkompatibel mit neueren firefox-esr-Versionen">
<correction lyz "Inkompatibel mit neueren firefox-esr-Versionen">
<correction npapi-vlc "Inkompatibel mit neueren firefox-esr-Versionen">
<correction nukeimage "Inkompatibel mit neueren firefox-esr-Versionen">
<correction openinbrowser "Inkompatibel mit neueren firefox-esr-Versionen">
<correction perspectives-extension "Inkompatibel mit neueren firefox-esr-Versionen">
<correction pwdhash "Inkompatibel mit neueren firefox-esr-Versionen">
<correction python-facebook "Defekt wegen Änderungen durch die Originalautoren">
<correction python-tvrage "Nutzlos, nachdem tvrage.com abgeschaltet wurde">
<correction reloadevery "Inkompatibel mit neueren firefox-esr-Versionen">
<correction sage-extension "Inkompatibel mit neueren firefox-esr-Versionen">
<correction scrapbook "Inkompatibel mit neueren firefox-esr-Versionen">
<correction self-destructing-cookies "Inkompatibel mit neueren firefox-esr-Versionen">
<correction spdy-indicator "Inkompatibel mit neueren firefox-esr-Versionen">
<correction status-4-evar "Inkompatibel mit neueren firefox-esr-Versionen">
<correction stylish "Inkompatibel mit neueren firefox-esr-Versionen">
<correction tabmixplus "Inkompatibel mit neueren firefox-esr-Versionen">
<correction tree-style-tab "Inkompatibel mit neueren firefox-esr-Versionen">
<correction ubiquity-extension "Inkompatibel mit neueren firefox-esr-Versionen">
<correction uppity "Inkompatibel mit neueren firefox-esr-Versionen">
<correction useragentswitcher "Inkompatibel mit neueren firefox-esr-Versionen">
<correction video-without-flash "Inkompatibel mit neueren firefox-esr-Versionen">
<correction webdeveloper "Inkompatibel mit neueren firefox-esr-Versionen">
<correction xul-ext-monkeysphere "Inkompatibel mit neueren firefox-esr-Versionen">
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Kraft und Zeit einbringen, um das vollständig freie Betriebssystem 
Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail an 
&lt;press@debian.org&gt; oder kontaktieren das Stable-Release-Team 
auf Englisch über &lt;debian-release@lists.debian.org&gt;.</p>
