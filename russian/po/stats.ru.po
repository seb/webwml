# translation of others.ru.po to Russian
# Lev Lamberov <l.lamberov@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml stats\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2018-06-28 15:02+0500\n"
"Last-Translator: Lev Lamberov <dogsleg@debian.org>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 2.0.8\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Статистика перевода сайта Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Страниц для перевода: %d."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Байт для перевода: %d."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Строк для перевода: %d."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Неверная версия перевода"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Этот перевод слишком устарел"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Оригинал новее, чем перевод"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "Оригинал более не существует"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "число обращений не доступно"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "обращения"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Нажмите, чтобы получить данные diffstat"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr "Создано с помощью <transstatslink>"

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Резюме перевода для"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Не переведено"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Устарело"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Переведено"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "Актуально"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "файлы"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "байты"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Замечание: список страниц отсортирован по популярности. Наведите указатель "
"мыши на название страницы, чтобы увидеть количество обращений."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Устаревшие переводы"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Файл"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Комментарий"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr "Командная строка git"

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Журнал"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Перевод"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Сопровождающий"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Состояние"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Переводчик"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Дата"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Не переведённые страницы общего характера"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Не переведённые страницы общего характера"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Не переведённые новости"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Не переведённые новости"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Не переведённые страницы о консультантах/пользователях"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Не переведённые страницы консультантов/пользователей"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Не переведённые международные страницы"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Не переведённые международные страницы"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Переведённые страницы (актуальные)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Переведённые шаблоны (файлы PO)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "Статистика перевода PO"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Неточные"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Не переведено"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Всего"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Всего:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Переведённые веб-страницы"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Статистика перевода по счетчику страниц"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Язык"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Переводы"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Переведённые веб-страницы (по размеру)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Статистика перевода по размеру страниц"

#~ msgid "Created with"
#~ msgstr "Создано с помощью"

#~ msgid "Hit data from %s, gathered %s."
#~ msgstr "Данные о посещениях с %s, собраны %s."

#~ msgid "Commit diff"
#~ msgstr "diff коммита"

#~ msgid "Colored diff"
#~ msgstr "Цветной diff"

#~ msgid "Unified diff"
#~ msgstr "Унифицированный diff"
